<?php

require_once './controller/ControllerBase.php';

class DefaultController extends ControllerBase {

    public function index() {
        self::render('./view/home.php', []);
    }

    public function modal_register() {
        if ($this->denyRender(parent::RENDER_VIEW) && parent::method('GET', TRUE)) {
            $type_holes = new TypeOfHoles();
            if ($this->_render == parent::RENDER_JSON) {
                json_encode($type_holes);
            } elseif (file_exists('./view/type_holes/type_holes_register.php')) {
                include './view/type_holes/type_holes_register.php';
            } else {
                echo 'There was a problem fetching the data.';
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . 01);
        }
    }
}
