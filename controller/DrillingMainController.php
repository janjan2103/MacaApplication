<?php

require_once './controller/ControllerBase.php';

class DrillingMainController extends ControllerBase {

    public function index() {
        $drilling_main = new DrillingMain();
        $list = $drilling_main->getList();
        self::render('./view/drilling_main/drilling_main_index.php', ['list' => $list, 'drilling_main' => $drilling_main]);
    }

    public function info() {
        if (parent::method('GET', TRUE)) {
            $drilling_main = new DrillingMain(self::paramGet('id', TRUE));
            $productions = (new DrillingProduction())->retriveAll(['id_drilling_main' => $drilling_main->id_drilling_main]);
            if ($this->_render == parent::RENDER_PARTIAL) {
                if (file_exists('./view/drilling_main/_info_drilling_main.php')) {
                    include './view/drilling_main/_info_drilling_main.php';
                }
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . '10');
        }
    }

    public function register() {
        if (parent::method('GET', TRUE)) {
            $drilling_main = new DrillingMain();
            self::render("./view/drilling_main/drilling_main_register.php", ["drilling_main" => $drilling_main, 'method' => 'create']);
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . '10');
        }
    }

    public function edit() {
        if (parent::method('GET', TRUE)) {
            $drilling_main = new DrillingMain(self::paramGet('drilling'));
            $drilling_main->date_drilling = Validate::formatDateTimeFromEn($drilling_main->date_drilling);
            $drilling_main->start_hamer = $drilling_main->start_hamer;
            $drilling_main->finish_hamer = $drilling_main->finish_hamer;
            self::render("./view/drilling_main/drilling_main_edit.php", ["drilling_main" => $drilling_main, 'method' => 'update']);
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . '10');
        }
    }

    public function detail() {
        if (parent::method('GET', TRUE)) {
            if (!empty(self::paramGet("id"))) {
                $drilling_main = new DrillingMain(self::paramGet("id"));
                self::render("./admin/drilling_main/drilling_main_detail.php", ["drilling_main" => $drilling_main]);
            } else {
                self::render("./admin/drilling_main/drilling_main_index.php", ["drilling_main" => $drilling_main]);
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . '10');
        }
    }

    public function create() {
        try {
            $productions = self::paramPost('productions');
            $drilling_main = new DrillingMain();
            $drilling_main->setState($_POST);
            if ($drilling_main->id_shift <= 0) {
                throw new Exception("Define a Shift", self::ERROR_CODE_PREFIX . 2689);
            }
            if ($drilling_main->id_drill_operator <= 0) {
                throw new Exception("Define a Drill Operator", self::ERROR_CODE_PREFIX . 2689);
            }
            $drilling_main->hours_shift = 1;
            $drilling_main->date_drilling = Validate::formatDateTimeFromPtBr($drilling_main->date_drilling);
            $drilling_main->start_hamer = $drilling_main->start_hamer;
            $drilling_main->finish_hamer = $drilling_main->finish_hamer;
            $drilling_main->id_drilling_main = $drilling_main->create();
            if (!$drilling_main->id_drilling_main) {
                throw new ExceptionController('Fail in register', 1212);
            } else {
                $this->updateProductions($productions, $drilling_main);
                return ['code' => 0, 'message' => 'Drilling Main registed  and drilling productions updated'];
            }
        } catch (Exception $e) {
            return ['code' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }

    public function update() {
        try {
            $productions = self::paramPost('productions');
            $drilling_main = new DrillingMain();
            $drilling_main->setState($_POST);
            if ($drilling_main->id_shift <= 0) {
                throw new Exception("Define a Shift", self::ERROR_CODE_PREFIX . 2689);
            }
            if ($drilling_main->id_drill_operator <= 0) {
                throw new Exception("Define a Drill Operator", self::ERROR_CODE_PREFIX . 2689);
            }
            $drilling_main->hours_shift = 1;
            $drilling_main->date_drilling = Validate::formatDateTimeFromPtBr($drilling_main->date_drilling);
            $drilling_main->start_hamer = $drilling_main->start_hamer;
            $drilling_main->finish_hamer = $drilling_main->finish_hamer;
            $drilling_main->update(['id_drilling_main' => $drilling_main->id_drilling_main]);
            if (!$drilling_main->id_drilling_main) {
                throw new ExceptionController('Fail in update', 1212);
            } else {
                $this->updateProductions($productions, $drilling_main);
                return ['code' => 1, 'message' => 'Drilling Main updated and drilling productions updated'];
            }
        } catch (PDOException $ex) {
            return ['code' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }

    private function updateProductions(Array $productions, DrillingMain $drilling_main) {
        try {
            $drilling_production = new DrillingProduction();
            $drilling_production->id_drilling_main = $drilling_main->id_drilling_main;
            $rows = 0;
            $drilling_production->delete(['id_drilling_main' => $drilling_main->id_drilling_main]);
            foreach ($productions as $row => $field) {
                $drilling_production->setState($field);
                $drilling_production->id_drilling_production = NULL;
                if ($drilling_production->id_type_holes > 0 && $drilling_production->meters > 0) {
                    $drilling_production->create();
                    $rows++;
                }
            }
        } catch (Exception $e) {
            throw new ExceptionController('It\'s do not possible update this productions.', self::ERROR_CODE_PREFIX . '80');
        }
    }

}
