<?php

require_once './controller/ControllerBase.php';

class ShiftController extends ControllerBase {

    public function index() {
        $shift = new Shift();
        $list = $shift->getList();
        self::render('./view/shift/shift_index.php', ['shift' => $shift, 'list' => $list]);
    }

    public function info() {
        if (parent::method('GET', TRUE)) {
            $shift = new Shift(self::paramGet('id', TRUE));
            if ($this->_render == parent::RENDER_PARTIAL) {
                if (file_exists('./view/shift/_info_shift.php')) {
                    include './view/shift/_info_shift.php';
                }
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . '10');
        }
    }

    public function modal_register() {
        if ($this->denyRender(parent::RENDER_VIEW) && parent::method('GET', TRUE)) {
            $shift = new Shift();
            if ($this->_render == parent::RENDER_PARTIAL) {
                if (file_exists('./view/shift/shift_register.php')) {
                    include './view/shift/shift_register.php';
                } else {
                    echo 'There was a problem fetching the data.';
                }
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . 01);
        }
    }

    public function modal_edit() {
        if ($this->denyRender(parent::RENDER_VIEW) && parent::method('GET', TRUE)) {
            $shift = new Shift(self::paramGet('shift', TRUE));
            if ($this->_render == parent::RENDER_PARTIAL) {
                if (file_exists('./view/shift/shift_edit.php')) {
                    include './view/shift/shift_edit.php';
                } else {
                    echo 'There was a problem fetching the data.';
                }
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . 01);
        }
    }

    public function create() {
        try {
            $shift = new Shift();
            $shift->setState($_POST);
            if ($shift->create()) {
                return ['code' => 0, 'message' => 'Successfully registered'];
            } else {
                throw new Exception('Falha ao registrar', self::ERROR_CODE_PREFIX . 02);
            }
        } catch (Exception $e) {
            return ['code' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }

    public function update() {
        try {
            $shift = new Shift(self::paramPost('id_shift'));
            $shift->setState($_POST);
            if ($shift->update(['id_shift' => $shift->id_shift]) >= 0) {
                return ['code' => 0, 'message' => 'Successfully registered'];
            } else {
                throw new Exception('Falha ao atualizar', self::ERROR_CODE_PREFIX . 02);
            }
        } catch (Exception $e) {
            return ['code' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }

}
