<?php

require_once './controller/ControllerBase.php';

class CategoryPeriodController extends ControllerBase {

    public function index() {
        $id = $GLOBALS['match']['params']['id'];
        $data = (new CategoryPeriod())->read();
        return $data;
    }

    public function register() {
        $id = $GLOBALS['match']['params']['id'];
        return ['id_category' => $id];
    }

    public function create() {
        $category_period = new CategoryPeriod();
        $count_periods = count((new Category($GLOBALS['match']['params']['id']))->getPeriods());
        $category_period->id_period = $count_periods + 1;
        $category_period->value = self::paramPost("period_value");
        $category_period->dtt_limit = self::paramPost("period_dtt_limit");
        $category_period->quantity = self::paramPost("period_quantity");
        $category_period->id_category = $GLOBALS['match']['params']['id'];
        if ($category_period->create() == 0) {
            return ['code' => 0, 'message' => 'Successfully registered'];
        } else {
            return ['id' => $id, 'message' => 'Falha ao registrar'];
        }
    }

    public function update() {
        $category_period = (new CategoryPeriod())->retrive(['id_period' => self::paramPost('period_id'), 'id_category' => $GLOBALS['match']['params']['id']]);
        if ($category_period instanceof CategoryPeriod) {
            $category_period->value = self::paramPost("period_value");
            $category_period->dtt_limit = self::paramPost("period_dtt_limit");
            $category_period->quantity = self::paramPost("period_quantity");
            if ($category_period->update(['id_period' => $category_period->id_period, 'id_category' => $category_period->id_category])) {
                return ['code' => 0, 'message' => 'Atualizado com sucesso'];
            } else {
                return ['code' => 1211, 'message' => 'Falha ao atualizar'];
            }
        } else {
            return ['code' => 1211, 'message' => 'Falha ao atualizar'];
        }
    }

    public function delete() {
        $category_period = (new CategoryPeriod())->retrive(['id_period' => self::paramPost('period_id'), 'id_category' => $GLOBALS['match']['params']['id']]);
        if ($category_period instanceof CategoryPeriod) {
            if ($category_period->delete(['id_period' => $category_period->id_period, 'id_category' => $category_period->id_category])) {
                return ['code' => 0, 'message' => 'Deletado com sucesso'];
            } else {
                return ['code' => 1211, 'message' => 'Falha ao deletar'];
            }
        } else {
            return ['code' => 1211, 'message' => 'Falha ao deletar'];
        }
    }

    public function get_category_periods() {
        $data = (new CategoryPeriod())->getList();
        return $data;
    }

}
