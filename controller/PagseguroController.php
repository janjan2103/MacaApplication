<?php

require_once './library/PagSeguro/PagSeguroLibrary.php';
require_once './helpers/Pagseguro.php';
require_once './controller/ControllerBase.php';

class PagseguroController extends ControllerBase {

    private $credentials;

    public function __construct($route = NULL) {
        $this->credentials = Pagseguro::credentials();
        return parent::__construct($route);
    }

    public function index() {
        if ($this->isUser()) {
            
        } else {
            $this->redirect(routeFor('/admin/purchases/confirm/'));
        }
    }

    public function sessionid() {
        $checkout = new Checkout();
        $checkout->printSessionId();
    }

    public function checkout_transparent() {
        try {
            $checkout = new Checkout();
            $xmlArray = $checkout->doPayment($_POST);
            if (array_key_exists('transaction', $xmlArray)) {
                $transaction = PagSeguroTransactionSearchService::searchByCode(
                                $this->credentials, $xmlArray['transaction']['code']
                );
                $payment_status = $this->buildPaymentStatusByTransaction($transaction, "");
                if ($payment_status->create()) {
                    $this->updateStatusItens($payment_status);
                    if(array_key_exists('paymentLink', $xmlArray['transaction'])) {
                        echo json_encode(['code' => 1, 'message' => 'Acesse seu email para mais informações sobre o pagamento, imprima o boleto e o pague em até 3 dias.', 'paymentLink' => $xmlArray['transaction']['paymentLink']]);
                    } else {
                        echo json_encode(['code' => 0, 'message' => 'Seu pagamento foi registrado no pagseguro. Acompanhe a atualização dos status de seus pagamentos <a href="/transactions/" class="text-info">aqui</a>.', 'paymentLink' => NULL]);
                    }
                    
                } else {
                    Logger::log_array('error_debug_pagseguro', $payment_status);
                    echo json_encode(['code' => 1, 'message' => 'Notificação não foi registrada.']);
                }
            } else {
                echo json_encode($xmlArray);
            }
        } catch (Exception $e) {
            Logger::log_exception($e);
            ob_clean();
            echo json_encode(['code' => $e->getCode(), 'message' => $e->getMessage()]);
        }
    }

    private function updateStatusItens($payment_status) {
        $itens = json_decode($payment_status->json_itens);
        $purchase = new Purchase();
        foreach ($itens AS $id => $item) {
            $pc = $purchase->retrive(['id_purchase' => $item->id]);
            $pc->id_status = $payment_status->transaction_status;
            $pc->update(['id_purchase' => $item->id]);
            $event = (new Event())->retrive(['id_event' => $pc->id_event]);
            switch ($item->type_id) {
                case Purchase::ITEM_TICKET:
                    $ticket = new Ticket($pc->id_reference, $event->db_name);
                    $ticket->id_ticket_status = $payment_status->transaction_status;
                    $ticket->update(['id_ticket' => $ticket->id_ticket]);
                    break;
                case Purchase::ITEM_CATEGORY:
                    $user_category = new UserCategory($pc->id_reference, $event->db_name);
                    $user_category->id_status = $payment_status->transaction_status;
                    $user_category->update(['id_user_category' => $user_category->id_user_category]);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Local em que o usuário volta após o pagamento
     */
    protected function callback() {
        try {
            $code = self::paramGet('cod_transaction');
            $transaction = PagSeguroTransactionSearchService::searchByCode(
                            $this->credentials, $code
            );
            $payment_status = $this->buildPaymentStatusByTransaction($transaction);
            $last = $payment_status->lastNotificationByUser($payment_status->reference_id);
            if ($payment_status->create()) {
                $res = $payment_status->itensProcess($last);
                Session::setMessage('Seu pagamento foi registrado no pagseguro. Acompanhe a atualização dos status de seus ingressos.');
            } else {
                Logger::log_array(get_class(), get_class_vars($payment_status));
                throw new Exception('Notificação não foi registrada', self::ERROR_CODE_PREFIX . 32);
            }
        } catch (Exception $exc) {
            Logger::log_array(get_class(), ['cod' => $exc->getCode(), 'msg' => $exc->getMessage()]);
        }
        if ($this->isUser()) {
            $this->redirect(routeFor('/tickets/'));
        } else {
            $this->redirect(routeFor('/login/'));
        }
    }

    protected function update() {
        $this->method('GET');
        $code = $this->paramAny('transaction', TRUE);

        $transaction = PagSeguroTransactionSearchService::searchByCode(
                        $this->credentials, $code
        );
        $payment_status = $this->buildPaymentStatusByTransaction($transaction);

        $last = $payment_status->lastNotificationByUser($payment_status->reference_id);
        $payment_status->id = $payment_status->create();
        if ($payment_status->id > 0) {
            $res = $payment_status->itensProcess($last);
            if ($this->_render == self::RENDER_JSON) {
                return ['itens' => $res, 'notification_id' => $payment_status->id];
            } else {
                $this->redirect(routeFor('/admin/purchases/detail/', ['id' => $payment_status->id]), 'Status da transação atualizada.');
            }
        } else {
            Logger::log_array(get_class(), get_class_vars($payment_status));
            throw new Exception('Não foi identificada atualizações.', self::ERROR_CODE_PREFIX . 32);
        }
    }

    /**
     * Recebe notificações do Pagseguro
     * @throws Exception
     */
    protected function notification() {
        try {
            $this->credentials = Pagseguro::credentials();
            $notificationType = $this->paramPost('notificationType', TRUE);
            $notificationCode = $this->paramPost('notificationCode', TRUE);
            Logger::log_array('notification_requests', $_POST);
            if ($notificationType === 'transaction') :
                return $this->processNotification($notificationCode);
            else:
                throw new Exception('Método de Notificação inválido', self::ERROR_CODE_PREFIX . 20);
            endif;
        } catch (Exception $exc) {
            Logger::log_array(get_class(), ['cod' => $exc->getCode(), 'msg' => $exc->getMessage()]);
        }
    }

    private function processNotification($notificationCode) {
        try {
            $transaction = PagSeguroNotificationService::checkTransaction($this->credentials, $notificationCode);
            $payment_status = $this->buildPaymentStatusByTransaction($transaction, $notificationCode);

            $last = $payment_status->lastNotificationByUser($payment_status->reference_id);
            $payment_status->id = $payment_status->create();
            if ($payment_status->id > 0) {
                $res = $payment_status->itensProcess($last);
                header("HTTP/1.0 200 success");
                return json_encode(['itens' => $res, 'notification_id' => $payment_status->id]);
            } else {
                header("HTTP/1.0 500 not_registered");
                Logger::log_array(get_class(), get_class_vars($payment_status));
                throw new Exception('Notificação não foi registrada', self::ERROR_CODE_PREFIX . 32);
            }
        } catch (Exception $exc) {
            header("HTTP/1.0 500 Error");
            Logger::log_exception($exc);
            return [$exc->getCode(), $exc->getMessage()];
        }
    }

    private function buildPaymentStatusByTransaction(PagSeguroTransaction $transaction, $notificationCode = NULL) {
        $payment_status = new PaymentStatus();
        $payment_status->id_method = PaymentMethod::PAGSEGURO;
        $payment_status->notification_code = $notificationCode;
        $transactionType = $transaction->getType();
        $paymentMethod = $transaction->getPaymentMethod();
        $paymentMethodType = $paymentMethod->getType();
        $paymentMethodCode = $paymentMethod->getCode();
        $status = $transaction->getStatus();
        $sender = $transaction->getSender();
        $cancelation = $transaction->getCancellationSource();

        $payment_status->reference_id = $transaction->getReference();
        $payment_status->id_user = $this->_user->id_user;
        $payment_status->transaction_status = $status->getValue();
        $payment_status->transaction_status_desc = $status->getTypeFromValue();
        $payment_status->notification_code = $notificationCode;
        $payment_status->transaction_code = $transaction->getCode();
        $payment_status->total_value = $transaction->getGrossAmount();
        $payment_status->discount_value = $transaction->getDiscountAmount();
        $payment_status->liquid_value = $transaction->getNetAmount();
        $payment_status->extra_value = $transaction->getExtraAmount();
        $dtt_transaction = new DateTime($transaction->getDate());
        $payment_status->dtt_transaction = $dtt_transaction->format('Y-m-d H:i');
        $dtt_lastevent = new DateTime($transaction->getLastEventDate());
        $payment_status->dtt_lastevent = $dtt_lastevent->format('Y-m-d H:i');
        $payment_status->payment_way_code = $paymentMethodCode->getValue();
        $payment_status->payment_way_desc = $paymentMethodCode->getTypeFromValue();
        $payment_status->purchase_method_code = $paymentMethodType->getValue();
        $payment_status->purchase_method_desc = $paymentMethodType->getTypeFromValue();
        $payment_status->installment_number = $transaction->getInstallmentCount();
        $payment_status->cancelation_source = $cancelation instanceof PagSeguroTransactionCancellationSource ?
                $cancelation->getValue() : NULL;

        $_itens = [];
        $tItens = $transaction->getItems();
        if ($tItens && is_array($tItens)) {
            foreach ($tItens as $key => $item) {
                $id = $item->getId();
                $ids = explode('-', $id);
                if (count($ids) > 1) {
                    $type_id = $ids[0];
                    $reference_id = $ids[1];
                } else {
                    $type_id = $id;
                    $reference_id = $id;
                }
                $_itens[$item->getId()] = [
                    'id' => $id,
                    'type_id' => $type_id,
                    'reference_id' => $reference_id,
                    'description' => Validate::replaceSpecialChars($item->getDescription()),
                    'quantity' => $item->getQuantity(),
                    'value' => $item->getAmount(),
                ];
            }
        }
        $payment_status->_itens = $payment_status->setItens($_itens);
        return $payment_status;
    }

}
