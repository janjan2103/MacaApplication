<?php

require_once './controller/ControllerBase.php';

class SessionController extends ControllerBase {

    public function index() {
        self::render("./view/home.php");
    }

    public function create() {
        try {
            $user = new User();
            $user->setState($_POST);
            if ($user->password != $user->re_password) {
                throw new Exception("The passwords don't are the same.", "32133");
            }
            $user->password = md5($user->password);
            $user->id_user = $user->create();
            if ($user->id_user > 0) {
                Session::setCurrentUser($user);
                return ['code' => 0, 'message' => 'Redirecting...', 'url' => "/"];
            } else {
                throw new Exception('Fail in register.', '67800');
            }
        } catch (Exception $e) {
            return ['code' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }

    public function update() {
        try {
            $user = new User(self::paramPost('id_user'));
            if (!empty(self::paramPost('password'))) {
                if ($user->password != $user->re_password) {
                    throw new Exception("The passwords don't are the same.", "32133");
                }
                $user->setState($_POST);
                $user->password = md5($user->password);
            } else {
                $user->setState($_POST);
                $user->password = $this->_user->password;
            }
            $user->profile_image = $this->saveProfileImage();
            $user->id_user = $user->update(['id_user' => $user->id_user]);
            if ($user->id_user > 0) {
                Session::setCurrentUser($user);
                return ['code' => 0, 'message' => 'Redirecting...', 'url' => "/"];
            } else {
                throw new Exception('Fail in register.', '67800');
            }
        } catch (Exception $e) {
            return ['code' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }

    public function modal_login() {
        if (file_exists('./partial/_modal_login.php')) {
            include './partial/_modal_login.php';
        } else {
            echo 'Ocorreu um problema.';
        }
    }

    public function modal_register() {
        if (file_exists('./partial/_modal_register.php')) {
            include './partial/_modal_register.php';
        } else {
            echo 'Ocorreu um problema.';
        }
    }

    public function modal_update() {
        $current_user = $this->_user;
        if (file_exists('./partial/_modal_update.php')) {
            include './partial/_modal_update.php';
        } else {
            echo 'Ocorreu um problema.';
        }
    }

    public function login() {
        try {
            $user = new User();
            $email = self::paramPost("email", TRUE);
            $password = md5(self::paramPost("password"));
            if ($user->validateUser($email, $password)) {
                $user = $user->retrive(['email' => $email]);
            } else {
                throw new Exception("Este Usuário é Inválido.", 23442);
            }
            if ($user instanceof User) {
                Session::setCurrentUser($user);
                return ['code' => 0, 'message' => 'Redirecting...', 'url' => '/'];
            } else {
                throw new Exception("Este Usuário é Inválido.", 23442);
            }
        } catch (Exception $e) {
            if ($this->_render == parent::RENDER_JSON) {
                return ['code' => $e->getCode(), 'message' => $e->getMessage()];
            } else {
                Session::setError($e->getCode(), $e->getMessage());
                throw $e;
            }
        }
    }

    /**
     * Muda o get de idioma
     */
    protected function change_language() {
        switch (self::paramGet('language')) {
            case 1:
                $_SESSION[I18N::SESSION_LANGUAGE_PREFERENCE] = 'pt_BR';
                break;
            case 2:
                $_SESSION[I18N::SESSION_LANGUAGE_PREFERENCE] = 'en_US';
                break;
            case 3:
                $_SESSION[I18N::SESSION_LANGUAGE_PREFERENCE] = 'en_US';
                break;
            case 4:
                $_SESSION[I18N::SESSION_LANGUAGE_PREFERENCE] = 'en_US';
                break;
            default:
                $_SESSION[I18N::SESSION_LANGUAGE_PREFERENCE] = 'pt_BR';
                break;
        }
        header("Location: " . $_SERVER['HTTP_REFERER']);
    }

    public function logout() {
        Session::destroy();
        header("Location: /");
        exit();
    }

    public function change_password() {
        try {
            if (parent::method('POST')) {
                $request = new UserRequest();
                if ($request->find(self::paramPost('code'))) {
                    $user = new User($request->id_user);
                    $pass = md5(self::paramPost('password'));
                    $repeat = md5(self::paramPost('re_password'));
                    if ($pass != $repeat) {
                        throw new Exception('As senhas informadas não conferem', self::ERROR_CODE_PREFIX . '56');
                    }
                    $res = $user->updatePassword($pass, $user->id_user);
                    if ($res >= 0) {
                        return ['code' => 0, 'message' => 'Sua senha foi atualizada com sucesso, realize o login.'];
                    } else {
                        throw new Exception('Nova senha não registrada', self::ERROR_CODE_PREFIX . '55');
                    }
                } else {
                    throw new Exception('Código de recuperação inválido', self::ERROR_CODE_PREFIX . '54');
                }
            } else {
                throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . '10');
            }
        } catch (Exception $e) {
            return ['code' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }

    private function saveProfileImage() {
        try {
            if (FileHelper::isFileSent('profile_image')) {
                $file = $_FILES['profile_image'];
                FileHelper::getErrorFile($file);
                $format = FileHelper::checkFormatImage($file);
                $path = FileHelper::ensureDirectoryExistence('images');
                $value = 'profile_image_' . str_replace(' ', '', $this->_user->name) . $format;
                FileHelper::uploadFile($file, $path, $value);
                return '/images/' . $value;
            } else {
                return $this->_user->profile_image;
            }
        } catch (Exception $e) {
            Logger::logFor('FileHelper', $e->getMessage());
            throw $e;
        }
    }
}
