<?php

require_once './controller/ControllerBase.php';

class EquipmentController extends ControllerBase {

    public function index() {
        $equipment = new Equipment();
        $list = $equipment->getList();
        self::render('./view/equipment/equipment_index.php', ['equipment' => $equipment, 'list' => $list]);
    }

    public function info() {
        if (parent::method('GET', TRUE)) {
            $equipment = new Equipment(self::paramGet('id', TRUE));
            if ($this->_render == parent::RENDER_PARTIAL) {
                if (file_exists('./view/equipment/_info_equipment.php')) {
                    include './view/equipment/_info_equipment.php';
                }
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . '10');
        }
    }

    public function modal_register() {
        if ($this->denyRender(parent::RENDER_VIEW) && parent::method('GET', TRUE)) {
            $equipment = new Equipment();
            if ($this->_render == parent::RENDER_PARTIAL) {
                if (file_exists('./view/equipment/equipment_register.php')) {
                    include './view/equipment/equipment_register.php';
                } else {
                    echo 'There was a problem fetching the data.';
                }
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . 01);
        }
    }

    public function modal_edit() {
        if ($this->denyRender(parent::RENDER_VIEW) && parent::method('GET', TRUE)) {
            $equipment = new Equipment(self::paramGet('equipment', TRUE));
            if ($this->_render == parent::RENDER_PARTIAL) {
                if (file_exists('./view/equipment/equipment_edit.php')) {
                    include './view/equipment/equipment_edit.php';
                } else {
                    echo 'There was a problem fetching the data.';
                }
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . 01);
        }
    }

    public function create() {
        try {
            $equipment = new Equipment();
            $equipment->setState($_POST);
            if ($equipment->create()) {
                return ['code' => 0, 'message' => 'Successfully registered'];
            } else {
                throw new Exception('Falha ao registrar', self::ERROR_CODE_PREFIX . 02);
            }
        } catch (Exception $e) {
            return ['code' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }

    public function update() {
        try {
            $equipment = new Equipment(self::paramPost('id_equipment'));
            $equipment->setState($_POST);
            if ($equipment->update(['id_equipment' => $equipment->id_equipment]) >= 0) {
                return ['code' => 0, 'message' => 'Successfully registered'];
            } else {
                throw new Exception('Falha ao atualizar', self::ERROR_CODE_PREFIX . 02);
            }
        } catch (Exception $e) {
            return ['code' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }

}
