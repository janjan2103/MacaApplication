<?php

require_once './helpers/TableHelper.php';

/**
 * Define os métodos que os controllers filhos tem a obrigação de implementar
 */
interface controllerMethods {

    /**
     * Método que é acessado ao realizar uma chamada para o controller sem especificar uma ação
     * @param void 
     * @return mixed Retornar um array ou uma string, de acordo com a opção _render
     */
    function index();
}

/**
 * Base para implemetação de controllers de rota única.
 * No caso de necessidade de utilizar outra rota, basta não construir o objeto, 
 * tratando a requisição em um método estático
 */
class ControllerBase implements controllerMethods {

    const ERROR_CODE_PREFIX = '80';
    const RENDER_JSON = 'json';
    const RENDER_VIEW = 'view';
    const RENDER_PARTIAL = 'partial';

    protected $_user;
    protected $_action;
    protected $_render;
    protected $_redirect = NULL;
    protected $_supress_error = FALSE;

    /**
     * Ao construir o objeto o parametro identificado pelo AltoRouter será analizado 
     * e identificará a ação a ser chamada de acordo com o parâmetro [:action].
     * Se a ação (método) retornar alguma coisa, será renderizado o retorno de acordo 
     * com a opção definida em [:render]
     * @param array $route Variavel $match definida pelo AltoRouter em index.php no formato: '/[:event]/tickets/?[:action]?/?.[:render]?'
     */
    public function __construct($route = NULL) {
        try {
            ob_start();
            //Obtem usuário
            $this->_user = Session::currentUser();
            date_default_timezone_set('America/Sao_Paulo');

            //Obtem o método a ser acessado
            $this->_action = array_key_exists('action', $route['params']) && $route['params']['action'] ? $route['params']['action'] : 'index';
            $this->_render = array_key_exists('render', $route['params']) ? $route['params']['render'] : self::RENDER_VIEW;
            $this->callAction($this->_action);
            ob_end_flush();
        } catch (Exception $ex) {
            Logger::log_exception($ex);
            if ($ex instanceof PDOException) {
                $message = 'Ocorreu um erro!';
                $code = $ex->getCode();
            } elseif ($ex->getCode() > 5000) {
                $message = $ex->getMessage();
                $code = $ex->getCode();
            } else {
                $message = $ex->getMessage();
                $code = $ex->getCode();
                Session::unsetMessages();
                $this->_redirect = '/ops/';
            }
            switch ($this->_render) {
                case self::RENDER_JSON:
                    header("HTTP/1.0 500 " . json_encode($message));
                    echo json_encode([
                        'code' => $code,
                        'message' => $message,
                        'render' => $this->_render,
                        'action' => $this->_action,
                    ]);
                    break;
                case self::RENDER_VIEW:
                    Session::setError($code, $message);
                    if (!empty($this->_redirect)) {
                        header("Location: {$this->_redirect}");
                    } elseif (empty($_SERVER['HTTP_REFERER'])) {
                        self::render('./error.php');
                    } else {
                        header("Location: {$_SERVER['HTTP_REFERER']}");
                    }
                    break;
                case self::RENDER_PARTIAL:
                default:
                    header("HTTP/1.0 500 " . json_encode($message));
            }
        }
    }

    /**
     * Realiza a chamada do método que representa a ação renderizando, se possível, o que for retornado
     * @param string $action Ação requisitada
     * @param string $render_type Forma de renderização [http, json]
     * @return boolean
     */
    private function callAction($action) {
        if (method_exists($this, $action)) {
            $data = $this->$action();
            $data || die();
            switch ($this->_render) {
                case self::RENDER_JSON:
                    ob_clean();
                    if (is_array($data)) {
                        echo json_encode($data);
                    } elseif (is_object($data)) {
                        echo json_encode($data);
                    } elseif (is_string($data)) {
                        echo $data;
                    }
                    break;
                case self::RENDER_VIEW:
                    if (is_string($data)) {
                        $this->render($data);
                    }
                    break;
                case self::RENDER_PARTIAL:
                default:
                    if (is_string($data)) {
                        echo $data;
                    }
            }
        } else {
            throw new Exception('Ação inválida', self::ERROR_CODE_PREFIX . 11);
        }
    }

    /**
     * Verifica se o usuário corrente é participante
     * @return boolean Usuário é instancia de User!
     */
    protected function isUser() {
        return $this->_user instanceof User;
    }

    /**
     * Verifica se o usuário corrente é participante
     * @return boolean Usuário é instancia de User!
     */
    protected function isParticipant() {
        return $this->_user instanceof Participant;
    }

    /**
     * Se o usuário corrente não for participante dispara exceção
     * @param string $redirect Link para redirecionamento para o caso de exceção
     * @return boolean Usuário é instancia de User!
     * @throws Exception
     */
    protected function requireUserSession($redirect = NULL) {
        if ($this->_user instanceof User) {
            return TRUE;
        } else {
            $this->_redirect = routeFor('/login/');
            throw new Exception('Sessão inválida, por favor realize o login para continuar.', self::ERROR_CODE_PREFIX . '10');
        }
    }

    protected function change_language() {
        switch (self::paramGet('language')) {
            case 1:
                $_SESSION[I18N::SESSION_LANGUAGE_PREFERENCE] = 'pt_BR';
                break;
            case 2:
                $_SESSION[I18N::SESSION_LANGUAGE_PREFERENCE] = 'en_US';
                break;
            case 3:
                $_SESSION[I18N::SESSION_LANGUAGE_PREFERENCE] = 'en_US';
                break;
            case 4:
                $_SESSION[I18N::SESSION_LANGUAGE_PREFERENCE] = 'en_US';
                break;
            default:
                $_SESSION[I18N::SESSION_LANGUAGE_PREFERENCE] = 'pt_BR';
                break;
        }
        header("Location: " . $_SERVER['HTTP_REFERER']);
    }

    /**
     * Checa se o usuário corrente é Administrador
     * @param array $credentials Credenciais de administrador requeridas
     * @return boolean Usuário é instancia de Admin
     */
    protected function isAdmin($credentials = []) {
        return $this->_user instanceof Admin && $this->_user->validateCredentials($credentials);
    }

    /**
     * Se o usuário corrente não for Administrador dispara exceção
     * @param array $credentials Credenciais de administrador requeridas
     * @return boolean Usuário é instancia de Admin
     * @throws Exception
     */
    protected function requireAdminSession($credentials = []) {
        if ($this->isUser($credentials)) {
            return TRUE;
        } else {
            $this->_redirect = routeFor('/login/');
            throw new Exception("Você não tem credenciais necessárias para acessar essa página.", 3223);
        }
    }

    /**
     * Verifica se o método de requisição
     * @param string $method 'POST'|'GET'
     * @param boolean $required TRUE para disparar exceção
     * @return boolean
     */
    protected static function method($method, $required = FALSE) {
        $res = $_SERVER['REQUEST_METHOD'] == $method;
        if (!$res && $required) {
            throw new Exception('Requisição inválida', self::ERROR_CODE_PREFIX . 12);
        } else {
            return $res;
        }
    }

    /**
     * Obtem o dado recebido por parâmetro GET ou POST
     * @param string $param chave do parametro
     * @param boolean $required TRUE para disparar excessão
     * @return boolean
     */
    protected static function paramAny($param, $required = FALSE) {
        if (array_key_exists($param, $_POST)) {
            return $_POST[$param];
        } elseif (array_key_exists($param, $_GET)) {
            return $_GET[$param];
        } elseif ($required) {
            throw new Exception("Os dados enviados não são válidos. Necessário informar \"$param\"", self::ERROR_CODE_PREFIX . 12);
        } else {
            return NULL;
        }
    }

    /**
     * Obtem o dado recebido por parâmetro POST
     * @param string $param chave do parametro
     * @param boolean $required TRUE para disparar excessão
     * @return boolean
     */
    protected static function paramPost($param, $required = FALSE) {
        if (array_key_exists($param, $_POST)) {
            return $_POST[$param];
        } elseif ($required) {
            throw new Exception("Os dados enviados não são válidos. Necessário informar \"$param\". ", self::ERROR_CODE_PREFIX . 12);
        } else {
            return NULL;
        }
    }

    /**
     * Obtem o dado recebido por parâmetro GET
     * @param string $param chave do parametro
     * @param boolean $required TRUE para disparar excessão
     * @return boolean
     */
    protected static function paramGet($param, $required = FALSE) {
        if (array_key_exists($param, $_GET)) {
            return $_GET[$param];
        } elseif ($required) {
            throw new Exception("Os dados enviados não são válidos. Necessário informar \"$param\".", self::ERROR_CODE_PREFIX . 12);
        } else {
            return NULL;
        }
    }

    /**
     * Renderiza uma view
     * @param string $path caminho para o arquivo, exemplo './view/login.php'
     * @param array $options array para transporte de dados entre o escopo da ação e a view ex: ['user' => $user]
     * @return void
     */
    protected static function render($path, array $options = []) {
        foreach ($options as $key => $option) {
            if (is_string($key)) {
                $$key = $option;
            } else {
                throw new Exception('Erro: Parametro inválido', self::ERROR_CODE_PREFIX . 12);
            }
        }
        if (Session::isLoged()) {
            $current_user = Session::currentUser();
        } else {
            $current_user = new User();
        }
        if (file_exists($path)) {
            include $path;
        } else {
            include './404.php';
        }
        exit();
    }

    /**
     * Interrompe a execução retornado uma string como texto. Opções:  ['view', 'json', 'partial']
     * @param mixed $options String da forma proibida ou um array de formas proíbidas
     */
    protected function denyRender($options) {
        $stop = FALSE;
        if (is_array($options)) {
            foreach ($options as $key => $option) {
                $stop = !$stop || $this->_render == $option;
            }
        } else {
            $stop = $this->_render == $options;
        }
        $code = self::ERROR_CODE_PREFIX . 13;
        $message = "Não é possível executar a ação";
        if ($stop) {
            $this->_render = 'undefined';
            throw new Exception($message, $code);
        }
        return TRUE;
    }

    /**
     * Redireciona utilizando HEADER
     * @param string $route Rota para o arquivo, exemplo './view/login.php'
     * @param string $message (Opcional) Mensagem de sucesso para ser definida em Sessão 
     * @return void
     */
    protected static function redirect($route, $message = NULL) {
        if ($route === 'back' && array_key_exists('HTTP_REFERER', $_SERVER) && $_SERVER['HTTP_REFERER']) {
            $route = $_SERVER['HTTP_REFERER'];
        }
        if (!empty($message) && is_string($message)) {
            Session::setMessage($message);
        }
        ob_clean();
        header("Location: " . $route);
        exit();
    }

    public function index() {
        throw new Exception('Acesso a esta ação não é permitido', self::ERROR_CODE_PREFIX . 66);
    }

}
