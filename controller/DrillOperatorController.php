<?php

require_once './controller/ControllerBase.php';

class DrillOperatorController extends ControllerBase {

    public function index() {
        $drill_operator = new DrillOperator();
        $list = $drill_operator->getList();
        self::render('./view/drill_operator/drill_operator_index.php', ['drill_operator' => $drill_operator, 'list' => $list]);
    }

    public function info() {
        if (parent::method('GET', TRUE)) {
            $drill_operator = new DrillOperator(self::paramGet('id', TRUE));
            if ($this->_render == parent::RENDER_PARTIAL) {
                if (file_exists('./view/drill_operator/_info_drill_operator.php')) {
                    include './view/drill_operator/_info_drill_operator.php';
                }
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . '10');
        }
    }

    public function modal_register() {
        if ($this->denyRender(parent::RENDER_VIEW) && parent::method('GET', TRUE)) {
            $drill_operator = new DrillOperator();
            if ($this->_render == parent::RENDER_JSON) {
                json_encode($drill_operator);
            } elseif (file_exists('./view/drill_operator/drill_operator_register.php')) {
                include './view/drill_operator/drill_operator_register.php';
            } else {
                echo 'There was a problem fetching the data.';
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . 01);
        }
    }

    public function modal_edit() {
        if ($this->denyRender(parent::RENDER_VIEW) && parent::method('GET', TRUE)) {
            $drill_operator = new DrillOperator(self::paramGet('drill', TRUE));
            if ($this->_render == parent::RENDER_JSON) {
                json_encode($drill_operator);
            } elseif (file_exists('./view/drill_operator/drill_operator_edit.php')) {
                include './view/drill_operator/drill_operator_edit.php';
            } else {
                echo 'There was a problem fetching the data.';
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . 01);
        }
    }

    public function create() {
        try {
            $drill_operator = new DrillOperator();
            $drill_operator->setState($_POST);
            if ($drill_operator->create()) {
                return ['code' => 0, 'message' => 'Successfully registered'];
            } else {
                throw new Exception('Falha ao registrar', self::ERROR_CODE_PREFIX . 02);
            }
        } catch (Exception $e) {
           return ['code' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }

    public function update() {
        try {
            $drill_operator = new DrillOperator(self::paramPost('id_drill_operator'));
            $drill_operator->setState($_POST);
            if ($drill_operator->update(['id_drill_operator' => $drill_operator->id_drill_operator]) >= 0) {
                return ['code' => 0, 'message' => 'Successfully registered'];
            } else {
                throw new Exception('Falha ao atualizar', self::ERROR_CODE_PREFIX . 02);
            }
        } catch (Exception $e) {
           return ['code' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }
}
