<?php

require_once './controller/ControllerBase.php';

class TypeHolesController extends ControllerBase {

    public function index() {
        $type_holes = new TypeOfHoles();
        $list = $type_holes->getList();
        self::render('./view/type_holes/type_holes_index.php', ['type_holes' => $type_holes, 'list' => $list]);
    }

    public function info() {
        if (parent::method('GET', TRUE)) {
            $type_holes = new TypeOfHoles(self::paramGet('id', TRUE));
            if ($this->_render == parent::RENDER_PARTIAL) {
                if (file_exists('./view/type_holes/_info_type_holes.php')) {
                    include './view/type_holes/_info_type_holes.php';
                }
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . '10');
        }
    }

    public function modal_register() {
        if ($this->denyRender(parent::RENDER_VIEW) && parent::method('GET', TRUE)) {
            $type_holes = new TypeOfHoles();
            if ($this->_render == parent::RENDER_JSON) {
                json_encode($type_holes);
            } elseif (file_exists('./view/type_holes/type_holes_register.php')) {
                include './view/type_holes/type_holes_register.php';
            } else {
                echo 'There was a problem fetching the data.';
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . 01);
        }
    }

    public function modal_edit() {
        if ($this->denyRender(parent::RENDER_VIEW) && parent::method('GET', TRUE)) {
            $type_holes = new TypeOfHoles(self::paramGet('holes', TRUE));
            if ($this->_render == parent::RENDER_JSON) {
                json_encode($type_holes);
            } elseif (file_exists('./view/type_holes/type_holes_edit.php')) {
                include './view/type_holes/type_holes_edit.php';
            } else {
                echo 'There was a problem fetching the data.';
            }
        } else {
            throw new Exception('Method not allowed', self::ERROR_CODE_PREFIX . 01);
        }
    }

    public function create() {
        try {
            $type_holes = new TypeOfHoles();
            $type_holes->setState($_POST);
            if ($type_holes->create()) {
                return ['code' => 0, 'message' => 'Successfully registered'];
            } else {
                throw new Exception('Falha ao registrar', self::ERROR_CODE_PREFIX . 02);
            }
        } catch (Exception $e) {
           return ['code' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }

    public function update() {
        try {
            $type_holes = new TypeOfHoles(self::paramPost('id_type_holes'));
            $type_holes->setState($_POST);
            if ($type_holes->update(['id_type_holes' => $type_holes->id_type_holes]) >= 0) {
                return ['code' => 0, 'message' => 'Successfully registered'];
            } else {
                throw new Exception('Falha ao atualizar', self::ERROR_CODE_PREFIX . 02);
            }
        } catch (Exception $e) {
           return ['code' => $e->getCode(), 'message' => $e->getMessage()];
        }
    }
}
