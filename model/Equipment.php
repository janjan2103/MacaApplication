<?php

class Equipment extends Crud {
    
    const ERROR_CODE_PREFIX = 60;

    public $id_equipment;
    public $equipment;
    public $company;
    public $type;
    public $fleet;
    public $status;
    protected $_table = "EQUIPMENT";
    protected $_key_field = 'id_equipment';
    
    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($id = NULL) {
        parent::__construct();
        if ($id > 0) {
            $this->find($id);
        }
    }

    static public function getDescriptionKey($key) {
        try {
            switch ($key) {
                case 'id_equipment':
                    return 'ID';
                case 'company':
                    return 'Company';
                case 'type':
                    return 'Type';
                default :
                    return $key;
            }
        } catch (Exception $e) {
            return NULL;
        }
    }

    public function find($id) {
        if (parent::find([$this->_key_field => $id])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function setState($data) {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function getList($rows = 1000) {
        try {
            $stmt = $this->_db->prepare("SELECT *, $this->_key_field AS `options` FROM `$this->_table` ORDER BY `$this->_key_field` ASC LIMIT $rows");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $e) {
            Logger::logFor(__CLASS__ . '-list', $e->getTraceAsString());
            return NULL;
        }
    }
    
    public static function getBadge($equipment) {
        $id = (new Equipment())->read(['equipment' => $equipment])[0]['id_equipment'];
        return '<span class="badge ' . self::badges($id) . '">' . $equipment . '</span>';
    }
    
    public static function badges($id) {
        switch ($id) {
            case 1:
                return 'badge-info';
            case 2:
                return 'badge-primary';
            case 3:
                return 'badge-danger';
            case 4:
                return 'badge-warning';
            case 5:
                return 'badge-default';
            case 5:
                return 'badge-success';
            default:
                return 'badge-default';
        }
    }
}

?>
