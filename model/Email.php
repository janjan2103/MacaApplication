<?php

class Email {

    const LOGO_EMAIL = '';
    const SITE = '';
    const DEFAULT_SERVICE = 1;
    const SENDGRID_SERVICE = 1;
    const MANDRIL_SERVICE = 2;
    const TYPE_REGISTER = 1;
    const TYPE_REQUEST_ACTIVE = 2;
    const TYPE_REQUEST_PASSWORD = 3;
    const RESPONSE_TO_USER = 4;
    const PLURAL_DESC = "emails";
    const SINGULAR_DESC = "email";
    const SENDDGRID_USER = '';
    const SENDDGRID_KEY = '';
    const MANDRILL_USER = '';
    const MANDRILL_KEY = '';
    const CONTACT_EMAIL = '';
    const ADMIN_EMAIL = '';
    const TEMPLATE_DEFAULT = './email/_email_default.html';
    const ACTIVE_ACCOUNT = '';
    const REQUEST_PASSWORD = '';
    const LOGO_IMAGE = "";

    protected $from = Email::CONTACT_EMAIL;
    protected $to;
    protected $subject;
    protected $html;
    protected $text;
    private $content;

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    public function sendEmail($service = Email::DEFAULT_SERVICE) {
        if ($service == Email::SENDGRID_SERVICE) {
            return $this->sendSendgridEmail();
        } elseif ($service == Email::MANDRIL_SERVICE) {
            return $this->sendMandrilEmail();
        }
    }

    public function loadTemplate($template_file = self::TEMPLATE_DEFAULT) {
		$date_event = '';
		$event_title = '';
		$email = self::CONTACT_EMAIL;
		$location = "";
		$phone = "";
		$link = self::SITE;
        $content = file_get_contents($template_file);
        $default = array("{logo}", "{event_title}", "{content}", "{date_event}", "{contact_email}", "{location}", "{contact_phone}", "{link}");
        $param = array(self::LOGO_EMAIL, $event_title, $this->content, $date_event, $email, $location, $phone, $link);
        return str_replace($default, $param, $content);
    }

    private function sendSendgridEmail() {
        try {
            $url = 'https://api.sendgrid.com/';
            $params = array(
                'api_user' => Email::SENDDGRID_USER,
                'api_key' => Email::SENDDGRID_KEY,
                'to' => $this->to,
                'subject' => $this->subject,
                'html' => $this->html,
                'text' => $this->text,
                'from' => $this->from
            );

            $request = $url . 'api/mail.send.json';

// Generate curl request
            $session = curl_init($request);
// Tell curl to use HTTP POST
            curl_setopt($session, CURLOPT_POST, TRUE);
// Tell curl that this is the body of the POST
            curl_setopt($session, CURLOPT_POSTFIELDS, $params);
// Tell curl not to return headers, but do return the response
            curl_setopt($session, CURLOPT_HEADER, FALSE);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, TRUE);
// Turn OFF the SSL verification
            curl_setopt($session, CURLOPT_SSL_VERIFYPEER, FALSE);
// obtain response
            $response = curl_exec($session);
            if (curl_errno($session)) {
                echo 'error:' . curl_error($session);
                Logger::logFor('LOG_ERROR_EMAIL', $response);
//                
            }
            curl_close($session);
            return $response;
        } catch (Exception $exc) {
            var_dump($exc);
            echo $exc->getTraceAsString();
            return NULL;
        }
    }

    private function sendMandrillEmail() {
        
    }

    public function sendRequestEmail($to, $subject, $html) {
        /**
         * Método responsável valorizar os atributos da classe Email e enviar o email
         */
        $this->to = $to; //Valorizando o atributo to (remetente)
        $this->subject = $subject; //Valorizando o atributo subject (Assunto)
        $this->html = $html; //Valorizando o atributo html (mensagem)

        $res = $this->sendEmail(); //envio do email

        if ($res) { // teste de resposta de envio
            return TRUE;
        } else {
            return FALSE;
            throw new Exception('Falha ao enviar email.', '1100');
        }
    }
}

?>
