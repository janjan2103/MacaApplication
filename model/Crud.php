<?php

class CrudException extends Exception {
    
    public function __construct($message, $code = 0, Exception $previous = null, $exception_type = NULL) {
        parent::__construct($message, $code, $previous);
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}

class Crud {
    /**
     * 
     **/
    const DB_NAME = 'macaapplication';
    const DB_USER = 'root';
    const DB_PASS = '';
    
    protected $_db;

    public function __construct() {
        try {
            $host = 'localhost';
            $data_base = self::DB_NAME;
            $user = self::DB_USER;
            $password = self::DB_PASS;
            $colation = 'utf8';
            $this->_db = new PDO("mysql:host=$host;dbname=$data_base", "$user", "$password");
            $this->_db->exec("set names $colation");
        } catch (Exception $e) {
            Logger::log_exception($e, __CLASS__, __METHOD__, 'DB_Connection');
            throw new Exception('Houve um problema ao contectar com banco de dados', '5001', $e);
        }
    }

    public function __desctruct() {
        $this->_db = null;
    }

    public function disconnectDB() {
        $this->_db = null;
    }

    public function persist($removing = FALSE) {
        foreach ($this->_keys as $key) {
            $keys[$key] = $this->$key;
        }
        if ($removing) {
            return $this->delete($keys);
        } else {
            if ($this->persisted($keys)) {
                return $this->update($keys);
            } else {
                $id = $this->create();
                return $id == 0 ? 1 : 0;
            }
        }
    }

    public function create($key = NULL, $value = NULL) {
        try {
            $data = get_class_vars(get_class($this));
            foreach ($data as $key => $value) {
                if ($key[0] != '_') {
                    $fields[] = "`" . $key . "`";
                    $values[] = $this->clear($this->$key);
                }
            }
            $fields = implode(", ", $fields);
            $values = $this->getCreateValues($values);
            $insert = "INSERT INTO {$this->_table} ({$fields}) VALUES ({$values})";
            $stmt = $this->_db->prepare("$insert", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            $id = NULL;
            $this->_db->setAttribute(PDO::ATTR_AUTOCOMMIT, 0);
            $this->_db->beginTransaction();
            if ($stmt->execute() && $stmt->rowCount() > 0) {
                $id = $this->_db->lastInsertId();
                $this->_db->commit();
                return $id;
            } else {
                $error = $stmt->errorInfo();
                Logger::log_array(__CLASS__, $error);
                throw new Exception($stmt->errorInfo()[2], $stmt->errorInfo()[1]);
            }
        } catch (PDOExecption $e) {
            Logger::log_array(__CLASS__, $e);
            $this->_db->rollback();
            throw $e;
        } finally {
            if($this->_db->inTransaction()) {
                $this->_db->commit();
            }
            $this->_db->setAttribute(PDO::ATTR_AUTOCOMMIT, 1);
        }
    }

    public function createMultiple($data_fileds) {
        $all_values = '';
        $detail_id = 1;
        try {
            foreach ($data_fileds as $key => $data) {
                $fields = array();
                $values = array();
                foreach (get_class_vars(get_class($this)) as $key => $value) {
                    if ($key == $this->_key_field || $key == 'id') {
                        $fields[] = "`" . $key . "`";
                        $values[] = $detail_id;
                        $detail_id += 1;
                    } elseif ($key[0] != '_') {
                        $fields[] = "`" . $key . "`";
                        $values[] = $this->clear((empty($data[$key]) ? $this->$key : $data[$key]));
                    }
                }
                $sql = $this->getCreateValues($values);
                $all_values .= "({$sql}), ";
            }
            $sql_values = substr_replace($all_values, '', -2, 2);
            $sql_fields = implode(", ", $fields);
            $insert = "INSERT INTO {$this->_table} ({$sql_fields}) VALUES {$sql_values}";
            $stmt = $this->_db->prepare("$insert", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            $this->_db->setAttribute(PDO::ATTR_AUTOCOMMIT, 0);
            $this->_db->beginTransaction();
            $rows = NULL;
            if ($stmt->execute()) {
                $rows = $stmt->rowCount();
                $this->_db->commit();
                return $rows;
            } else {
                $this->_db->rollback();
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                return NULL;
            }
        } catch (PDOExecption $e) {
            Logger::log_array(__CLASS__, $e);
            $this->_db->rollback();
            throw $e;
        }
    }

    public function updateAsDetails($data_fileds) {
        $all_values = '';
        $detail_id = 1;
        try {
            foreach ($data_fileds as $key => $data) {
                $fields = array();
                $values = array();
                foreach (get_class_vars(get_class($this)) as $key => $value) {
                    if ($key == $this->_key_field || $key == 'id') {
                        $fields[] = $key;
                        $values[] = $detail_id;
                        $detail_id += 1;
                    } elseif ($key[0] != '_') {
                        $fields[] = $key;
                        $values[] = $this->clear((empty($data[$key]) ? $this->$key : $data[$key]));
                    }
                }
                $sql = $this->getCreateValues($values);
                $all_values .= "({$sql}), ";
            }
            $sql_values = substr_replace($all_values, '', -2, 2);
            $sql_fields = implode(", ", $fields);
            $insert = "INSERT INTO {$this->_table} ({$sql_fields}) VALUES {$sql_values}";
            $stmt = $this->_db->prepare("$insert", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            $this->_db->setAttribute(PDO::ATTR_AUTOCOMMIT, 0);
            $this->_db->beginTransaction();
            $rows = NULL;
            if ($stmt->execute()) {
                $rows = $stmt->rowCount();
                $this->_db->commit();
                return $rows;
            } else {
                $this->_db->rollback();
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                return NULL;
            }
        } catch (PDOExecption $e) {
            Logger::log_array(__CLASS__, $e);
            $this->_db->rollback();
            throw $e;
        }
    }

    public function createDB($db_name) {
        try {
            $sql = "CREATE SCHEMA IF NOT EXISTS `$db_name` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;"
                    . " USE `$db_name`;";
            $stmt = $this->_db->prepare("$sql", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            if ($stmt->execute()) {
                return TRUE;
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
            }
        } catch (PDOExecption $e) {
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function read($filter = null) {
        try {
            $where = $this->mountWhere($filter);

            $select = "SELECT * FROM `{$this->_table}` {$where} ORDER BY 1";
            $stmt = $this->_db->prepare("$select", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC); 
                return $stmt->fetchAll();
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
            }
        } catch (PDOExecption $e) {
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function persisted($filter = null) {
        try {
            $where = $this->mountWhere($filter);

            $select = "SELECT COUNT(*) as count FROM `{$this->_table}` {$where}";
            $stmt = $this->_db->prepare("$select", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            if ($stmt->execute()) {
                return $stmt->fetch()['count'] > 0;
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                return NULL;
            }
        } catch (PDOExecption $e) {
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function readView($filter = null) {
        try {
            $where = $this->mountWhere($filter);

            $select = "SELECT * FROM $this->_list $where";
            $stmt = $this->_db->prepare("$select", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
            }
        } catch (PDOExecption $e) {
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function readNView($view, $filter = null) {
        try {
            $where = $this->mountWhere($filter);

            $select = "SELECT * FROM $view $where";
            $stmt = $this->_db->prepare("$select", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
            }
        } catch (PDOExecption $e) {
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function find($filter) {
        try {
            $row_data = $this->read($filter);
            if (count($row_data) > 0) {
                $class_attr = get_class_vars(get_class($this));
                foreach ($row_data[0] as $key => $value) {
                    $this->$key = $value;
                }
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (Exception $e) {
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    protected function getSqlValue($value) {
        try {
            switch ($value) {
                case is_int($value):
                case is_double($value):
                    return $value;
                case is_array($value):
                    throw new Exception('Inválido');
                case empty($value):
                    return 'NULL';
                case is_string($value):
                default:
                    return "'{$this->clear($value)}'";
            }
        } catch (Exception $e) {
            Logger::log_exception($e);
            throw $e;
        }
    }

    public function retrive($filter) {
        try {
            $where = $this->mountWhere($filter);

            $select = "SELECT * FROM {$this->_table} {$where} ORDER BY 1";
            $stmt = $this->_db->prepare("$select", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_CLASS, get_class($this));
                return $stmt->fetchObject(get_class($this));
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                return NULL;
            }
        } catch (PDOExecption $e) {
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function retriveAll($filter) {
        try {
            $where = $this->mountWhere($filter);

            $select = "SELECT * FROM {$this->_table} {$where} ORDER BY 1";
            $stmt = $this->_db->prepare("$select", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_CLASS, get_class($this));
                return $stmt->fetchAll();
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                return NULL;
            }
        } catch (PDOExecption $e) {
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function update($filter = NULL, $fields = NULL) {
        try {
            $where = $this->mountWhere($filter);
            if ($fields != NULL) {
                foreach ($fields as $key => $value) {
                    if ($key[0] != '_' && array_key_exists($key, get_class_vars(get_class($this)))) {
                        $values[] = "`$key` = {$this->getValueForSql($value)}";
                    }
                }
            } else {
                foreach (get_class_vars(get_class($this)) as $key => $value) {
                    if ($key[0] != '_') {
                        $values[] = "`$key` = {$this->getValueForSql($this->$key)}";
                    }
                }
            }
            $values = implode(", ", $values);
            substr_replace($values, '', 0, 2);
            $update = "UPDATE `{$this->_table}` SET {$values} {$where}";
            $stmt = $this->_db->prepare("$update", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            $this->_db->setAttribute(PDO::ATTR_AUTOCOMMIT, 0);
            $this->_db->beginTransaction();
            if ($stmt->execute()) {
                $this->_db->commit();
                return $stmt->rowCount();
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                throw new Exception($stmt->errorInfo()[2], $stmt->errorInfo()[1]);
            }
        } catch (PDOExecption $e) {
            $this->_db->rollback();
            throw $e;
        } finally {
            $this->_db->setAttribute(PDO::ATTR_AUTOCOMMIT, 1);
        }
    }

    public function updateManyToMany(Array $filter, Array $fields = NULL) {
        try {
            $where = $this->mountWhere($filter);
            if ($fields != NULL) {
                foreach ($fields as $key => $value) {
                    if ($key[0] != '_' && array_key_exists($key, get_class_vars(get_class($this)))) {
                        $values[] = "{$key} = {$this->getValueForSql($value)}";
                    }
                }
            } else {
                foreach (get_class_vars(get_class($this)) as $key => $value) {
                    if ($key[0] != '_') {
                        $values[] = "{$key} = {$this->getValueForSql($this->$key)}";
                    }
                }
            }
            
            $values = implode(", ", $values);
            substr_replace($values, '', 0, 2);
            $update = "UPDATE {$this->_table} SET {$values} {$where}";
            $stmt = $this->_db->prepare("$update", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            if ($stmt->execute()) {
                return $stmt->rowCount();
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
            }
        } catch (PDOExecption $e) {
            throw $e;
        } finally {
            
        }
    }

    public function delete(Array $filter) {
        try {
            $where = $this->mountWhere($filter); 
            foreach (get_class_vars(get_class($this)) as $key => $value) {
                if ($key[0] != '_') {
                    $fields[] = "{$key} = {$this->getValueForSql($this->$key)}";
                }
            }
            $fields = implode(", ", $fields); 
            substr_replace($fields, '', 0, 2); 
            $fields = "DELETE FROM {$this->_table} {$where}";
            $stmt = $this->_db->prepare("$fields", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            if ($stmt->execute()) {
                return $stmt->rowCount();
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                return NULL;
            }
        } catch (PDOExecption $e) {
            throw $e;
        }
    }

    protected function mountWhere($filter) {
        /* método responsável por montar a estrutura de filtros quando necessário
         * @param FILTER filtro de string
         * @return STRING $where string com filtro sql
         * */
        try {
            $where = "WHERE (1 = 1) "; //inicia a estrutura where para não haver conflitos
            if ($filter != null) { //verifica se existe filtro
                //monta a string que filtrará a busca
                foreach ($filter as $key => $value) {
                    if (strtolower(substr($key, 0, 2)) == 'id') {
                        $where = $where . " AND (`{$key}` = '$value')";
                    } else {
                        $where = $where . " AND (LOWER(`{$key}`) LIKE LOWER('{$value}'))";
                    }
                }
            }
            return $where; //retornando string do sql
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function getCreateValues($values) {
        /**
         * Obter valores do create
         * 
         * @param ARRAY $values array com valores
         * @return STRING string
         */
        try {
            $sql = ''; //inicia a variável sql
            //montar string do sql
            foreach ($values as $key => $value) {
                if (is_int($value) || is_double($value)) {
                    $sql .= $value . ', ';
                } elseif ($value == NULL || $value == '') {
                    $sql .= 'NULL, ';
                } else {
                    $sql .= "'$value', ";
                }
            }
            return substr_replace($sql, '', -2, 2); //retornando uma substring do sql
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function getValueForSql($value) {
        /* Médoto responsável por validar o valor recebido. identificando se é string, inteiro ou double
         * @param INDEFINIDO $value valor do array enviado
         * @return INDERFINIDO $value
         */
        try {
            if ($value == NULL || $value == '') { //verifica se é nulo ou string vazia
                return 'NULL'; //retorna null em string
            } else if (is_int($value) || is_double($value)) { //verifica se é interiro ou double
                return $value; //retorna o valor sem alterações
            } else {
                return "'{$this->clear($value)}'"; //retorna o valor como string
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function clear($string) {
        //$chars = array("'", ";", "!", "@", "#", "&", "$", ",", ".", "%", "\"", '\\');
        $chars = array("'", "--"); //especifica os caracteres que serão retirados da string
        return str_replace($chars, "", $string); //realiza a busca e limpeza da string
    }

    private function vl_string($string) {
        //$chars = array("'", ";", "!", "@", "#", "&", "$", ",", ".", "%", "\"", '\\');
        $chars = array("'", ";", '.', '-', '(', ')'); //especifica os caracteres que serão retirados da string
        return str_replace($chars, "", $string); //realiza a busca e limpeza da string
    }

    public function searchValue($field, $value, $id = NULL) {
        /*
         * Método para validação de dados do usuário
         * 
         * @param String $field Field campo para pesquisa
         * @param String $id Identificador para comparação
         * @param String $id Identificador para comparação
         * @return BOOL TRUE se uma ou mais ocorrencias encontradas
         */
        try {
            $where = $this->mountWhere(array($field => $value));

            $select = "SELECT * FROM {$this->_table} {$where} "
                    . (!empty($id) ? " AND $this->_key_field != $id" : "")
                    . " ORDER BY 1";
            $stmt = $this->_db->prepare("$select", array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            if ($stmt->execute()) {
                return $stmt->rowCount() > 0;
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                return FALSE;
            }
        } catch (PDOExecption $e) {
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function setState($state) {
        if (is_array($state)) {
            foreach ($state as $key => $value) {
                if (is_string($key)) {
                    $this->$key = $value;
                }
            }
        }
        return $this;
    }

}

/* Obter os valores e as chaves para dar um estado ao objeto*/
//        $class_attr = get_class_vars(get_class($this));
//        foreach ($row_data[0] as $key => $value) {
//            $this->$key = $value;
//        }