<?php

class PagseguroNotification extends Crud {

    protected $id_notification;
    protected $reference_id;
    protected $transaction_status;
    protected $transaction_status_desc;
    protected $notification_code;
    protected $transaction_code;
    protected $total_value;
    protected $discount_value;
    protected $extra_value;
    protected $liquid_value;
    protected $dtt_transaction;
    protected $dtt_lastevent;
    protected $payment_way_code;
    protected $payment_way_desc;
    protected $purchase_method_desc;
    protected $purchase_method_code;
    protected $installment_number;
    protected $cancelation_source;
    protected $_dtt_register;
    protected $_table = "PAGSEGURO_NOTIFICATION";
    protected $_list = 'lst_pagseguro_notification';
    protected $_key_field = 'id_pagseguro_notification';

    public function __sleep() {
        return array('id_notification', 'reference_id', 'transaction_status', 'transaction_status_desc',
            'notification_code', 'transaction_code', 'total_value', 'discount_value',
            'extra_value', 'liquid_value', 'dtt_transaction', 'dtt_lastevent', 'payment_way_code',
            'payment_way_desc', 'purchase_method_desc', 'purchase_method_code', 'installment_number'
        );
    }

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($id = NULL) {
        parent::__construct();
        if ($id > 0) {
            $this->find($id);
        }
    }

    public function logState() {
        Logger::log_array('notificationState', get_object_vars($this));
    }

    public function find($id) {
        if (parent::find(Array($this->_key_field => $id))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getSerialized($id = NULL) {
        $this->retriveAll(($id != NULL ? Array($this->_key_field => $id) : ''));
        $serialized = serialize($this);
        return $serialized;
    }

    public function create($k = NULL, $v = NULL) {
        if (!$this->persisted(['dtt_lastevent' => $this->dtt_lastevent, 'transaction_code' => $this->transaction_code])) {
            return parent::create();
        } else {
            return 0;
        }
    }

    public function __wakeup() {
        return $this;
    }

    static public function getStatusDescription($status) {
        try {
            switch ($status) {
                case 1 : return 'Aguardando pagamento';
                case 2 : return 'Pagamento em análise';
                case 3 :
                case 4 : return 'Pagamento registrado';
                case 5 : return 'Pagamento em disputa';
                case 6 : return 'Pagamento Devolvido';
                case 7 : return 'Pagamento Cancelado';
                default : return 'Realize o pagamento de sua inscrição';
            }
        } catch (Exception $exc) {
            return NULL;
        }
    }

    static public function getStatusInfo($status) {
        try {
            switch ($status) {
                case 1 : return 'O pagamento de sua inscrição ainda não foi registrado. Dependendo da forma de pagamento escolhida, o registro pode não ser imediato. '
                            . '<br/> Se você utilizou cartão de crédito, em algumas horas seu pagamento deve ser registrado.'
                            . '<br/> Em outras formas de pagamento, como boletos ou transferências bancárias, o período de registro do pagamento pode demorar até 3 dias .'
                            . '<br/> Se julgar que algum problema ocorreu no registro do pagamento de sua inscrição, por favor entre em contato para que os responsáveis pelo evento possam lhe ajudar.';
                case 2 : return 'O registro do seu pagamento foi identificado e está em análise, este processo pode demorar algumas horas.';
                case 3 : return 'Seu pagamento foi registrado. Agradecemos sua participação e esperamos que aprecie o evento.<br/> Não se esqueça de selecionar as atividades que deseja participar para garantir sua vaga.';
                case 4 : return 'Seu pagamento foi registrado. Agradecemos sua participação e esperamos que aprecie o evento.<br/> Não se esqueça de selecionar as atividades que deseja participar para garantir sua vaga.';
                case 5 : return 'Você iniciou uma disputa no pagseguro, por favor, entre em contato com a administração do evento para que a solução seja encontrada.';
                case 6 : return 'O valor de pagamento de sua inscrição foi devolvido. Se ainda deseja participar do evento, realize o pagamento da inscrição.';
                case 7 : return 'O pagamento de sua inscrição foi cancelado. Entre em contato com a organização do evento para maiores detalhes.';
                default : return $status;
            }
        } catch (Exception $exc) {
            return NULL;
        }
    }

    static public function getStatusColor($status) {
        try {
            switch ($status) {
                case 1 : return 'yellow lighten-5';
                case 2 : return 'yellow lighten-5';
                case 3 : return 'green lighten-5';
                case 4 : return 'blue lighten-5';
                case 5 : return 'orange lighten-5';
                case 6 : return 'red lighten-5';
                case 7 : return 'red lighten-5';
                default : return $status;
            }
        } catch (Exception $exc) {
            return NULL;
        }
    }

    public function getTransactions($reference_id) {
        try {
            $stmt = $this->_db->prepare("SELECT id_notification as _id, reference_id AS _reference_id , transaction_code, dtt_lastevent as `Data`, pagseguro_status(transaction_status) as `Status` 
FROM `PAGSEGURO_NOTIFICATION` as pn WHERE id_notification = (SELECT MAX(id_notification) FROM PAGSEGURO_NOTIFICATION WHERE transaction_code = pn.transaction_code)
AND reference_id = {$reference_id} ORDER BY pn.transaction_code ASC, pn.dtt_lastevent DESC");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }

    public function getList($rows = 50) {
        try {
            $stmt = $this->_db->prepare("SELECT * FROM "
                    . " `$this->_table` ORDER BY `dtt_register` DESC LIMIT $rows");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }

    public function getPurchaseStatus($id_purchase, $rows = 10) {
        try {
            $stmt = $this->_db->prepare("SELECT id_notification as _id, transaction_status as status, dtt_lastevent as dtt_status FROM  "
                    . " `$this->_table` WHERE reference_id = {$id_purchase} ORDER BY `dtt_lastevent` DESC LIMIT $rows");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }

    public function isLastNotification() {
        try {
            $stmt = $this->_db->prepare("SELECT IF(transaction_status != '{$this->transaction_status}' OR transaction_status IS NULL, 1, 0) as result FROM `$this->_table` "
                    . "WHERE transaction_code = '{$this->transaction_code}' ORDER BY `dtt_lastevent` DESC LIMIT 1");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->rowCount() > 0 ? $stmt->fetch()['result'] : 1;
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }

    static public function getDescriptionKey($key) {
        try {
            switch ($key) {
                case 'id_notification':
                    return 'ID';
                case 'transaction_status_desc':
                    return 'Status';
                case 'transaction_code':
                    return 'Transação Pagseguro';
                case 'reference_id':
                    return 'Compra';
                case 'total_value':
                    return 'Total';
                case 'discount_value':
                    return 'Desconto';
                case 'extra_value':
                    return 'Acréscimos';
                case 'dtt_register':
                    return 'Registro';
                case 'dtt_lastevent':
                    return 'Data';
                case 'payment_method_desc':
                    return 'Método';
                case 'payment_way_desc':
                    return 'Modo';
                case 'installment_number':
                    return 'Parcelas';
                default : return $key;
            }
        } catch (Exception $exc) {
            return NULL;
        }
    }

}

?>