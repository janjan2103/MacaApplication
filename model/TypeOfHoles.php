<?php

class TypeOfHoles extends Crud {
    
    const ERROR_CODE_PREFIX = 80;

    public $id_type_holes;
    public $type;
    public $description;
    protected $_table = "TYPE_OF_HOLES";
    protected $_key_field = 'id_type_holes';
    
    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($id = NULL) {
        parent::__construct();
        if ($id > 0) {
            $this->find($id);
        }
    }

    static public function getDescriptionKey($key) {
        try {
            switch ($key) {
                case 'id_type_holes':
                    return 'ID';
                case 'type':
                    return 'Tipo';
                case 'description':
                    return 'Descrição';
                default :
                    return $key;
            }
        } catch (Exception $e) {
            return NULL;
        }
    }

    public function find($id) {
        if (parent::find([$this->_key_field => $id])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function setState($data) {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }
    
    public function getList($rows = 1000) {
        try {
            $stmt = $this->_db->prepare("SELECT *, $this->_key_field AS `options` FROM `$this->_table` ORDER BY `$this->_key_field` ASC LIMIT $rows");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $e) {
            Logger::logFor(__CLASS__ . '-list', $e->getTraceAsString());
            return NULL;
        }
    }
}

?>
