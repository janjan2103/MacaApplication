<?php

if (!class_exists('Logger')) {

    class Logger {

        const FILE_PATH = "/log/";
        const FILE_NAME = "log";

        public function __construct() {

            date_default_timezone_set('America/Sao_Paulo');
        }

        private static function getLogPath() {
            return realpath('./') . self::FILE_PATH;
        }

        public static function log_string($msg) {
            Logger::gravar(Logger::FILE_NAME, $msg, FALSE);
        }

        public static function log_array($filename, $array, $local = NULL) {
            $msg = is_array($array) ? implode("; ", $array) : (is_string($array) ? $array : '');
            Logger::gravar($filename, $msg, $local);
        }

        private static function explode_array(array $array) {
            $msg = '';
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $msg .= '{' . self::explode_array($value) . '}, ';
                } else {
                    $msg .= "$key: $value; ";
                }
            }
            return $msg;
        }

        public static function log_matrix($filename, $array, $local = NULL) {
            if (is_array($array)) {
                $msg = self::explode_array($array);
                Logger::gravar($filename, $msg, $local);
            }
        }

        public static function log_exception(Exception $ex, $class = NULL, $method = NULL, $local = NULL) {
            $class = $class ? $class : get_class($ex);
            $msg = "[$class][$method]";
            $msg .= ';' . $ex->getCode();
            $msg .= ';' . $ex->getFile();
            $msg .= ';' . $ex->getLine();
            $msg .= ' - ' . $ex->getMessage();
            Logger::gravar($class, $msg, $local);
        }

        public static function logFor($filename, $msg) {
            Logger::gravar($filename, $msg);
        }

        public static function gravar($filename, $msg, $local = NULL, $data_on_name = TRUE) {
            try {
                $data = date("y-m-d");
                if ($data_on_name) {
                    $filename .= '-' . $data;
                }
                $hora = date("H:i:s");
                $ip = $_SERVER['REMOTE_ADDR']; //Nome do arquivo:
                if ($local != NULL) {
                    $method = $local;
                } else {
                    $method = '';
                }
                $arquivo = self::getLogPath() . "log_$filename.log";

                $texto = "[$data][$hora][$ip][$method]> $msg \n";
                $manipular = fopen($arquivo, "r");
                fwrite($manipular, $texto);
                fclose($manipular);
            } catch (Exception $ex) {
                
            }
        }

        public static function dump($filename, $string) {
            try {
                $file_path = self::getLogPath() . "$filename.log";
                $file = fopen($file_path, "r");
                if ($file) {
                    $res = fwrite($file, $string . ";\n");
                    fclose($file);
                } else {
                    throw new Exception('não abriu');
                }
            } catch (Exception $ex) {
                self::logFor('logger', $ex->getMessage());
            }
        }

    }

}
?>