<?php

class Shift extends Crud {
    
    const ERROR_CODE_PREFIX = 60;

    public $id_shift;
    public $code;
    public $qtd_hours;
    protected $_table = "SHIFT";
    protected $_key_field = 'id_shift';
    
    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($id = NULL) {
        parent::__construct();
        if ($id > 0) {
            $this->find($id);
        }
    }

    static public function getDescriptionKey($key) {
        try {
            switch ($key) {
                case 'id_shift':
                    return 'ID';
                case 'code':
                    return 'Código';
                case 'qtd_hours':
                    return 'Qtd. de Horas';
                default :
                    return $key;
            }
        } catch (Exception $e) {
            return NULL;
        }
    }

    public function find($id) {
        if (parent::find([$this->_key_field => $id])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function setState($data) {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function getList($rows = 1000) {
        try {
            $stmt = $this->_db->prepare("SELECT *, $this->_key_field AS `options` FROM `$this->_table` ORDER BY `$this->_key_field` ASC LIMIT $rows");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $e) {
            Logger::logFor(__CLASS__ . '-list', $e->getTraceAsString());
            return NULL;
        }
    }
    
    public static function getBadge($shift) {
        $id = (new Shift())->read(['code' => $shift])[0]['id_shift'];
        return '<span class="badge ' . Shift::badges($id) . '">' . $shift . '</span>';
    }
    
    public static function badges($id) {
        switch ($id) {
            case 1:
                return 'badge-default';
            case 2:
                return 'badge-info';
            case 3:
                return 'badge-success';
            case 4:
                return 'badge-primary';
            case 5:
                return 'badge-warning';
            case 5:
                return 'badge-danger';
            default:
                return 'badge-default';
        }
    }
}

?>
