<?php

class DrillingMain extends Crud {

    const ERROR_CODE_PREFIX = 99;

    public $id_drilling_main;
    public $id_shift;
    public $id_drill_operator;
    public $id_equipment;
    public $date_drilling;
    public $rl;
    public $shot_number;
    public $hour_start_meter;
    public $hour_finish_meter;
    public $start_hamer;
    public $finish_hamer;
    public $hours_shift;
    public $production_hours;
    public $redrill_hours;
    public $idle_hours;
    public $standby_hours;
    public $climate_hours;
    public $date_maintenance_meter;
    public $maintenance_events;
    protected $_table = "DRILLING_MAIN";
    protected $_key_field = "id_drilling_main";

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        if (property_exists(get_class($this), $atribute)) {
            return $this->$atribute;
        } else {
            return NULL;
        }
    }

    function __construct($id = 0) {
        parent::__construct();
        if ($id > 0) {
            $this->find($id);
        }
    }

    public function find($id) {
        if (parent::find([$this->_key_field => $id])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function setState($data) {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function getList($rows = 1000) {
        try {
            $stmt = $this->_db->prepare("SELECT dm.id_drilling_main, "
                    . "s.code AS shift, do.name AS operator, e.equipment AS equipment, dm.date_drilling, 
                    dm.rl, dm.shot_number, dm.hour_start_meter, dm.hour_finish_meter, 
                    dm.start_hamer, dm.finish_hamer 
                    FROM `$this->_table` AS dm 
                    INNER JOIN `SHIFT` AS s ON s.id_shift = dm.id_shift 
                    INNER JOIN `DRILL_OPERATOR` AS do ON do.id_drill_operator = dm.id_drill_operator 
                    INNER JOIN `EQUIPMENT` AS e ON e.id_equipment = dm.id_equipment 
                    ORDER BY `$this->_key_field` ASC LIMIT $rows");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $e) {
            Logger::logFor(__CLASS__ . '-list', $e->getTraceAsString());
            return NULL;
        }
    }

    public function getProductions() {
        try {
            $stmt = $this->_db->prepare("SELECT * FROM `DRILLING_PRODUCTION` WHERE `id_drilling_main` = {$this->id_drilling_main}");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }
}

?>
