<?php

class DrillingProduction extends Crud {

    const ERROR_CODE_PREFIX = 90;
    
    public $id_drilling_production;
    public $id_drilling_main;
    public $id_type_holes;
    public $meters;
    public $number_holes;
    public $bit_prefix;
    public $bit_number;
    public $bit_sharp;
    protected $_table = "DRILLING_PRODUCTION";
    protected $_key_field = ['id_drilling_main', 'id_drilling_production'];

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($id1 = NULL, $id2 = NULL) {
        parent::__construct();
        if ($id1 > 0 && $id2 > 0) {
            $this->findDrilling($id1, $id2);
        }
    }

    public function findDrilling($id1, $id2) {
        if (parent::find([$this->_key_field[0] => $id1, $this->_key_field[1] => $id2])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function setState($data) {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function getList($rows = 1000) {
        try {
            $stmt = $this->_db->prepare("SELECT * FROM `$this->_table` ORDER BY `$this->_key_field` ASC LIMIT $rows");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }
}
?>
