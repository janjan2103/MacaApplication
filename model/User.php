<?php

class User extends Crud {

    public $id_user;
    public $name;
    public $email;
    public $password;
    public $profile_image;
    public $_dtt_register;
    private $_table = 'USER';

    //Método SET
    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    //Método GET
    function __get($atribute) {
        if (property_exists(get_class($this), $atribute)) {
            return $this->$atribute;
        } else {
            return NULL;
        }
    }

    public function profileImageLink() {
        if (empty($this->profile_image)) {
            return false;
        } else {
            return $this->profile_image;
        }
    }

    function __construct($id = NULL) {
        parent::__construct();
        if ($id > 0) {
            $this->find($id);
        }
    }

    public function getList() {
        try {
            $stmt = $this->_db->prepare("SELECT * FROM `PARTICIPANT` ORDER BY u.`id_user` DESC");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }

    static public function getDescriptionKey($key) {
        try {
            switch ($key) {
                case 'id_user':return 'ID';
                case 'name':return 'Nome';
                case 'email':return 'Email';
            }
        } catch (Exception $e) {
            return NULL;
        }
    }
    
    public function validateUser($email, $password) {
        $data = $this->read(array("email" => $email, 'password' => $password));
        if (count($data) > 0) {
            foreach ($data[0] as $key => $value) {
                $this->$key = $value;
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
?>
