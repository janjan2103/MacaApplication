$(document).ready(function () {
    $("#sameHolder").change(function () {
        $("#address").removeClass('hideMe');
        $("#creditCardPaymentButton").removeClass('hideMe');
    });
    $("#show-fields").change(function () {
        showFields();
    });
    $('#billingAddressPostalCode').change(function () {
        var postcode = $('#billingAddressPostalCode').val().toUpperCase();
        var results = getAddress(postcode, $("#data-address"));
        $("#shippingAddressPostalCode").val($("#billingAddressPostalCode").val());
        $("#creditCardPaymentButton").removeClass("hideMe");
    });
    $("#billingAddressStreet").change(function () {
        $("#shippingAddressStreet").val($("#billingAddressStreet").val());
    });
    $("#billingAddressNumber").change(function () {
        $("#shippingAddressNumber").val($("#billingAddressNumber").val());
    });
    $("#billingAddressComplement").change(function () {
        $("#shippingAddressComplement").val($("#billingAddressComplement").val());
    });
    $("#billingAddressDistrict").change(function () {
        $("#shippingAddressDistrict").val($("#billingAddressDistrict").val());
    });
    $("#billingAddressCity").change(function () {
        $("#shippingAddressCity").val($("#billingAddressCity").val());
    });
    $("#billingAddressState").change(function () {
        $("#shippingAddressState").val($("#billingAddressState").val());
    });
    $("#billingAddressCountry").change(function () {
        $("#shippingAddressCountry").val($("#billingAddressCountry").val());
    });
});