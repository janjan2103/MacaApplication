(function ($) {

    /**
     * Armazena o array de itens carregados, mantendo a relação com o 'trigger'
     */
    jQuery.fn.renderOn_itens = new Array();
    jQuery.fn.renderOn_settings = function (options) {
        if (options == null) {
            options = {};
        }
        if (this.length) {
            settings = {
                route: null, // Rota que será acessada para obtenção do conteúdo
                container: false, // Seletor Jquery que conterá o conteúdo a ser carregado
                class_on_container: 'fn-loader', // Classe a ser adicionada no item que conterá o conteúdo a ser carregado
                class_on_active: 'active', // Ao clicar no item, adiciona-se a classe do 'trigger'
                data_bind: ['id'], // ARRAY: Dados em data-bind que será enviado via parametro GET na requisição ajax
                complete: null, // "Callback" que será chamado ao carregamento estar completo 
                track: false, // Verifica se deve ser rastreado o "caller" da ação, para que este exclua o item ao clicar novamente
                build: function (el, settings, content) {
                    var loader_el;
                    if (el.localName === 'tr') {
                        settings.track = true;
                        loader_el = jQuery("<tr />", {
                            class: settings.class_on_container,
                            id: 'render_on-' + el.id,
                            for : el.id,
                            html: jQuery("<td />", {
                                colspan: jQuery(el).children().length,
                                html: content
                            })
                        });
                    } else {
                        loader_el = jQuery("<div />", {
                            class: settings.class_on_container,
                            id: 'render_on-' + el.id,
                            for : el.id,
                            html: content
                        });
                    }

                    if (settings.container && jQuery(settings.container).length) {
                        jQuery(settings.container).html(loader_el);
                    } else if(settings.container instanceof jQuery) {
                        settings.container.html(loader_el.html());
                    } else {
                        jQuery(el).after(loader_el);
                    }

                    if (settings.track) {
                        jQuery.fn.renderOn_itens.push({
                            loaded: loader_el[0].id,
                            loader: el.id,
                            settings: settings
                        });
                    }
                    jQuery(el).addClass(settings.class_on_active);
                    if (jQuery.isFunction(settings.complete)) {
                        settings.complete(el, settings, loader_el);
                    }
                }
            };

            id = jQuery(this)[0].id;
            item_loaded = jQuery.grep(jQuery.fn.renderOn_itens, function (item) {
                return id == item.loader || id == item.loaded;
            });
            if (item_loaded && item_loaded.length) {
                item_settings = item_loaded[0]['settings'];
                return jQuery.extend(item_settings, options);
            } else {
                return jQuery.extend(settings, options);
            }
        }
    };
    /**
     * @param {string} selector
     * @param {object} options
     * @returns {object} element list of triggering handling
     */
    jQuery.fn.renderOn_handle = function (selector, options) {
        return this.each(function (ind, el) {
            jQuery(el).on('click', selector, function () {
                jQuery(this).renderOn_load(options);
            });
        });
    };
    /**
     * @param {object} settings
     * @returns {object} Element list
     */
    jQuery.fn.renderOn_load = function (options) {
        return this.each(function (ind, el) {

            settings = jQuery(el).renderOn_settings(options);
            if (!(settings && settings.route)) {
                throw 'Route attribute is not defined on the settings objects';
            }
            if (!el.id) {
                console.log('Please, add an "id" property to the caller element.');
                throw new Error('data-bind "id" is required on the element proprerties.');
            }

            if (jQuery('#render_on-' + el.id).length && settings.track) {
                jQuery('#render_on-' + el.id).remove();
                jQuery(el).removeClass(settings.class_on_active);
                jQuery.fn.renderOn_itens = jQuery.grep(jQuery.fn.renderOn_itens, function (item) {
                    return item.loader !== el.id;
                });
                return false;
            }

            var params = '';
            jQuery.each(jQuery(el).data(), function (i, value) {
                params += i + '=' + value + '&';
            });

            jQuery.ajax({
                method: "GET",
                url: settings.route + '?' + params,
                success: function (data) {
                    settings.build(el, settings, data);
                }, error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                }
            });
        });
    };
    jQuery.fn.renderOn_reload = function (element_id) {
        if (element_id) {
            element = jQuery(element_id);
        } else {
            element = this;
        }
        return element.each(function (ind, el) {
            settings = jQuery(el).renderOn_settings(null);
            if (!(settings && settings.route)) {
                throw 'Route attribute is not defined on the settings objects';
            }

            loader = '#' + jQuery(el).attr('for');
            var params = '';
            jQuery.each(jQuery(loader).data(), function (i, value) {
                params += i + '=' + value + '&';
            });
            jQuery.ajax({
                method: "GET",
                url: settings.route + '?' + params,
                success: function (data) {
                    el.remove();
                    jQuery.fn.renderOn_itens = jQuery.grep(jQuery.fn.renderOn_itens, function (item) {
                        return item.loader !== loader;
                    });
                    settings.build(jQuery(loader)[0], settings, data);
                }, error: function (xhr, ajaxOptions, thrownError) {
                    console.log(JSON.parse(thrownError));
                }
            });
        });
    }
    ;
})(jQuery);