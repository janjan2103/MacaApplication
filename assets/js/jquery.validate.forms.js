$().ready(function () {
    if ($('#form-register').length) {
        $("#form-register").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 5
                },
                dt_born: {
                    required: true,
                    minlength: 5
                },
                password: {
                    required: true,
                    minlength: 4
                },
                email: {
                    required: false,
                    email: true
                },
                re_password: {
                    minlength: 4,
                    equalTo: "#password"
                },
                id_state: {
                    required: true,
                    selects: true
                },
                combobox_city: {
                    required: true,
                    selects: true
                },
                cpf: {
                    cpfBR: true,
                    minlength: 11
                }
            },
            messages: {
                name: {
                    required: "Por favor, digite seu nome",
                    minlength: "Por favor, digite seu nome completo"
                },
                password: {
                    required: "Digite uma senha.",
                    minlength: "Sua senha deve conter no mínimo 5 caracteres"
                },
                re_password: {
                    minlength: "Sua senha deve conter no mínimo 5 caracteres",
                    equalTo: "A senha deve ser idêntica a informada acima"
                },
                email: {
                    required: "Por favor, digite seu e-mail",
                    email: "Por favor, digite um email válido."
                },
                id_state: {
                    required: "Por favor, selecione seu estado"
                },
                combobox_city: {
                    required: "Por favor, selecione sua cidade"
                },
                cpf: {
                    required: 'Digite um CPF!',
                    cpfBR: 'CPF precisa ser válido!',
                    minlength: 'CPF possúi 11 caracteres.'
                },
                agree: "Por favor, você deve aceitar nossos termos de compromisso."
            }
        });
    }
    if ($('#form-contact').length) {
        $("#form-contact").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 5
                },
                email: {
                    required: true,
                    email: true
                },
                subject: {
                    minlength: 4,
                    required: true,
                },
                content: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Por favor, digite seu nome",
                    minlength: "Por favor, digite seu nome completo"
                },
                email: {
                    required: "Por favor, digite seu e-mail",
                    email: "Por favor, digite um email válido."
                },
                subject: {
                    required: "Por favor, digite o assunto"
                },
                content: {
                    required: 'Digite uma mensagem!',
                    minlength: 'mínimo de 4 caracteres.'
                },
                agree: "Por favor, você deve aceitar nossos termos de compromisso."
            }
        });
    }
    if ($('#form-login').length) {
        $("#form-login").validate({
            rules: {
                email: {
                    required: true,
                    email: false
                },
                login: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 4
                }
            },
            messages: {
                email: {
                    required: "Por favor, digite seu e-mail ou cpf",
                    email: "Por favor, digite um email válido."
                },
                login: {
                    required: "Por favor, digite seu e-mail ou cpf"
                },
                password: {
                    required: "Por favor, digite uma senha",
                    minlength: "Sua senha deve conter no mínimo 5 caracteres"
                }
            }
        });
    }
});