var Script = function () {
    jQuery.validator.addMethod("dateGreaterThan", function (value, element, params) {
        if (!/Invalid|NaN/.test(value.split("/"))) {
            splitvalue = value.split("/");
            var newDate = new Date(splitvalue[2] + "/" + splitvalue[1] + "/" + splitvalue[0]);
            splitparams = params.split("/");
            var limitDate = new Date(splitparams[2] + "/" + splitparams[1] + "/" + splitparams[0]);
            return newDate > limitDate;
        }
        return isNaN(value) && isNaN(params) || (Number(value) > Number(params));
    }, 'Informe uma data superior a {0}.');
    jQuery.validator.addMethod("dateLessThan", function (value, element, params) {
        if (!/Invalid|NaN/.test(value.split("/"))) {
            splitvalue = value.split("/");
            var newDate = new Date(splitvalue[2] + "/" + splitvalue[1] + "/" + splitvalue[0]);
            splitparams = params.split("/");
            var limitDate = new Date(splitparams[2] + "/" + splitparams[1] + "/" + splitparams[0]);
            return newDate < limitDate;
        }
        return isNaN(value) && isNaN(params) || (Number(value) > Number(params));
    }, 'Informe uma data inferior a {0}.');
    $.validator.addMethod('greaterThan', function (value, el, param) {
        return value > param;
    }, 'Informe um valor superior a {0}.');
    $.validator.addMethod('lessThan', function (value, el, param) {
        return value < param;
    }, 'Informe um valor inferior a {0}.');
    $.validator.addMethod('lessThanEl', function (value, el, param) {
        console.log(param);
        console.log($(param));
        return value <= $(param).val();
    }, 'Valor Inválido.');
    $.validator.addMethod('lessThanHour', function (value, el, param) {
        return value <= $(param).val();
    }, 'Valor Inválido.');
    $.validator.addMethod('wordcount', function (value, el, param) {
        var words = $(el).val().split(' ');
        console.log(words)
        return value < words;
    }, 'Valor Inválido.');

    $("#login-user").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            email: {
                email: "Formato de email inválido."
            },
            password: {
                minlength: "A senha deve conter no mínimo 5 caracteres."
            }
        }
    });
    $("#form-activity").validate({
        rules: {
            name: {
                required: true,
                minlength: 5
            },
            presenter: {
                required: true
            },
            type: {
                required: true
            },
            vacancies_total: {
                required: true,
                greaterThan: 0
            },
            vacancies_publish: {
                required: true,
                greaterThan: 0
            },
            dtt_begin: {
                required: true
            },
            dtt_end: {
                required: true
            },
            dtt_registration_begin: {
                required: true
            },
            dtt_registration_end: {
                required: true
            },
            location: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Informe o nome da atividade."
            },
            vacancies_publish: {
                lessThan: "O valor informado deve ser inferior ao número real de vagas."
            },
            vacancies_total: {
                required: "Informe a quantidade de vagas totais da atividade"
            },
            presenter: {
                required: "Informe o nome de quem irá ministrar a atividade"
            },
            type: {
                required: 'Informe o tipo da sua atividade'
            },
            dtt_begin: {
                required: 'Informe a data de inicio dessa atividade'
            },
            dtt_end: {
                required: 'Informe a data de fim dessa atividade'
            },
            dtt_registration_begin: {
                required: 'Informe a data de inicio das inscrições dessa atividade'
            },
            dtt_registration_end: {
                required: 'Informe a data de fim das inscrições dessa atividade'
            },
            location: {
                required: 'Informe o local de realização da atividade'
            }
        }
    });
    $("#form-edit_user").validate({
        rules: {
            name: {
                required: true,
                minlength: 5
            },
            email: {
                required: true,
                email: true
            },
            doc_id: {
                required: true
            },
            phone: {
                required: true
            },
            cellphone: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Informe seu nome.",
                minlength: "Informe seu nome completo"
            },
            email: {
                required: "Informe o e-mail para acesso",
                email: "Por favor, digite um email válido."
            },
            password: {
                required: "Informe uma senha para acesso.",
                minlength: "A senha deve conter no mínimo 4 caracteres"
            },
            re_password: {
                required: "Digite a confirmação da senha.",
                minlength: "A senha deve conter no mínimo 4 caracteres",
                equalTo: "A senha deve ser idêntica a informada anteriormente"
            }
        }
    });
}();