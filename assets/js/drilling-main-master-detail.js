var template = "#productionsRowTemplate";

$(document).ready(function () {
    $(".form-drilling-main").focus();
    $("#date_drilling").mask("99/99/9999 99:99");
    var data = detailData();
    if (data.length > 0) {
        $(template).tmpl(data).appendTo('#detail-fields');
    }
    $('#date_drilling').datetimepicker({
        format: 'd/m/Y H:i',
        inline: false,
        lang: 'en',
        allowTimes: [
            '01:00', '02:00', '03:00', '04:00', '05:00',
            '06:00', '07:00', '08:00', '09:00', '10:00',
            '11:00', '12:00', '13:00', '15:00', '12:00',
            '13:00', '14:00', '15:00', '16:00', '17:00',
            '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'
        ]
    });
});

function addDetail() {
    id = $('#detail-fields .new_production').last().data('id');
    if (typeof id == 'undefined' || id == '') {
        id = 1;
    } else {
        id = parseInt(id) + 1;
    }
    $(template).tmpl([{"id_drilling_production": id}]).appendTo("#detail-fields");
}

$(document).on('click', '.remove-detail', function () {
    container = $(this).closest(".new_production");
    id = container.data()['id'];
    console.log('excluindo: ' + id);
    detailRow = container.addClass('hideMe');
    detailRow = container.append('<input name="productions[' + id + '][remove]" value="1" type="hidden">');
});
function buildMessage(code, message) {
    if (code === 0 || code === 1) {
        swal({
            title: message,
            text: "Clique em ok para fechar esta mensagem",
            type: 'success',
            confirmButtonColor: "#1F82BF",
            confirmButtonText: "OK",
            closeOnCancel: false,
            timer: 2000
        });
    } else {
        swal({
            title: "Atenção",
            text: message,
            type: 'error',
            confirmButtonColor: "#1F82BF",
            confirmButtonText: "OK",
            closeOnCancel: false,
            timer: 3000
        });
    }
}
function sendForm(form, callback) {
    var formData = new FormData($(form)[0]);
    var method = $(form).data("method");
    $.ajax({
        method: "POST",
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        url: "/drilling/main/" + method + ".json",
        success: function (data) {
            response = JSON.parse(data);
            callback(response);
        }, error: function (xhr, ajaxOptions, thrownError) {
            callback(xhr.responseText);
        }
    });
}

function register(form) {
    $(form).submit(function () {
        return false
    });
    sendForm(form, function (response) {
        if (response.code === 0) {
            $("#shift-select").attr('selected', 'selected');
            $("#hole-select").attr('selected', 'selected');
            $("#operator-select").attr('selected', 'selected');
            var form_input = $(form + " input");
            form_input.each(function (i) {
                $(this).val('');
            });
            $(document).scrollTop(0);
            buildMessage(response.code, response.message);
        } else if (response.code === 1) {
            setTimeout(function () {
                buildMessage(response.code, response.message);
                window.location.assign('/drilling/main/');
            }, 3000);
        }
    });
}