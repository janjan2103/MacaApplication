function getOptionsAddress(postcode, data, prepend) {
//    if (data && data.lenght) {
    var html = '<div class="list-group" id="choiceAddress">';
    if (data.length > 3) {
        len_i = 3;
    } else {
        len_i = data.length;
    }
    for (var i = 0; i < len_i; i++) {
        html += '<div class="list-group-item option_address lead-sm" \n\
            onclick="buildFields(this)" style="cursor: pointer;" ';
        components = data[i].address_components;
        data_address = [];
        switch (components.length) {
            case 4:
                data_address[0] = ' data-street="" ';
                data_address[1] = ' data-district="" ';
                data_address[2] = ' data-city="' + components[1].long_name + '" ';
                data_address[3] = ' data-state="' + components[2].long_name + '" ';
                data_address[4] = ' data-country="' + components[3].long_name + '" ';
                break;
            case 5:
                data_address[0] = ' data-street="" ';
                data_address[1] = ' data-district="' + components[0].long_name + '" ';
                data_address[2] = ' data-city="' + components[1].long_name + '" ';
                data_address[3] = ' data-state="' + components[2].long_name + '" ';
                data_address[4] = ' data-country="' + components[3].long_name + '" ';
                break;
            case 8:
                data_address[0] = ' data-street="' + components[1].long_name + '" ';
                data_address[1] = ' data-district="' + components[2].long_name + '" ';
                data_address[2] = ' data-city="' + components[4].long_name + '" ';
                data_address[3] = ' data-state="' + components[5].short_name + '" ';
                data_address[4] = ' data-country="' + components[6].long_name + '" ';
                break;
            case 9:
                data_address[0] = ' data-street="' + components[2].long_name + '" ';
                data_address[1] = ' data-district="' + components[3].long_name + '" ';
                data_address[2] = ' data-city="' + components[4].long_name + '" ';
                data_address[3] = ' data-state="' + components[6].short_name + '" ';
                data_address[4] = ' data-country="' + components[7].long_name + '" ';
                break;
        }
        for (var j = 0; j < data_address.length; j++) {
            html += data_address[j];
        }
        html += '>' + data[i].formatted_address + '</div>';
    }
    html += '</div>';
    $("#data-address").removeClass("hideMe");
    $(".show_fields").removeClass("hideMe");
    $(prepend).html("");
    $(prepend).prepend(html);
}

function getAddress(postcode, prepend) {
    var results = null;
    $("#data-address").addClass("hideMe");
    $("#address").addClass("hideMe");
    $.getJSON('//maps.googleapis.com/maps/api/geocode/json?address=' + postcode + '&sensor=false',
            function (data) {
                var lat = data.results[0].geometry.location.lat;
                var lng = data.results[0].geometry.location.lng;
                //Get address    
                $.getJSON('//maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng + '&sensor=false',
                        function (data) {
                            var results = data.results;
                            getOptionsAddress(postcode, results, prepend);

                        });
            });
}
