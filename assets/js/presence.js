var sendForm = function (el) {
    console.log($(el).data('status'));
    if ($(el).data('status') < 3 && $(el).data('button') === 'open_modal') {
        $(el).renderOn_load({
            'route': "/" + eventId + "/presences/modal_presence.partial",
            container: "#partial-modal"
        });
    } else {
        $.ajax({
            method: "POST",
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            url: "/" + eventId + "/presences/create.json?id=" + $(el).data('id'),
            success: function (data) {
                res = JSON.parse(data);
                if (res.code == 0) {
                    setButtonRemove(res);
                } else {
                    console.log(res.message);
                }
                $("#modal-presence").modal("hide");
            }, error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    }
};

var remove_presence = function (el) {
    $.ajax({
        method: "POST",
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        url: "/" + eventId + "/presences/remove.json?id=" + $(el).data('id'),
        success: function (data) {
            res = JSON.parse(data);
            if (res.code == 0) {
                setButtonRegister(res);
            } else {
                console.log(res.message);
            }
        }, error: function (xhr, ajaxOptions, thrownError) {
            console.log(thrownError);
        }
    });
};
function setButtonRegister(res) {
    var html = "<button data-id='" + res.id_ticket + "' data-button='open_modal' data-status='" + res.id_ticket_status + "' id='register_presence_" + res.id_ticket + "' onclick='sendForm(this)'" +
            "class = 'btn btn-link text-primary'><i class='fa fa-check'></i> Registrar presença</button > ";
    $("#presence_" + res.id_ticket).html(html);
}

function setButtonRemove(res) {
    var html = "Presença registrada em: <br/><b class='text-info'>" + res.date + "</b><br/>" +
            "<button data-id='" + res.id_presence + "' id='remove_presence_" + res.id_presence + "' onclick='remove_presence(this)'" +
            "class = 'btn btn-link text-danger padding-none'> <small> <i class='fa fa-times'></i> Remover presença</small></button>";
    $("#presence_" + res.id_ticket).html(html);
}