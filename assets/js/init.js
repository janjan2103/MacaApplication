
function setStoreTable(el) {
    $(el).DataTable({
        "pagingType": "full_numbers",
        "sPaginationType": "full_numbers",
        "scrollX": true,
        "bSortClasses": false,
        "bInfo": false,
        "order": [[0, "desc"]],
        "stateSave": true,
        "language": {
            "lengthMenu": "Resultados por página: _MENU_ ",
            "zeroRecords": "Nenhum resultado para exibição",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "Sem resultados",
            "infoFiltered": "(Filtrado entre _MAX_ resultados)",
            "search": "Buscar:",
            "paginate": {
                "first": "Início",
                "last": "Final",
                "next": "Próxima",
                "previous": "Anterior"
            }
        }
    });
}

(function ($) {
    Number.prototype.formatMoney = function (c, d, t) {
        var n = this,
                c = isNaN(c = Math.abs(c)) ? 2 : c,
                d = d === undefined ? "." : d,
                t = t === undefined ? "," : t,
                s = n < 0 ? "-" : "",
                i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };
    HELPER = {
        storeTable: null,
        validator: null,
        event: null,
        swap: null,
        lang: null,
        detail_rows: function (selector, content) {
            $(selector).on('click', 'tr.details-control', function () {
                var tr = $(this);
                id = tr.data('id');
                if (tr.data('detail')) {
                    $('#detail-' + id).remove();
                    tr.data('detail', false);
                } else {
                    tr.data('detail', true);
                    elementId = 'detail-' + id;
                    len = HELPER.storeTable.columns().indexes().length;
                    tr.after('<tr id="' + elementId + '" class="detail" ><td colspan="' + len + '">Carregando...</td></tr>');
                    content('#' + elementId, id);
                }
            });
        }
    };
    
    var current_lang = $('meta[name=lang]').attr("content");
    var currentLanguage = current_lang || "en_US";
    $.getJSON("/i18n/" + currentLanguage + "/" + currentLanguage + ".json", function (json) {
        HELPER['lang'] = json;
    });

    $(document).ready(function () {
        $('[data-toggle="popover"]').popover({
            html: true
        });
        $('#dropdown_menu .dropdown-language').on("click", function (e) {
            $(this).next('div').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
        if ($("table.table tbody tr").length > 0) {
            HELPER.storeTable = setStoreTable('table.table');
        }
        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        jQuery.extend(jQuery.validator.messages, {
            required: "Esse campo é obrigatório.",
            remote: "Por favor, corriga esse campo.",
            email: "Por favor, insira um email.",
            url: "Por favor, insira uma URL válida.",
            date: "Por favor, insira uma data válida.",
            dateISO: "Por favor, insira uma data(ISO) válida.",
            number: "Por favor, insira um número válido.",
            digits: "Por favor, digite apenas digitos.",
            creditcard: "Por favor, insira um número de cartão de crédito válido.",
            equalTo: "Os campos não estão iguais.",
            accept: "Por favor, inisira um arquivo com a extensão válida.",
            maxlength: jQuery.validator.format("Por favor, insira no máximo {0} caracteres."),
            minlength: jQuery.validator.format("Por favor, inisra no mínimo {0} caracteres."),
            rangelength: jQuery.validator.format("Por favor, insira entre {0} e {1} caracteres."),
            range: jQuery.validator.format("Por favor, insira entre {0} e {1}."),
            max: jQuery.validator.format("Por favor, insira um valor menor ou igual a {0}."),
            min: jQuery.validator.format("Por favor, insira um valor maior ou igual a {0}.")
        });
    }); // end of document ready
})(jQuery); // end of jQuery name space