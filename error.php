<?php
$current_user = Session::currentUser();
$event_id = Event::id();
?>
<html>
    <head>
        <?php
        header("HTTP/1.0 500 Internal Server Error");
        include './partial/head.php';
        ?>
        <link type="text/css" rel="stylesheet" href="/assets/css/cover.css">        
    </head>
    <body>
         <!-- error code: <?= Session::errorCode() ?>;  -->
        <?= env("development") ? '<!-- error message: ' . Session::errorMessage() . ' -->' : '' ?>
        <div class="site-wrapper">
            <div class="site-wrapper-inner">
                <div class="inner cover">
                    <h1 class="cover-heading"><i class="text-white fa fa-bug"></i> Ops...</h1>
                    <p class="lead">
                        <?php
                        echo "Ocorreu um erro, entre em contato ou tente novamente mais tarde.";
                        Session::unsetMessages();
                        ?>
                    </p>
                    <p class="lead">
                        Por favor, caso seja algum botão ou link tenha redirecionado você para esta página, contate-nos.
                    </p>
                    <p class="lead">
                        <?php if ($current_user instanceof Admin or $current_user instanceof Participant) { ?>
                            <a class="btn btn-lg btn-secondary" href="<?= $current_user->linkTo('control_panel') ?>">PAINEL DE CONTROLE</a>
                        <?php } else { ?>
                            <a class="btn btn-lg btn-secondary" href="<?= routeFor("/contacts/") ?>">CONTATO</a>
                        <?php } ?>
                    </p>
                </div>
                <div class="mastfoot">
                    <div class="inner">
                        <p>by <a href="//<?= EVENTFY ?>">Eventfy</a></p>
                    </div>
                </div>
            </div>
        </div>
        <?php include './partial/scripts.php'; ?>
    </body>
</html>