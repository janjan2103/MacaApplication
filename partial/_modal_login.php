<script>
    function get_modal_register(el) {
        $("#modal-login").modal("hide");
        $(el).renderOn_load({
            'route': "/login/modal_register.partial",
            container: "#partial-modal"
        });
    }
    function sendForm() {
        $("#box-error").addClass('hideMe');
        var formData = new FormData($("#login-modal-user")[0]);
        $.ajax({
            method: "POST",
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            url: "/login/login.json",
            success: function (data) {
                res = JSON.parse(data);
                if (res.code == 0) {
                    $("#box-success").removeClass("hideMe");
                    $("#message").html(res.message);
                    setTimeout(function () {
                        $("#modal-login").modal("hide");
                        window.location.assign(res.url);
                    }, 3000);
                } else {
                    $("#message-error").html("<strong>Ops!</strong> " + res.message);
                    $("#box-error").removeClass('hideMe');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                console.log("xhr: " + xhr);
                console.log("ajaxOptions: " + ajaxOptions);
                console.log("thrownError: " + thrownError);
                $("#message-error").html("<strong>Ops!</strong> " + JSON.parse(thrownError));
                $("#box-error").removeClass('hideMe');
            }
        });
    }
    $(document).ready(function () {
        $("#modal-login").modal("show");
    });
</script>
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title text-lg-center text-primary" id="modal-login-label"><?= __('Login') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 hideMe" id="box-error">
                        <div class="alert alert-danger" id="message-error">

                        </div>
                    </div>
                    <div class="col-sm-12 hideMe" id="box-success">
                        <div class="alert alert-success" id="message">

                        </div>
                    </div>
                    <form class="col-sm-12" action="<?= '/login/login/' ?>" method="POST" id="login-modal-user" enctype="multipart/form-data">
                        <div class="col-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="email"><?= __('E-mail') ?></label>
                                <input class='form-control' type="text" id="email" name="email" placeholder="*<?= __('Obrigatório') ?>" value="" required="" autofocus="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="password"><?= __('Password') ?></label>
                                <input class='form-control' type="password" id="password" name="password" placeholder="*<?= __('Obrigatório') ?>" value="" required="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success float-lg-left" id="button-register" onclick="get_modal_register(this);"><?= __('Sign Up') ?></button>
                <button id="cancel_modal" type="button" class="btn btn-link" data-dismiss="modal"><?= __('Cancel') ?></button>
                <button type="button" class="btn btn-primary" onclick="sendForm(this, 'form#login-modal-user');"><?= __('Sign In') ?></button>
            </div>
        </div>  
    </div>
</div>