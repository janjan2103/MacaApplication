<script>
    function sendForm() {
        $("#box-error").addClass('hideMe');
        var formData = new FormData($("#update-modal-user")[0]);
        $.ajax({
            method: "POST",
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            url: "/login/update.json",
            success: function (data) {
                res = JSON.parse(data);
                if (res.code == 0) {
                    $("#box-success").removeClass("hideMe");
                    $("#message").html(res.message);
                    setTimeout(function () {
                        $("#modal-update").modal("hide");
                        window.location.assign(res.url);
                    }, 3000);
                } else {
                    $("#message-error").html("<strong>Ops!</strong> " + res.message);
                    $("#box-error").removeClass('hideMe');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                console.log("xhr: " + xhr);
                console.log("ajaxOptions: " + ajaxOptions);
                console.log("thrownError: " + thrownError);
                $("#message-error").html("<strong>Ops!</strong> " + JSON.parse(thrownError));
                $("#box-error").removeClass('hideMe');
            }
        });
    }
    $("#button-image").click(function () {
        $("#profile_image").trigger("click");
        $('#profile_image').change(function () {
            $("#profile_img_div").removeClass("hideMe");
            $(".image-user").addClass("hideMe");
        });
    });
    $(document).ready(function () {
        $("#modal-update").modal("show");

        document.getElementById('profile_image').onchange = function (evt) {
            var tgt = evt.target || window.event.srcElement, files = tgt.files;
            if (FileReader && files && files.length) {
                var fr = new FileReader();
                fr.onload = function () {
                    document.getElementById('profile_img_div').src = fr.result;
                    document.getElementById('profile_image').val = fr.result;
                }
                fr.readAsDataURL(files[0]);
            } else {
            }
        }
    });
</script>
<div class="modal fade" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="modal-update-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title text-lg-center text-primary" id="modal-update-label"><?= __('Change Profile') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 hideMe" id="box-error">
                        <div class="alert alert-danger" id="message-error">

                        </div>
                    </div>
                    <div class="col-sm-12 hideMe" id="box-success">
                        <div class="alert alert-success" id="message">

                        </div>
                    </div>
                    <form class="col-sm-12" action="<?= '/login/login/' ?>" method="POST" id="update-modal-user" enctype="multipart/form-data">
                        <input type="hidden" name="id_user" value='<?= $current_user->id_user ?>' />
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="name"><?= __('Name') ?></label>
                                <input class='form-control' type="text" id="name" name="name" placeholder="*<?= __('Obrigatório') ?>" value="<?= $current_user->name ?>" required="" autofocus="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="email"><?= __('E-mail') ?></label>
                                <input class='form-control' type="text" id="email" name="email" placeholder="*<?= __('Obrigatório') ?>" value="<?= $current_user->email ?>" required="" <?= (!empty($current_user->email) ? 'readonly=""' : '') ?>>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <small class="text-muted lead-sm"><?= __("For don't change your password, leave this fields blank.") ?></small>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="password"><?= __('Password') ?></label>
                                <input class='form-control' type="password" id="password" name="password" placeholder="*<?= __('Obrigatório') ?>" value="" required="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="re-password"><?= __('Repeat Password') ?></label>
                                <input class='form-control' type="password" id="re-password" name="re_password" placeholder="*<?= __('Obrigatório') ?>" value="" required="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <h5 class="lead-lg text-primary text-lg-center"><?= __("Profile Image") ?></h5>
                            <div class="form-group fileUpload">
                                <button type="button" class="btn-sm btn btn-info mb-1 col-sm-12" id="button-image"><?= __("Select Image") ?></button>
                                <input type="file" class="profile_image hideMe" id="profile_image" name="profile_image" accept="image/*" aria-describedby="profile_image">
                                <br/>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6 text-lg-center">
                            <div class="image-user <?= (!empty($current_user->profile_image) ? 'hideMe' : '') ?> ">
                                <?= Helper::profileImage($current_user, 'profile_menu img-rounded'); ?>
                            </div>
                            <img class='image-profile rounded-circle <?= (empty($user->profile_image) ? 'hideMe' : '') ?>' id="profile_img_div" src="<?= (!empty($current_user->profile_image) ? $current_user->profile_image : '') ?>" style="width: 70%; margin-left: 15%;" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button id="cancel_modal" type="button" class="btn btn-link" data-dismiss="modal"><?= __('Cancel') ?></button>
                <button type="button" class="btn btn-primary" onclick="sendForm(this, 'form#update-modal-user');"><?= __('Update') ?></button>
            </div>
        </div>  
    </div>
</div>