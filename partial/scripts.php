<script type="text/javascript" src="/assets/js/tether.min.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap-4.0.0.js"></script>
<script type="text/javascript" src="/assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="/assets/js/sweetalert2.js"></script>
<script type="text/javascript" src="/assets/js/html5shiv.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="/assets/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery.validate.additional.min.js"></script>
<script type="text/javascript" src="/assets/js/init.js"></script>
<script type="text/javascript" src="/assets/js/forms-validation.js"></script>
<script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '1a67538cc4ab6a72fc8daf7a7decebac56bf4f05');
</script>