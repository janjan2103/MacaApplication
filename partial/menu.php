<?php
if (Session::isLoged()) {
    ?>
    <div class="col-sm-12 col-lg-3">
        <div class="list-group mt-1 text-muted">
            <div class="list-group-item text-lg-center">
                <?= Helper::profileImage($current_user, 'rounded-circle', ['style="width: 70%;"'], 'fa-5x') ?>
                <br/>
                <button class="btn btn-link btn-sm lead-sm text-muted" type="button" id="change_profile" onclick="change_profile(this)" data-id="<?= $current_user->id_user ?>"><?= __("Change Profile") ?></button>
                <p class="lead-md text-primary"><?= $current_user->name ?></p>
            </div>
            <div class="list-group-item">
                <a class="btnx-link btn btn" href="/drilling/main/"><i class="fa fa-diamond"></i> <?= __("Drilling Main") ?></a>
            </div>
            <div class="list-group-item">
                <a class="btnx-link btn btn" href="/drill/operator/"><i class="fa fa-user-circle-o"></i> <?= __("Drill Operator") ?></a>
            </div>
            <div class="list-group-item">
                <a class="btnx-link btn btn" href="/holes/"><i class="fa fa-bullseye"></i> <?= __("Type of Holes") ?></a>
            </div>
            <div class="list-group-item">
                <a class="btnx-link btn btn" href="/shift/"><i class="fa fa-briefcase"></i> <?= __("Shift") ?></a>
            </div>
            <div class="list-group-item">
                <a class="btnx-link btn btn" href="/equipment/"><i class="fa fa-gears"></i> <?= __("Equipment") ?></a>
            </div>
        </div>
    </div>
    <?php
    $col_lg = 'col-lg-9';
} else {
    $col_lg = '';
}
?>