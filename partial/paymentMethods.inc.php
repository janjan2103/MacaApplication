<div class="col-sm-12 col-lg-4 float-lg-right mb-3" id="paymentMethods">
    <div class="card">
        <div class="card-block">
            <h5 class="lead-lg text-primary text-center"><small><?= __("Meios de Pagamento") ?></small></h5>
            <div id="paymentMethodsOptions" class="text-center">
                <label class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" name="changePaymentMethod" id="creditCardRadio" value="creditCard"> 
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description"><?= __("Cartão de crédito") ?></span>
                </label>
                <label class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" name="changePaymentMethod" id="boletoRadio" value="boleto"> 
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description"><?= __("Boleto") ?></span>
                </label>
            </div>
            <div id="creditCardData" class="paymentMethodGroup hideMe" class="col-sm-12" dataMethod="creditCard">
                <hr>
                <div id="cardData">
                    <h5 class="lead-md text-primary text-center"><?= __("Dados do Cartão") ?></h5>
                    <div class="form-group row" id="cardBrand">
                        <label for="cardNumber" class="col-sm-2 col-form-label-sm lead"><?= __("Número") ?>:</label>
                        <div class="col-sm-8 pr-0">
                            <input type="text" name="cardNumber" id="cardNumber" class="form-control cardDatainput" value="" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cardExpirationMonth" class="col-sm-5 col-form-label-sm lead text-lg-left pr-0"><?= __("Data de vencimento") ?>:</label>
                        <input type="text" name="cardExpirationMonth" id="cardExpirationMonth" class="form-control month" maxlength="2" style="width: 50px !important" value="" /> 
                        <span style="font-size: 1.5em; font-weight: 300; margin: 0 10px;">/</span>
                        <input type="text" name="cardExpirationYear" id="cardExpirationYear" class="form-control year col-sm-3" maxlength="4" value="" />
                    </div>
                    <div class="form-group row">
                        <label for="cardCvv" class="col-sm-8 col-form-label-sm lead text-lg-left"><?= __("Código de Segurança") ?>:</label>
                        <input type="text" name="cardCvv" id="cardCvv" maxlength="5" class="col-sm-4 form-control" value="" />
                    </div>
                    <label class="custom-control custom-checkbox p-0">
                        <input type="checkbox" class="custom-control-input" value="1">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description pl-2"><small><?= __("Salvar informações do cartão para compras futuras") ?></small></span>
                    </label>
                </div>
                <div class="form-group row" id="installmentsWrapper">
                    <label for="installmentQuantity" class="col-sm-6 col-form-label-sm lead text-lg-left"><?= __("Parcelamento") ?>:</label>
                    <select name="installmentQuantity" id="installmentQuantity" class="col-sm-6 form-control"></select>
                    <input type="hidden" name="installmentValue" id="installmentValue" />
                </div>
                <hr>
                <h5 class="lead-lg text-primary text-center"><small><?= __("Dados do Titular do Cartão") ?></small></h5>
                <div class="form-check form-check-inline">
                    <div id="holderDataChoice">
                        <label class="custom-control custom-radio">
                            <input class="custom-control-input holderType" type="radio" name="holderType" id="sameHolder" value="creditCard"> 
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description"><?= __("Mesmo que o comprador") ?></span>
                        </label>
                    </div>
                </div>
                <div class="form-check form-check-inline">
                    <label class="custom-control custom-radio">
                        <input class="custom-control-input holderType" type="radio" name="holderType" id="otherHolder" value="boleto"> 
                            <span class="custom-control-indicator"></span>
                        <span class="custom-control-description"><?= __("Outro") ?></span>
                    </label>
                </div>
                <div class="dataHolder hideMe">
                    <div class="form-group row">
                        <label for="creditCardHolderBirthDate" class="col-sm-8 col-form-label-sm lead text-lg-left"><?= __("Data de nascimento") ?>:</label>
                        <input type="text" name="creditCardHolderBirthDate" id="creditCardHolderBirthDate" maxlength="10" class="col-sm-4 form-control" value="" />
                    </div>
                    <div id="holderData" class="col-sm-12 p-0">
                        <div class="form-group">
                            <label for="creditCardHolderName" class="lead text-center"><?= __("Nome (Como está impresso no cartão)") ?>:</label>
                            <input type="text" name="creditCardHolderName" id="creditCardHolderName" holderField="name" class="form-control" value="" />
                        </div>
                        <div class="form-group row">
                            <label for="creditCardHolderCPF" class="col-sm-7 col-form-label-sm lead text-lg-left"><?= ("CPF (somente números)") ?>:</label>
                            <input type="text" name="creditCardHolderCPF" id="creditCardHolderCPF" holderField="cpf" maxlength="11" class="col-sm-5 form-control" value="" />
                        </div>
                        <div class="form-group row">
                            <label for="creditCardHolderAreaCode" class="col-sm-4 col-form-label-sm lead text-lg-left"><?= __("Telefone") ?>:</label>
                            <input type="text" name="creditCardHolderAreaCode" id="creditCardHolderAreaCode" holderField="areaCode" maxlength="2" class="areaCode col-sm-2 mr-2 form-control" value="" />
                            <input type="text" name="creditCardHolderPhone" id="creditCardHolderPhone" holderField="phone" maxlength="9" class="phone col-sm-4 form-control" />
                        </div>
                        <h5 class="lead-md text-primary text-center"><?= __("Endereço de Cobrança") ?></h5>
                        <div class="form-group row">
                            <label for="billingAddressPostalCode" class="col-sm-2 col-form-label-sm lead text-lg-left"><?= __("CEP") ?>:</label>
                            <input type="text" name="billingAddressPostalCode" id="billingAddressPostalCode" holderField="postalCode" class="col-sm-10 form-control" value="" />
                        </div>
                        <div id='data-address'></div>
                        <div class="form-check form-check-inline row hideMe show_fields p-1">
                            <label class="custom-control custom-checkbox p-0">
                                <input class="custom-control-input" type="checkbox" id="show-fields">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description pl-2"><small><?= __("Digitar endereço manualmente") ?></small></span>
                            </label>
                        </div>
                        <div id="address" class="hideMe">
                            <label class="custom-control custom-checkbox p-0">
                                <input type="checkbox" class="custom-control-input" value="1">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description pl-2"><small><?= __("Salvar informações e endereço no seu perfil") ?></small></span>
                            </label>
                            <hr>
                            <div class="form-group row mt-1">
                                <label for="billingAddressStreet" class="col-sm-6 col-form-label-sm lead text-lg-left"><?= __("Rua, Avenida, etc ...") ?>:</label>
                                <input type="text" name="billingAddressStreet" id="billingAddressStreet" holderField="street" class="col-sm-6 form-control" />
                            </div>
                            <div class="form-group row">
                                <label for="billingAddressNumber" class="col-sm-3 col-form-label-sm lead text-lg-left"><?= __("Número") ?>:</label>
                                <input type="text" name="billingAddressNumber" id="billingAddressNumber" holderField="number" class="col-sm-9 form-control" value="" />
                            </div>
                            <div class="form-group row">
                                <label for="billingAddressComplement" class="col-sm-4 col-form-label-sm lead text-lg-left"><?= __("Complemento") ?>:</label>
                                <input type="text" name="billingAddressComplement" id="billingAddressComplement" holderField="complement" class="col-sm-8 form-control" value="" />
                            </div>
                            <div class="form-group row">
                                <label for="billingAddressDistrict" class="col-sm-3 col-form-label-sm lead text-lg-left"><?= __("Bairro") ?>:</label>
                                <input type="text" name="billingAddressDistrict" id="billingAddressDistrict" holderField="district" class="col-sm-9 form-control" />
                            </div>
                            <div class="form-group row">
                                <label for="billingAddressCity" class="col-sm-3 col-form-label-sm lead text-lg-left"><?= __("Cidade") ?>:</label>
                                <input type="text" name="billingAddressCity" id="billingAddressCity" holderField="city" class="col-sm-9 form-control" />
                            </div>
                            <div class="form-group row">
                                <label for="billingAddressState" class="col-sm-3 col-form-label-sm lead text-lg-left"><?= __("Estado") ?>:</label>
                                <input type="text" name="billingAddressState" id="billingAddressState" holderField="state" maxlength="2" class="addressState col-sm-9 form-control" />
                            </div>
                            <div class="form-group row">
                                <label for="billingAddressCountry" class="col-sm-3 col-form-label-sm lead text-lg-left"><?= __("País") ?>:</label>
                                <input type="text" name="billingAddressCountry" id="billingAddressCountry" holderField="country" class="col-sm-9 form-control" />
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="creditCardToken" id="creditCardToken"  />
                <input type="hidden" name="creditCardBrand" id="creditCardBrand"  />
                <button type="button" id="creditCardPaymentButton" class="btn btn-success col-sm-4 offset-sm-4 hideMe">Pagar</button>
            </div>
            <div id="eftData" class="paymentMethodGroup hideMe" dataMethod="eft">
                <ul>
                    <li dataBank="bancodobrasil" class="bank-flag bancodobrasil">Banco do Brasil</li>
                    <li dataBank="bradesco" class="bank-flag bradesco">Bradesco</li>
                    <li dataBank="itau" class="bank-flag itau">Itau</li>
                    <li dataBank="banrisul" class="bank-flag banrisul">Banrisul</li>
                    <li dataBank="hsbc" class="bank-flag hsbc">HSBC</li>
                </ul>
            </div>
            <div id="boletoData" class="paymentMethodGroup hideMe text-center" dataMethod="boleto">
                <input type="button" id="boletoButton" class="btn btn-success" value="Gerar Boleto"/>
            </div>
        </div>
    </div>
</div>
