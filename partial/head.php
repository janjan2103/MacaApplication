<meta charset="utf-8">
<meta name="lang" http-equiv="content-language" content="<?= LANG ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link type="text/css" rel="stylesheet" href="/assets/css/sweetalert2.css">
<link type="text/css" rel="stylesheet" href="/assets/css/font-awesome.css" /> 
<link type="text/css" rel="stylesheet" href="/assets/css/bootstrap-4.0.0.css" media="screen,projection" />
<link type="text/css" rel="stylesheet" href="/assets/css/bootstrap-flex.css" media="screen,projection" />
<link type="text/css" rel="stylesheet" href="/assets/css/bootstrap-grid.css" media="screen,projection" />
<link type="text/css" rel="stylesheet" href="/assets/css/bootstrap-reboot.css" media="screen,projection" />
<link rel="stylesheet" type="text/css" href="/assets/css/dataTables.bootstrap4.min.css"/>
<link type="text/css" rel="stylesheet" href="/assets/css/style.css" />
<script type="text/javascript" src="/assets/js/jquery-v3.1.1.js"></script>
