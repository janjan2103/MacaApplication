<script>
    function sendForm() {
        $("#box-error").addClass('hideMe');
        var formData = new FormData($("#register-modal-user")[0]);
        $.ajax({
            method: "POST",
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            url: "/login/create.json",
            success: function (data) {
                res = JSON.parse(data);
                if (res.code == 0) {
                    $("#box-success").removeClass("hideMe");
                    $("#message").html(res.message);
                    setTimeout(function () {
                        $("#modal-register").modal("hide");
                        window.location.assign(res.url);
                    }, 3000);
                } else {
                    $("#message-error").html("<strong>Ops!</strong> " + res.message);
                    $("#box-error").removeClass('hideMe');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                console.log("xhr: " + xhr);
                console.log("ajaxOptions: " + ajaxOptions);
                console.log("thrownError: " + thrownError);
                $("#message-error").html("<strong>Ops!</strong> " + JSON.parse(thrownError));
                $("#box-error").removeClass('hideMe');
            }
        });
    }
    $(document).ready(function () {
        $("#modal-register").modal("show");
    });
</script>
<div class="modal fade" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="modal-register-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title text-lg-center text-primary" id="modal-register-label"><?= __('Register') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 hideMe" id="box-error">
                        <div class="alert alert-danger" id="message-error">

                        </div>
                    </div>
                    <div class="col-sm-12 hideMe" id="box-success">
                        <div class="alert alert-success" id="message">

                        </div>
                    </div>
                    <form class="col-sm-12" action="<?= '/login/login/' ?>" method="POST" id="register-modal-user" enctype="multipart/form-data">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="name"><?= __('Name') ?></label>
                                <input class='form-control' type="text" id="name" name="name" placeholder="*<?= __('Obrigatório') ?>" value="" required="" autofocus="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="email"><?= __('E-mail') ?></label>
                                <input class='form-control' type="text" id="email" name="email" placeholder="*<?= __('Obrigatório') ?>" value="" required="" autofocus="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="password"><?= __('Password') ?></label>
                                <input class='form-control' type="password" id="password" name="password" placeholder="*<?= __('Obrigatório') ?>" value="" required="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="re-password"><?= __('Repeat Password') ?></label>
                                <input class='form-control' type="password" id="re-password" name="re_password" placeholder="*<?= __('Obrigatório') ?>" value="" required="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button id="cancel_modal" type="button" class="btn btn-link" data-dismiss="modal"><?= __('Cancel') ?></button>
                <button type="button" class="btn btn-primary" onclick="sendForm(this, 'form#register-modal-user');"><?= __('Sign Up') ?></button>
            </div>
        </div>  
    </div>
</div>