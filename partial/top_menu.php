<div class="bg-inverse pt-1 pb-1">
    <div class="nav nav-pills nav-fill container align-right">
        <span class="text-info lead-lg"><?= __("Maca Application") ?></span>
        <div class="float-lg-right">
            <div class="nav-item mr-1">
                <a class="nav-link btn btn-sm btn-link text-white p-0" href="/"><i class="fa fa-home fa-2x"></i></a>
            </div>
            <?php
            if (Session::isLoged()) {
                ?>
                <div class="nav-item">
                    <div class="dropdown">
                        <button class="btn nav-link btn-secondary dropdown-toggle lead-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= Helper::profileImage($current_user, 'rounded-circle mr-1', ['style="width: 19px;"'], 'fa-1x') . explode(' ', $current_user->name)[0] ?>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <a class="dropdown-item" href="/drilling/main/"><i class="fa fa-diamond"></i> <?= __("Drilling Main") ?></a>
                            <a class="dropdown-item" href="/drill/operator/"><i class="fa fa-user-circle-o"></i> <?= __("Drill Operator") ?></a>
                            <a class="dropdown-item" href="/holes/"><i class="fa fa-bullseye"></i> <?= __("Type of Holes") ?></a>
                            <a class="dropdown-item" href="/shift/"><i class="fa fa-briefcase"></i> <?= __("Shift") ?></a>
                            <hr>
                            <a class="dropdown-item" href="/login/logout/"><i class="fa fa-sign-out"></i> <?= __("Logout") ?></a>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="nav-item">
                    <button class="nav-link btn btn-sm btn-info" id="button-login" onclick="get_modal_login(this)" type="button">Login</button>
                </div>
            <?php } ?>
        </div>
    </div>
</div>