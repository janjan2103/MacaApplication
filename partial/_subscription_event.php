<?php
if ($config->category_payment_style) {
    if (Session::isLoged()) {
        $user_cat = (new UserCategory())->read(['id_user' => $current_user->id_user]);
        if (count($user_cat)) {
            $user_cat = $user_cat[0];
            $category = new Category($user_cat['id_category']);
            $periods = $category->getPeriodsAsObject();
            $current_period = $category->getCurrentPeriod();
            $subscription = TRUE;
        } else {
            $subscription = FALSE;
        }
    } else {
        $subscription = FALSE;
    }
    if ($subscription) {
        ?>
        <div class="col-sm-12 col-lg-8">
            <h5 class="lead-md mt-1 text-primary"><?= __("Você já está incrito nesse evento.") ?></h5>
            <p class="lead text-muted m-0"><?= __("Você está inscrito na categoria ") . "<span class='text-info'>" . $category->title . "</span>" ?></p>
            <p class="lead-sm text-muted"><?= __("A inscrição foi realizada em ") . Validate::formatDateFromWide($user_cat['dtt_register']) ?></p>
        </div>
        <div class="col-sm-12 col-lg-4 text-lg-right">
            <?php
            if ($user_cat['id_status'] >= 3 && $user_cat['id_status'] <= 5) {
                ?>
                <h5 class="lead-sm text-primary mt-1"><?= __("Pagamento da sua inscrição confirmado") ?></h5>
                <a href="<?= routeFor('/activities/') ?>" class="btn btn-success"><?= __("Escolher Atividades") ?></a>
                <?php
            } else {
                ?>
                <h5 class="lead-sm text-primary mt-1"><?= __("Realize o pagamento da sua inscrição") ?></h5>
                <a href="/shopping_cart/" class="btn btn-primary"><?= __("Pagar Inscrição") ?></a>
                <?php
            }
            ?>
        </div>
        <?php
    } else {
        foreach ($list as $nrow => $category) {
            $periods = $category->getPeriodsAsObject();
            $current_period = $category->getCurrentPeriod();
            if ($current_period) {
                echo ($nrow ? '<div class="col-sm-12 pt-0 pb-0"><hr></div>' : '');
                ?>
                <form class="col-sm-12" id="subscription<?= $category->id_category ?>" action="<?= routeFor("/participants/subscription/") ?>" method="POST" enctype="multipart/form-data">
                    <h5 class="col-sm-12 col-lg-3 mt-2 lead text-primary"><big><?= $category->title; ?></big></h5>
                    <div class="col-sm-12 col-lg-6">
                        <span class="card-text text-muted text-justify"><small><?= $category->description; ?></small></span>
                        <?php
                        if ($category->identification_type == 2) {
                            if ((int) $category->require_identification > 0) {
                                ?>
                                <p class="lead text-info">
                                    <?= $category->identification_desc ?>
                                </p>
                            <?php } ?>
                            <?php
                        } elseif ($category->identification_type == 1) {
                            if ((int) $category->require_identification > 0) {
                                ?>
                                <p class="lead text-info">
                                    <?= $category->identification_desc ?>
                                </p>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <div class="col-sm-12 col-lg-3 text-lg-right">
                        <?php
                        foreach ($periods as $key => $period) {
                            if ($period->id_period == $current_period->id_period) {
                                ?>
                                <span class="lead text-primary"><?= __("R$") ?></span>
                                <span class="lead-lg text-primary"><small><?= $period->value; ?></small></span>
                                <p class="text-muted"><small><?= __("Até") ?> <?= Validate::formatDateFromEn($period->dtt_limit); ?></small></p>
                                <?php
                            }
                        }
                        ?>
                        <input type="hidden" value="<?= $category->id_category ?>" name="id_category" />
                        <button type="submit" class="btn btn-primary"><?= __("Participar") ?></button>
                    </div>
                </form>
                <?php
            }
        }
    }
} else {
    if (Session::isLoged()) {
        if (!Session::isLogedParticipant()) {
            ?>
            <div class="col-sm-12 text-center">
                <p class="text-muted lead"><big><?= __("Ao inscrever-se neste evento você terá acesso as atividades que da programação, inscreva-se e imprima seus ingressos.") ?></big></p>
            <a href="<?= routeFor('/participants/subscription/') ?>" class="btn btn-primary"><?= __("Participar") ?> </a>
            </div>
            <?php
        }
    }
}
?>

