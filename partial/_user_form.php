<script type="text/javascript" src="/assets/js/jquery.mask.js"></script>
<script src="/assets/js/intlTelInput.js"></script>
<script>
    $(document).ready(function () {
        console.log("cheguei");
        var telInput = $("#phone"), errorMsg = $("#error-tel-msg"), validMsg = $("#valid-tel-msg");
        var celInput = $("#cellphone"), errorMsgCell = $("#error-cell-msg"), validMsgCell = $("#valid-cell-msg");
        var reset_tel = function () {
            telInput.removeClass("error");
            errorMsg.addClass("hide");
            validMsg.addClass("hide");
        };
        var reset_cel = function () {
            celInput.removeClass("error");
            errorMsgCell.addClass("hide");
            validMsgCell.addClass("hide");
        };
        telInput.blur(function () {
            reset_tel();
            if ($.trim(telInput.val())) {
                if (telInput.intlTelInput("isValidNumber")) {
                    validMsg.removeClass("hide");
                } else {
                    telInput.addClass("error");
                    errorMsg.removeClass("hide");
                }
            }
        });
        telInput.on("keyup change", reset_tel);
        celInput.blur(function () {
            reset_cel();
            if ($.trim(celInput.val())) {
                if (celInput.intlTelInput("isValidNumber")) {
                    validMsgCell.removeClass("hide");
                } else {
                    celInput.addClass("error");
                    errorMsgCell.removeClass("hide");
                }
            }
        });
        celInput.on("keyup change", reset_cel);
        telInput.intlTelInput({
            initialCountry: "auto",
            geoIpLookup: function (callback) {
                $.get('//ipinfo.io', function () {}, "jsonp").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            utilsScript: "/assets/js/utils.js"
        });
        celInput.intlTelInput({
            initialCountry: "auto",
            geoIpLookup: function (callback) {
                $.get('//ipinfo.io', function () {}, "jsonp").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            utilsScript: "/assets/js/utils.js"
        });
        $('#dt_born').mask('00/00/0000');
        $("#register-user").validate({
            rules: {
                name: {
                    required: true,
                    wordcount: 2
                },
                email: {
                    required: true,
                    email: true
                },
                doc_id: {
                    required: true,
                },
                phone: {
                    required: true,
                },
                cellphone: {
                    required: true,
                },
                password: {
                    required: true,
                    minlength: 5,
                },
                re_password: {
                    required: true,
                    equalTo: password
                }
            },
            messages: {
                name: {
                    wordcount: "Digite nome e sobrenome."
                },
                email: {
                    email: "Formato de email inválido."
                },
                doc_id: {
                    required: "Digite seu CPF, RG ou Passaporte (necessário para certificados)"
                },
                password: {
                    minlength: "A senha deve conter no mínimo 5 caracteres."
                }
            }
        });
    });
</script>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="name"><?= __("Nome") ?></label>
            <input class='form-control' type="text" id="name" name="name" placeholder="<?= __('Nome completo') ?>" value="<?= (!empty($current_user->name) ? $current_user->name : (isset($name) ? $name : '')) ?>">
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="col-sm-12 col-lg-6">
        <?php if (empty($current_user->email)) { ?>
            <label for="email"><?= __("E-mail") ?></label>
            <input class='form-control' type="text" id="email" name="email" placeholder="<?= __('exemplo@mail.com') ?>" value="">
            <div class="help-block with-errors"></div>
        <?php } else {
            ?>
            <div class="row">
                <div class="col-sm-12 mt-1">
                    <span class="text-muted lead-sm">Seu e-mail: </span>
                </div>
                <div class="col-sm-12">
                    <h5 class="lead text-info"><?= $current_user->email ?></h5>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="col-sm-12 col-lg-6">
        <div class="form-group">
            <label for="cpf"><?= __("Documento de identificação") ?></label>
            <input class='form-control' type="text" id="doc_id" name="doc_id" placeholder="<?= __('CPF, passaporte ou registro geral.') ?>" value="<?= (!empty($current_user->doc_id) ? $current_user->doc_id : '') ?>">
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="col-sm-12 col-lg-6">
        <div class="form-group row">
            <label for="phone" class="col-sm-3 col-form-label"><?= __("Telefone") ?></label>
            <div class="col-sm-9">
                <input class='form-control' type="text" id="phone" name="phone" value="<?= (!empty($current_user->phone) ? $current_user->phone : (isset($phone) ? $phone : '')) ?>">
                <span id="valid-tel-msg" class="hide text-success lead">✓ <?= __("Válido") ?></span>
                <span id="error-tel-msg" class="hide lead" style="color: #b20000;"> <?= __("Número Inválido") ?></span>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-lg-6">
        <div class="form-group row">
            <label for="dt_born" class="col-sm-3 col-form-label"><?= __("Celular") ?></label>
            <div class="col-sm-9">
                <input class='form-control' type="text" id="cellphone" name="cellphone" value="<?= (!empty($current_user->cellphone) ? $current_user->cellphone : '') ?>">
                <span id="valid-cell-msg" class="hide text-success ml-2 lead">✓ <?= __("Válido") ?></span>
                <span id="error-cell-msg" class="hide ml-2 lead" style="color: #b20000;"> <?= __("Número Inválido") ?></span>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-lg-6">
        <div class="form-group">
            <label for="password"><?= __("Senha") ?></label>
            <input class='form-control' type="password" id="password" name="password" placeholder="<?= __('Defina uma senha') ?>" value="">
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="col-sm-12 col-lg-6">
        <div class="form-group">
            <label for="re_password"><?= __("Repita a Senha") ?></label>
            <input class='form-control' type="password" id="re_password" name="re_password" placeholder="<?= __('Informe novamente') ?>" value="">
            <div class="help-block with-errors"></div>
        </div>
    </div>
</div>