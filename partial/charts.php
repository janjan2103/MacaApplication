<script type="text/javascript">
    function drawChartActivities() {
        <?php
        echo 'var data = google.visualization.arrayToDataTable([';
        echo '["Atividades", "Inscritos", { role: "style" }],';
        $count = 0;
        foreach ($data_chart['activities'] as $row => $col) {
            $count++;
            if (count($data_chart['activities']) > $count) {
                echo "['$row', $col, '" . Helper::getColorChart() . "'],";
            } else {
                echo "['$row', $col, '" . Helper::getColorChart() . "']";
            }
        }
        echo ']);';
        ?>
        var options = {
            title: "Incrições de usuários",
            height: 400,
            legend: {position: "none"},
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("activities_user"));
        chart.draw(data, options);
    }
    function drawChartTickets() {
        var data = google.visualization.arrayToDataTable([
            ["Status", "Ingressos", { role: "style" }],
            ['Aguardando pagamento', <?= $data_chart['tickets']['Aguardando pagamento'] ?>, '#ff9800'],
            ['Bloqueado', <?= $data_chart['tickets']['Bloqueado'] ?>, '#ff5722'],
            ['Cancelado', <?= $data_chart['tickets']['Cancelado'] ?>, '#f44336'],
            ['Confirmado', <?= $data_chart['tickets']['Confirmado'] ?>, '#4caf50'],
            ['Devolvido', <?= $data_chart['tickets']['Devolvido'] ?>, '#9e9e9e'],
            ['Em Disputa', <?= $data_chart['tickets']['Em Disputa'] ?>, '#00bcd4'],
            ['Em Análise', <?= $data_chart['tickets']['Em análise'] ?>, '#2196f3']
            ]);
        var options = {
            title: "Status dos Ingressos",
            height: 400,
            legend: {position: "none"},
        };
        var chart = new google.visualization.PieChart(document.getElementById("tickets"));
        chart.draw(data, options);
    }
    function loadCharts() {
        google.charts.load("current", {packages: ['corechart']});
        google.charts.setOnLoadCallback(drawChartActivities);
        google.charts.load("current", {packages: ['corechart']});
        google.charts.setOnLoadCallback(drawChartTickets);
    }
</script>