<script>
    function sendForm() {
        $("#box-error").addClass('hideMe');
        var formData = new FormData($("#send-email")[0]);
        $.ajax({
            method: "POST",
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            url: "/contacts/send/.json",
            success: function (data) {
                res = JSON.parse(data);
                if (res.code == 0) {
                    $("#box-success").removeClass("hideMe");
                    $("#message").html(res.message);
                    setTimeout(function () {
                        $("#modal-email").modal("hide");
                        window.location.assign("/inbox/");
                    }, 3000);
                } else {
                    $("#message-error").html("<strong>Ops!</strong> " + res.message);
                    $("#box-error").removeClass('hideMe');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                console.log("xhr: " + xhr);
                console.log("ajaxOptions: " + ajaxOptions);
                console.log("thrownError: " + thrownError);
                $("#message-error").html("<strong>Ops!</strong> " + JSON.parse(thrownError));
                $("#box-error").removeClass('hideMe');
            }
        });
    }
    $(document).ready(function () {
        $("#modal-email").modal("show");
    });
</script>
<div class="modal fade" id="modal-email" tabindex="-1" role="dialog" aria-labelledby="modal-email-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title text-lg-center text-primary" id="modal-email-label"><?= __('Enviar email') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 hideMe" id="box-error">
                        <div class="alert alert-danger" id="message-error">

                        </div>
                    </div>
                    <div class="col-sm-12 hideMe" id="box-success">
                        <div class="alert alert-success" id="message">

                        </div>
                    </div>
                    <form class="col-sm-12" action="/contacts/send/" method="POST" id="send-email" enctype="multipart/form-data">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label lead"><?= __('Nome') ?></label>
                                <div class="col-sm-10">
                                    <input class='form-control' type="text" id="name" name="name" placeholder="Digite seu nome completo" value="" required=""/>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="from" class="col-sm-2 col-form-label lead"><?= __('Remetente') ?></label>
                                <div class="col-sm-10">
                                    <input class='form-control' type="text" id="from" name="from" placeholder="aaa@bbb.ccc" required="" value="<?= (!empty($current_user->email) ? $current_user->email : '' ) ?>" <?= (!empty($current_user->email) ? 'readonly=""' : "") ?> />
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="to" class="col-sm-2 col-form-label lead"><?= __('Destinatário') ?></label>
                                <div class="col-sm-10">
                                    <input class='form-control' type="text" id="to" name="to" placeholder="<?= Email::CONTACT_EMAIL ?>" value="<?= Email::CONTACT_EMAIL ?>" required="" readonly="" />
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="subject" class="col-sm-2 col-form-label lead"><?= __('Assunto') ?></label>
                                <div class="col-sm-10">
                                    <input class='form-control' type="text" id="subject" name="subject" placeholder="Dúvida/Erro/Orçamento/Suporte" value="" required="">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="content" class="lead"><?= __('Messagem') ?></label>
                                <textarea class="form-control" name="content" id="content" rows="6"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <input type="hidden" name="contact_type" value="2" />
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button id="cancel_modal" type="button" class="btn btn-link" data-dismiss="modal"><?= __('Cancelar') ?></button>
                <button type="button" class="btn btn-primary" onclick="sendForm(this, 'form#send-email');"><?= __('Entrar') ?></button>
            </div>
        </div>  
    </div>
</div>