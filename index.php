<?php

ob_start();
date_default_timezone_set("America/Sao_Paulo");
try {
    require './helpers/AltoRouter.php';
    require './helpers/Validate.php';
    require './helpers/Helper.php';
    require './helpers/I18n.php';
    require './helpers/Session.php';
    require './helpers/View.php';
    require './helpers/FileHelper.php';
    require './library/php_gettext/gettext.inc';

    spl_autoload_register(function ($class) {
        require_once(str_replace('\\', '/', "model/{$class}.php"));
    });
    Session::bootSession();
    if (array_key_exists('lang', $_GET)) {
        I18n::changeLanguage($_GET['lang']);
    }
    I18n::init();
    $router = new AltoRouter();
    
    $router->map('GET', '/dashboard/?[:action]?/?.[:render]?', function($route) {
        require_once './controller/DefaultController.php';
        new DefaultController($route);
    });
    $router->map('GET|POST', "/drilling/main/?[:action]?/?.[:render]?", function($route) {
        require_once './controller/DrillingMainController.php';
        new DrillingMainController($route);
    });
    $router->map('GET|POST', "/drilling/production/?[:action]?/?.[:render]?", function($route) {
        require_once './controller/DrillingMainController.php';
        new DrillingMainController($route);
    });
    $router->map('GET|POST', '/shift/?[:action]?/?.[:render]?', function($route) {
        require_once './controller/ShiftController.php';
        new ShiftController($route);
    });
    $router->map('GET|POST', '/equipment/?[:action]?/?.[:render]?', function($route) {
        require_once './controller/EquipmentController.php';
        new EquipmentController($route);
    });
    $router->map('GET|POST', '/drill/operator/?[:action]?/?.[:render]?', function($route) {
        require_once './controller/DrillOperatorController.php';
        new DrillOperatorController($route);
    });
    $router->map('GET|POST', '/holes/?[:action]?/?.[:render]?', function($route) {
        require_once './controller/TypeHolesController.php';
        new TypeHolesController($route);
    });
    $router->map('GET|POST', '/login/?[:action]?/?.[:render]?', function($route) {
        require_once './controller/SessionController.php';
        new SessionController($route);
    });
    $router->map('GET', '/', './redirect.php');
    
    $match = $router->match();

    if ($match) {
        if ($match['target'] instanceof Closure) {
            $match['target']($match);
        } else {
            require $match['target'];
        }
        ob_end_flush();
    } else {
        require '404.php';
    }
} catch (Exception $ex) {
    if ($ex->getCode() < 5000 || $ex instanceof PDOException) {
        Session::setError($ex->getCode(), 'Ocorreu um erro, entre em contato!');
    } else {
        Session::setErrorByException($ex);
    }
    ob_clean();
    require './error.php';
}
?>