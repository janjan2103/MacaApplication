<?php

/**
 * Classe para ajudar na composição de elementos via PHP
 */
class Element {

    public $tag = 'button';
    public $content = 'content';
    public $class = '';
    public $properties = [];

    function __construct($tag, $content, array $prop = NULL) {
        $this->tag = $tag;
        $this->content = $content;
        if ($prop) {
            $this->properties = $prop;
        }
    }

    public function __toString() {
        $properties = '';
        foreach ($this->properties as $key => $value) {
            if ($key == 'class') {
                $this->class .= ' ' . $value;
            } else {
                $properties .= " $key=\"$value\"";
            }
        }
        if ($this->tag === 'input') {
            return "<$this->tag class=\"$this->class\" $properties />";
        } else {
            return "<$this->tag class=\"$this->class\" $properties>$this->content</$this->tag>";
        }
    }

}

class ElementActivity extends Element {

    public $activity;
    public $type;

    public function __construct(Activity $activity, $additional = []) {
        $this->type = property_exists($activity, '_activity_type') ? $activity->_activity_type : new ActivityType($activity->id_activity_type);
        parent::__construct('div', $this->content($activity, $additional), [
            'id' => "card-activity-{$activity->id_activity}",
            'data-id' => $activity->id_activity,
            'data-status' => array_key_exists('id_status', $additional) ? $additional['id_status'] : null,
            'data-ticket' => array_key_exists('ticket', $additional) ? $additional['ticket'] : null,
            'class' => 'col-sm-12 col-md-6 col-lg-4'
        ]);
    }

    public function content($activity, $additional) {
        $content = ' <div class="card"><div class="card-block">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item" style="text-align: center">
                    <a href="#activity' . $activity->id_activity . '" onclick="get_modal_detail(this)" data-id="' . $activity->id_activity . '">'
                . '<h5 class="lead">' . $activity->name . '</h5>
                    </a>
                    <hr>
                    </li>
                    <li class="list-group-item p-0"><span class="card-title-discreet"><i class="fa ' . $this->type->icon . '" aria-hidden="true"></i> ' . $this->type->name . ' </span></li>
                    <li class="list-group-item p-0"><span class="label">' . __("Ministrado por") . ': </span><span>' . $activity->presenter . '</span></li>
                    <li class="list-group-item p-0"><span class="label">' . __("Data") . ': </span><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i><b>' . Helper::formatDateTime($activity->dtt_begin) . '</b> ~ ' . Helper::formatDateTime($activity->dtt_end, 'H:i') . '</b></b></span></li>
                    <li class="list-group-item p-0"><span class="label">' . __("Local") . ': </span><b>' . $activity->location . '</b></li>
                    <li class="list-group-item p-0"><span class="label">' . __("Vagas disponíveis") . ': </span><b>' . $activity->vacancies_publish . '</b></li>';
        if ($GLOBALS['config']->activity_payment_style) {
            $content .= '<li class="list-group-item p-0"><span class="label">' . __("Valor") . ': </span><b class="green-text text-darken-3">' . ($activity->unit_value > 0 ? 'R$ ' . number_format($activity->unit_value, 2, ',', '.') : __('Gratuito')) . '</b></li>';
        }
        $content .= '</ul> <hr>';
        if (array_key_exists('ticket', $additional) && !empty($additional['ticket'])) {
            $content .= '<button id="activity-action-' . $activity->id_activity . '" data-activity="' . $activity->id_activity . '" data-ticket="' . $additional['ticket'] . '" class="card-link btn text-white btn-danger activity-action" >' . __("Cancelar") . '</button>';
        } elseif ($additional['occupied'] >= $additional['vacancies']) {
            $content .= '<button class="card-link btn disabled btn-default" disabled="disabled" >' . __("Sem vagas") . '</button>';
        } else {
            $content .= '<button id="activity-action-' . $activity->id_activity . '" data-activity="' . $activity->id_activity . '" class="activity-action card-link btn text-white btn-primary" >' . __("Participar") . '</button>';
        }
        $content .= '<button class="card-link btn btn-default" data-id="' . $activity->id_activity . '" onClick="get_modal_detail(this)"><i class="fa fa-plus text-primary" aria-hidden="true"></i> Info</button>
                    <div class="status mt-1">
                        <i class="fa ' . TicketStatus::icon($additional['id_status'], TRUE) . TicketStatus::colorById($additional['id_status']) . '" aria-hidden="true"></i>
                        <span class="">' . TicketStatus::description($additional['id_status'], TRUE) . ' </span>
                    </div>
                    </div>
                </div>';
        return $content;
    }

}

class TopTabs extends Element {

    const EVENT = 'event';
    const USERS = 'users';
    const ACTIVITIES = 'activities';
    const PAPERS = 'papers';
    const FINANTIAL = 'finantial';
    const SYSTEM = 'system';

    public $itens = [];

    public function __construct($group, $actived_index = 0) {
        parent::__construct('ul', '', ['class' => 'nav nav-tabs']);
        $this->itens = self::$group();
        $this->itens[$actived_index]->content->class .= ' active ';
    }

    public function event() {
        return [
            0 => new Element('li', new Element('a', __('CONFIGURAÇÕES'), ['class' => "nav-link",
                'href' => routeFor("/configs/")]), ['class' => 'nav-item']),
            1 => new Element('li', new Element('a', __('ADMINISTRADORES'), ['class' => "nav-link",
                'href' => routeFor("/administrator/")]), ['class' => 'nav-item']),
            2 => new Element('li', new Element('a', __('CATEGORIAS'), ['class' => "nav-link",
                'href' => routeFor("/categories/")]), ['class' => 'nav-item']),
        ];
    }

    public function users() {
        return [
            1 => new Element('li', new Element('a', __('PARTICIPANTES'), ['class' => "nav-link",
                'href' => routeFor("/participants/")]), ['class' => 'nav-item']),
        ];
    }

    public function activities() {
        return [
            1 => new Element('li', new Element('a', __('ATIVIDADES'), ['class' => "nav-link",
                'href' => routeFor("/activities/")]), ['class' => 'nav-item']),
            2 => new Element('li', new Element('a', __('INGRESSOS'), ['class' => "nav-link",
                'href' => routeFor("/tickets/")]), ['class' => 'nav-item']),
            3 => new Element('li', new Element('a', __('PRESENÇAS'), ['class' => "nav-link",
                'href' => routeFor("/presences/")]), ['class' => 'nav-item']),
            4 => new Element('li', new Element('a', __('VER ATIVIDADES COMO PARTICIPANTE'), ['class' => "nav-link",
                'href' => routeFor("/activities/", ['param' => 'see_like_user'])]), ['class' => 'nav-item']),
        ];
    }

    public function papers() {
        return [
            1 => new Element('li', new Element('a', __('TRABALHOS'), ['class' => "nav-link",
                'href' => routeFor("/papers/")]), ['class' => 'nav-item']),
            2 => new Element('li', new Element('a', __('CRITÉRIOS'), ['class' => "nav-link",
                'href' => routeFor("/criterion/")]), ['class' => 'nav-item']),
            3 => new Element('li', new Element('a', __('LINHAS DE PESQUISA'), ['class' => "nav-link",
                'href' => routeFor("/paper_category/")]), ['class' => 'nav-item']),
            4 => new Element('li', new Element('a', __('AVALIAÇÕES'), ['class' => "nav-link",
                'href' => routeFor("/evaluation/")]), ['class' => 'nav-item']),
        ];
    }

    public function finantial() {
        return [
            1 => new Element('li', new Element('a', __('RECEBIMENTOS'), ['class' => "nav-link",
                'href' => routeFor("/admin/purchases/")]), ['class' => 'nav-item']),
        ];
    }

    public function system() {
        return [
            1 => new Element('li', new Element('a', __('Home'), ['class' => "nav-link",
                'href' => "/dashboard/"]), ['class' => 'nav-item']),
            2 => new Element('li', new Element('a', __('Meus Eventos'), ['class' => "nav-link",
                'href' => "/events/"]), ['class' => 'nav-item']),
            3 => new Element('li', new Element('a', __('Participar'), ['class' => "nav-link",
                'href' => "/participate/"]), ['class' => 'nav-item']),
            4 => new Element('li', new Element('a', __('Contatos'), ['class' => "nav-link",
                'href' => "/inbox/"]), ['class' => 'nav-item']),
        ];
    }

    public function __toString() {
        $properties = '';
        foreach ($this->properties as $key => $value) {
            if ($key == 'class') {
                $this->class .= ' ' . $value;
            } else {
                $properties .= " $key=\"$value\"";
            }
        }
        foreach ($this->itens as $i => $item) {
            $this->content .= $item;
        }

        return "<$this->tag class=\"$this->class\" $properties>$this->content</$this->tag>";
    }

}
