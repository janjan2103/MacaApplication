<?php

/**
 * Description of I18n
 *
 * @author Dalponte
 */
class I18n {

    const SESSION_LANGUAGE_PREFERENCE = 'i18n_language_preference';
    const pt = 'pt_BR';
    const en = 'en_US';

    public static function identify() {
        $locale = self::pt;
        
        if (array_key_exists(self::SESSION_LANGUAGE_PREFERENCE, $_SESSION)) {
            $locale = $_SESSION[self::SESSION_LANGUAGE_PREFERENCE];
//        } elseif (array_key_exists('HTTP_ACCEPT_LANGUAGE', $_SERVER)) {
//            $locale = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);
        }

        if (!defined('LANG')) {
            define('LANG', $locale);
        }
        return $locale;
    }

    public static function changeLanguage($lang) {
        $_SESSION[self::SESSION_LANGUAGE_PREFERENCE] = htmlspecialchars(trim($lang));
    }

    public static function domain($textdomain) {
        _bindtextdomain($textdomain, './i18n');
        _bind_textdomain_codeset($textdomain, 'UTF-8');
        _textdomain($textdomain);
    }

    public static function init($domain = 'general') {
        try {
            $lang = self::identify();

            putenv('LANGUAGE=' . $lang);
            putenv('LANG=' . $lang);
            putenv('LC_ALL=' . $lang);
            putenv('LC_MESSAGES=' . $lang);

            _setlocale(LC_ALL, $lang);
            _setlocale(LC_CTYPE, $lang);

            if ($domain) {
                self::domain($domain);
            }
        } catch (Exception $exc) {
            throw new Exception('It was not possible load language definitions', 7123);
        }
    }

}

class T {

    public static function __callStatic($domain, $arguments) {
        I18n::domain($domain);
        return __($arguments[0]);
    }

}
