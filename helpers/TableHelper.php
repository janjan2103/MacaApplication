<?php

/**
 * Description of TableHelper
 *
 * @author dalponte
 */
class TableHelper {

    public $col_count = 0;
    public $row_count = 0;
    public $table;
    public $thead;
    public $tbody;
    public $tfoot;
    protected $on_field = [];
    protected $on_col = [];
    protected $on_row = NULL;
    protected $fields = FALSE;
    protected $phantom_fields = [];

    public function __call($name, $args) {
        switch ($name) {
            case 'onField':
                if (is_array($args[0])) {
                    foreach ($args[0] as $key => $arg) {
                        if (is_callable($arg)) {
                            $this->onField($key, $arg);
                        } else {
                            return FALSE;
                        }
                    }
                } else {
                    $this->onField($args[0], $args[1]);
                }
                break;
            case 'onColAdd':
                if (is_array($args[0])) {
                    foreach ($args[0] as $key => $arg) {
                        if (is_callable($arg)) {
                            $this->onColAdd($key, $arg);
                        } else {
                            return FALSE;
                        }
                    }
                } else {
                    $this->onColAdd($args[0], $args[1]);
                }
                break;
            case 'onRowAdd':
                $this->onRowAdd($args[0]);
                break;
            default:
                break;
        }
    }

    public function __construct(array $attributes) {
        $html_prop = '';
        foreach ($attributes as $attr => $value) {
            if (is_string($value)) {
                $html_prop .= " $attr=\"$value\" ";
            }
        }
        $this->table = "<table $html_prop cellspacing=\"0\" width=\"100%\"";
    }

    public function setFields(array $fields) {
        $this->fields = $fields;
    }

    private function onField($field, Closure $process) {
        $this->on_field[$field] = $process;
    }

    private function onColAdd($field, Closure $process) {
        $this->on_col[$field] = $process;
    }

    private function onRowAdd(Closure $process) {
        $this->on_row = $process;
    }

    public function build($data) {
        if (is_array($data) && count($data)) {
            $thead = $this->theadLine(reset($data));
            echo $this->table . " data-cols=\"$this->col_count\">" . $thead;
            $this->tBody($data);
            echo $this->tfootLine($data);
            echo '</table>';
        } else {
            echo $this->table . '<tr><td><i style="color: #888">' . __('Não há dados para serem apresentados.') . '</i></td></tr>';
        }
    }

    protected function tBody(array $data) {
        if (!is_array($this->fields)) {
            throw new Exception('Necessário especificar os campos a serem apresentados');
        }
        echo '<tbody>';
        foreach ($data as $i => $row) {
            $on_row = $this->on_row;
            $html = '<tr ' . (is_callable($this->on_row) ? $on_row($row) : " data-ind='$i'") . '>';
            foreach (array_merge($this->fields, $this->phantom_fields) as $field => $alias) {
                $html .= "<td ";
                if (array_key_exists($field, $this->on_col)) {
                    $html .= $this->on_col[$field]($row);
                }
                $value = is_array($row) ? $row[$field] : $row->$field;
                if (array_key_exists($field, $this->on_field)) {
                    $html .= ">{$this->on_field[$field]($value)}</td>";
                } else {
                    $html .= ">{$value}</td>";
                }
            }
            echo $html . '</tr>';
            $this->row_count++;
        }
        echo '</tbody>';
    }

    protected function theadLine($row) {
        if (is_callable($this->thead)) {
            $thead = $this->thead;
            return $thead();
        } else {
            $html = '<tr>';
            $this->col_count = 0;
            if (is_array($this->fields)) {
                foreach ($this->fields as $key => $value) {
                    $this->col_count++;
                    $html .= "<th>$value</th>";
                }
            } elseif (is_object($row)) {
                foreach (get_object_vars($row) as $key => $value) {
                    $this->col_count++;
                    $this->fields[$key] = $key;
                    $html .= "<th>{$key}</th>";
                }
            } elseif (is_array($row)) {
                foreach ($row as $key => $value) {
                    if ($key[0] != '_') {
                        $this->col_count++;
                        $this->fields[$key] = $key;
                        $html .= "<th>{$this->fields[$key]}</th>";
                    }
                }
                $html .= '</tr>';
            }
            return "<thead>{$html}</thead>";
        }
    }

    protected function tfootLine($data = NULL) {
        if (is_callable($this->tfoot)) {
            $tfoot = $this->tfoot;
            return $tfoot($data);
        } elseif ($this->tfoot) {
            return $this->tfoot;
        } else {
            $colspan = $this->col_count - 1;
            return "<tfoot><tr><th colspan=\"{$colspan}\">" . __('Resultados') . "</th><th>{$this->row_count}</th></tr></tfoot>";
        }
    }

}
