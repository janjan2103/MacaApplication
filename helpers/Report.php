<?php

class Report {

    const ERROR_CODE_PREFIX = 72;

    private $_db;
    public $_reports_proprerties;
    public $_sql;
    public $_prop;
    public $_data;

    public function __construct($report_name) {
        $reports_proprerties = self::loadReportProperties();
        if (array_key_exists($report_name, $reports_proprerties)) {
            $this->_prop = $reports_proprerties[$report_name];
        } else {
            throw new Exception('O relatório não pode ser carregado', self::ERROR_CODE_PREFIX . '01');
        }
        $this->getSqlReport($this->file_name);
        return $this;
    }

    function __get($atribute) {
        if ($atribute[0] == '_' && property_exists($this, $atribute)) {
            return $this->$atribute;
        } elseif (array_key_exists($atribute, $this->_prop)) {
            return $this->_prop[$atribute];
        } else {
            return NULL;
        }
    }

    public static function reportProprieties($report_name) {
        switch ($report_name) {
            case 'participant_payments':
                break;
            default:
                break;
        }
    }

    public static function loadReportProperties() {
        return include './reports/ReportsData.php';
    }

    public function connect() {
        try {
            $host = 'localhost';
            $data_base = Event::database();
            $user = Event::database_user();
            $password = Event::database_pass();
            $colation = 'utf8';
            $this->_db = new PDO("mysql:host=$host;dbname=$data_base", "$user", "$password");
            $this->_db->exec("set names $colation");
        } catch (Exception $e) {
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function disconnect() {
        $this->_db = null;
    }

    private function getSqlReport($report_file_name) {
        try {
            if (file_exists("./reports/$report_file_name.sql")) {
                $sql = file_get_contents("./reports/$report_file_name.sql");
                return $sql;
            } else {
                return NULL;
            }
        } catch (Exception $e) {
            
        }
    }

    function execute() {
        try {
            $this->connect();
            $stmt = $this->_db->prepare($this->sql, array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $this->data = $stmt->fetchAll();
                return TRUE;
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                throw new Exception("Falha na execução", "1212");
            }
        } catch (Exception $e) {
            Logger::log_exception($e, __CLASS__, __METHOD__);
            return FALSE;
        } finally {
            $this->disconnect();
        }
    }

    public function columnNumber() {
        if (empty($this->data[0])) {
            return 0;
        } else {
            return count($this->data[0]);
        }
    }

    function getData() {
        return $this->data;
    }

    function getFilter($action) {
        switch ($action) {
            case 'rep_activity_presences':
                return (new Activity())->getList();
            case 'rep_activity_users':
                return (new Participant())->getList();

            default:
                break;
        }
    }

    function getFields($action) {
        switch ($action) {
            case 'rep_activity_presences':
                return Array('id' => 'activity', 'title' => 'name');
            case 'rep_activity_users':
                return Array('id' => 'id_activity', 'title' => 'name');
            default:
                break;
        }
    }

}
