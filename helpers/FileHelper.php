<?php

class FileHelper {

    public static function isFileSent($param) {
        return (!empty($_FILES[$param]['name']) && isset($_FILES[$param]['name']));
    }

    public static function getContentFrom($file_dir, $file_name) {
        $file_path = self::rootDirectory() . $file_dir . $file_name;
        return file_exists($file_path) ? file_get_contents($file_path) : FALSE;
    }

    public static function getErrorFile($file) {
        switch ($file['error']) {
            case '1':
                throw new Exception("Arquivo muito grande, limite de upload: 4Mb", '1030');
            case '2':
                throw new Exception("Arquivo ultrapassou o tamanho limite estipulado pelo sistema", '1030');
            case '3':
                throw new Exception("Arquivo foi parcialmente carregado", '1030');
            case '4':
                throw new Exception("Falha ao carregar o arquivo", '1030');
            case '6':
                throw new Exception("Faltando uma pasta temporária", '1030');
            case '7':
                throw new Exception("Falha ao gravar o arquivo, ver permissões", '1030');
            default:
                Logger::logFor(__CLASS__, "ErrorFile: " . $file['error']);
                break;
        }
    }

    public static function validateLimitSize($file, $limit) {
        if ($file['size'] > $limit) {
            throw new ExceptionController("Falha ao enviar arquivo, selecione um arquivo menor $limit", '1030');
        }
    }

    public static function uploadFile($file, $path_file, $name) {
        Logger::logFor(__CLASS__, $file["tmp_name"] . ' - ' . $path_file . $name);
        if (move_uploaded_file($file["tmp_name"], $path_file . $name)) {
            return TRUE;
        } else {
            self::getErrorFile($file);
        }
    }

    public static function downloadFile($file, $type) {
        if (isset($file) && file_exists($file)) {
            header("Content-Type: " . $type); // informa o tipo do arquivo ao navegador
            header("Content-Length: " . filesize($file)); // informa o tamanho do arquivo ao navegador
            header("Content-Disposition: attachment; filename=" . basename($file)); // informa ao navegador que é tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
            readfile($file); // lê o arquivo
            return TRUE;
        }
    }

    public static function rootDirectory() {
        $document_root = $_SERVER['DOCUMENT_ROOT'];
        $count_chars = strlen($document_root);
        if($document_root[$count_chars-1] == '/') {
            return $document_root;
        } else {
            return $document_root . "/";
        }
    }
    public static function requireFileParam($param, $message = 'Informações incompletas') {
        if (empty($_FILES[$param]['name'])) {
            throw new Exception($message, '10300');
        } else {
            return $_FILES[$param];
        }
    }

    public static function getIfExists($dir, $file) {
        if (file_exists(self::rootDirectory() . "$dir/$file")) {
            return self::rootDirectory() . "$dir/$file";
        }
    }

    public static function ensureDirectoryExistence($dir) {
        if (!is_dir(self::rootDirectory() . "$dir/")) {
            mkdir(self::rootDirectory() . "$dir/", 0777, TRUE);
        }
        return self::rootDirectory() . "$dir/";
    }

    public static function deleteFile($file) {
        if (file_exists($file)) {
            if (unlink($file)) {
                return TRUE;
            } else {
                throw new Exception('Falha ao deletar arquivo.', '1');
            }
        } else {
            return TRUE;
        }
    }

    public static function getTypeFile($file) {
        if (isset($file)) {
            $type_file = strtolower(substr(strrchr(basename($file), "."), 1));
            switch ($type_file) {
                case "pdf": $type = "application/pdf";
                    break;
                case "exe": $type = "application/octet-stream";
                    break;
                case "zip": $type = "application/zip";
                    break;
                case "odt": $type = "application/odt";
                    break;
                case "doc": $type = "application/doc";
                    break;
                case "docx": $type = "application/msword";
                    break;
                case "xls": $type = "application/vnd.ms-excel";
                    break;
                case "ppt": $type = "application/vnd.ms-powerpoint";
                    break;
                case "gif": $type = "image/gif";
                    break;
                case "png": $type = "image/png";
                    break;
                case "jpg": $type = "image/jpg";
                    break;
                case "mp3": $type = "audio/mpeg";
                    break;
                case "php": // deixar vazio por seurança
                    break;
                case "htm": // deixar vazio por seurança
                    break;
                case "html": // deixar vazio por seurança
                    break;
                default : $type = NULL;
                    break;
            }
            return $type;
        }
    }

    public static function checkFormatDoc($file) {
        switch ($file["type"]) {
            case 'application/doc':
                return TRUE;
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                return TRUE;
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.template':
                return TRUE;
            case 'application/vnd.ms-word.document.macroEnabled.12':
                return TRUE;
            case 'application/vnd.ms-word.template.macroEnabled.12':
                return TRUE;
            case 'application/msword':
                return TRUE;
            case 'application/pdf':
                return TRUE;
            case 'application/odt':
                return TRUE;
            default :
                throw new Exception('Formato Incompatível', '1');
        }
    }

    public static function checkFormatImage($file) {
        switch ($file["type"]) {
            case 'image/pjpeg':
                return '.pjpeg';
            case 'image/jpeg':
                return '.jpeg';
            case 'image/jpg':
                return '.jpg';
            case 'image/png':
                return '.png';
            case 'image/gif':
                return '.gif';
            case 'image/bmp':
                return '.bmp';
            default :
                throw new Exception('Formato Incompatível', '1');
        }
    }

    public static function checkFormatIdetification($file) {
        switch ($file["type"]) {
            case 'application/doc':
                return TRUE;
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                return TRUE;
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.template':
                return TRUE;
            case 'application/vnd.ms-word.document.macroEnabled.12':
                return TRUE;
            case 'application/vnd.ms-word.template.macroEnabled.12':
                return TRUE;
            case 'application/msword':
                return TRUE;
            case 'application/pdf':
                return TRUE;
            case 'application/odt':
                return TRUE;
            case 'image/pjpeg':
                return TRUE;
            case 'image/jpeg':
                return TRUE;
            case 'image/png':
                return TRUE;
            case 'image/bmp':
                return TRUE;
            default :
                throw new Exception('Arquivo com formato Incompatível', '7520');
        }
    }

    public static function getFormatIdetification($file) {
        switch ($file["type"]) {
            case 'application/doc':
                return '.docx';
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                return '.docx';
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.template':
                return '.docx';
            case 'application/vnd.ms-word.document.macroEnabled.12':
                return '.docx';
            case 'application/vnd.ms-word.template.macroEnabled.12':
                return '.docx';
            case 'application/msword':
                return '.docx';
            case 'application/pdf':
                return '.pdf';
            case 'application/odt':
                return '.odt';
            case 'image/pjpeg':
                return '.jpg';
            case 'image/jpeg':
                return '.jpg';
            case 'image/png':
                return '.png';
            case 'image/bmp':
                return '.bmp';
            case 'image/gif':
                return '.gif';
            default :
                throw new Exception('Arquivo com formato Incompatível', '7520');
        }
    }

}
