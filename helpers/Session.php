<?php

class Session {

    const USER_SESSION_KEY = 'user';
    const USER_SESSION = 0;
    const ADMIN_SESSION = 1;

    public static function __callStatic($name, $arguments) {
        /**
         * Através de métodos estáticos que não coorespondem aos declarados nesta classe
         * adiciona-se à chave "general" um item nomeado de acordo com o método utilizado
         */
        if (empty($arguments)) {
            return (!empty($_SESSION['general'][$name]) ? $_SESSION['general'][$name] : NULL);
        } else {
            return $_SESSION['general'][$name] = $arguments;
        }
    }

    public static function set($key, $value) {
        return $_SESSION[$key] = $value;
    }

    public static function get($key) {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : FALSE;
    }

    public static function remove($key) {
        /**
         * Remove valor de uso geral setado através de método estático
         */
        unset($_SESSION['general'][$key]);
    }

    public static function unsetKey($key) {
        /**
         * Remove valor de uso geral setado através de método estático
         */
        unset($_SESSION[$key]);
    }

    public static function bootSession() {
        /**
         * Inicia a sessão, caso já não esteja ativa.
         */
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
        }
    }

    public static function destroy() {
        /**
         * Destroy a sessão corrente
         */
        self::bootSession();
        $_SESSION = array(); //atribue um array vazio para a sessão
        session_destroy(); // destrói a sessão
    }

    public static function hasMessage() {
        /**
         * retorna se há uma mensagem a ser exibida para o usuário
         */
        return !empty($_SESSION['message']);
    }

    public static function setMessage($message) {
        /**
         * Adiciona uma mensagem a ser exibida para o usuário
         */
        $_SESSION['message'] = __($message);
    }

    public static function message() {
        /**
         * Retorna a mensagem a ser exibida pelo usuário
         */
        return $_SESSION['message'];
    }

    public static function hasError() {
        /**
         * Retorna se há mensagem de erro a ser exibida para o usuário
         */
        return !empty($_SESSION['error_code']);
    }

    public static function setError($code, $message) {
        /**
         * Adiciona uma mensagem de erro a ser exibida para o usuário
         */
        $_SESSION['error_code'] = $code;
        $_SESSION['error_message'] = __($message);
    }

    public static function setErrorCode($code) {
        /**
         * Adiciona o código da mensagem de erro a ser exibida para o usuário
         */
        $_SESSION['error_code'] = $code;
    }

    public static function errorCode() {
        /**
         * Retorna o código da mensagem de erro a ser exibida para o usuário
         */
        return $_SESSION['error_code'];
    }

    public static function setErrorMessage($message) {
        /**
         * Adiciona a mensagem de erro a ser exibida para o usuário
         */
        $_SESSION['error_message'] = __($message);
    }

    public static function errorMessage() {
        /**
         * Retorna a mensagem de errp a ser exibida para o usuário
         */
        return $_SESSION['error_message'];
    }

    public static function setErrorByException(Exception $ex) {
        /**
         * Adiciona mensagem e código de erro a ser exibida para o usuário com base em uma exceção
         */
        $_SESSION['error_code'] = $ex->getCode();
        $_SESSION['error_message'] = __($ex->getMessage());
    }

    public static function unsetMessages() {
        /**
         * Remove mensagens de erro e/ou sucesso da sessão
         */
        unset($_SESSION['message']);
        unset($_SESSION['error_code']);
        unset($_SESSION['error_message']);
    }

    public static function setCurrentUser($user) {
        /**
         * Adiciona uma sessão de usuário
         */
        $_SESSION['name'] = $user->name;
        $_SESSION['email'] = $user->email;
        $_SESSION['password'] = $user->password;
    }

    public static function isLoged() {
        /**
         * Retorna se há um usuário logado
         */
        return (isset($_SESSION['email']) && isset($_SESSION['password']));
    }

    public static function currentUser() {
        if (self::isLoged()) {
            $email = $_SESSION['email'];
            $password = $_SESSION['password'];
            $user = (new User())->retrive(['email' => $_SESSION['email']]);
            return $user;
        } else {
            return NULL;
        }
    }

    /* FUNÇÕES PARA SEREM UTILIZADAS MAJORITARIAMENTE NAS VIEWS */

    public static function requireSession($current_user) {
        /**
         * Necessário haver a sessão de algum Administrador ou User
         */
        if (empty($current_user)) {
            header('location: ' . Helper::handleError('1010'));
            exit();
        }
    }

    public static function requireUserSession($current_user) {
        /**
         * Necessário haver a sessão de algum Usuário
         */
        if (!$current_user instanceof User) {
            header('location: ' . Helper::handleError('1010'));
            exit();
        }
    }

    public static function requireNoSession($current_user) {
        /**
         * Nenhum usuário deve estar logado
         */
        if (!empty($current_user)) {
            header('location: ' . routeFor("/"));
            exit();
        }
    }

}

function current_user() {
    /**
     * Inicia a sessão, se necessário, e retorna o usuário logado
     */
    Session::bootSession();
    return Session::currentUser();
}

function message_box($colorize = TRUE) {
    if (Session::hasError()) {
        $message = Session::errorMessage();
        Session::unsetMessages();
        $class = $colorize ? 'alert-danger' : 'alert-default';
        $html = "<div class='alert $class m-0 mb-1' role='alert'>"
                . "<strong>Ops! </strong> $message </div>";
    } elseif (Session::hasMessage()) {
        $message = Session::message();
        Session::unsetMessages();
        $class = $colorize ? 'alert-success' : 'alert-default';
        $html = "<div class='alert $class m-0 mb-1' role='alert'>"
                . "<strong><i class='fa fa-check-circle-o' aria-hidden='true'></i></strong> $message </div>";
    } else {
        $html = null;
    }
    return $html;
}
