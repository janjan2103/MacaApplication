<?php

include_once './helpers/elements.php';

class View {

    /**
     * Aceita parametros adicionais, momentaneamente somente ICONES
     * Exemplo: echo View::button('editar', ['class' => 'btn btn-primary'], ['icon' => 'fa-pencil']); 
     * @param type $text
     * @param array $properties
     * @param type $additional
     * @return \Element
     */
    public static function button($text, array $properties = NULL, $additional = NULL) {
        if (is_array($additional) && array_key_exists('icon', $additional)) {
            $add = new Element('i', '', ['class' => 'fa ' . $additional['icon']]);
        } else {
            $add = NULL;
        }
        return new Element('button', $add . " " . __($text), array_merge(['type' => 'button'], $properties));
    }

    public static function hidden($name, $value) {
        return new Element('input', NULL, ['type' => 'hidden', 'name' => $name, 'value' => $value]);
    }

    public static function submit($text = 'Enviar', array $properties = ['class' => 'btn btn-primary float-lg-right']) {
        return new Element('button', __($text), array_merge(['type' => 'submit'], $properties));
    }

    public static function topMenu() {
        return new TopMenu('Nome?', 'Pages?');
    }

    public static function logo($type) {
        $types = [
            'blue' => '/assets/images/logo_azul.png',
            'white' => '/assets/images/logo_branca.png',
            'black' => '/assets/images/logo_preta.png',
        ];
        return new Element('img', NULL, ['src' => EVENTFY_URL . $types[$type], 'style' => 'max-height: 60px; margin-top: 5px;']);
    }

}

class Menu {

    const LOGO_PATH = '/assets/images/logo_branca.png';

    public static function logo() {
        $img = new Element('img', '', [
            'src' => self::LOGO_PATH,
            'class' => 'responsive-img mt-1'
        ]);
        return new Element('a', $img, ['href' => '/']);
    }

}

class TopMenu {

    public $pages;
    public $name;
    public $_user;

    public function __construct($name = NULL, $pages = NULL, $options = ['display_logo' => FALSE]) {
        $this->name = $name;
        $this->pages = $pages;
        $this->options = $options;
    }

    public function __toString() {
        $current_user = current_user();
        include './partial/top_menu.php';
        return '';
    }

}
