<?php

class Helper {

    const URL_LOGIN = '/login/';
    const URL_LOGIN_USER = '/login/';
    const URL_LOGIN_ADMIN = '/admin/';

    public static function getListNotifications() {
        
    }

    public static function createBarPage($name, $pages = FALSE) {
        return new TopMenu($name, $pages);
    }

    public static function control_panel_link($current_user) {
        return '/';
    }

    public static function requireGetParam($param, $redirect_to) {
        if (empty($_GET[$param])) {
            header('location: ' . routeFor($redirect_to));
            exit();
        } else {
            return $_GET[$param];
        }
    }

    public static function getMonthPT($month) {
        switch ($month) {
            case '01':
                return "Jan";
            case '02':
                return "Fev";
            case '03':
                return "Mar";
            case '04':
                return "Abr";
            case '05':
                return "Mai";
            case '06':
                return "Jun";
            case '07':
                return "Jul";
            case '08':
                return "Ago";
            case '09':
                return "Set";
            case '10':
                return "Out";
            case '11':
                return "Nov";
            case '12':
                return "Dez";
            default:
                return "";
        }
    }

    public static function makeFlag($flag) {
        switch ($flag) {
            case 1:
                $country = 'brazil';
                break;
            case 2:
                $country = 'eua';
                break;
            case 3:
                $country = 'spain';
                break;
            case 4:
                $country = 'germany';
                break;
            default:
                $country = 'brazil';
                break;
        }
        return "<img style='max-width: 25px !important;' src='/assets/flags/$country.png' />";
    }

    public static function getMonthExpandPT($month) {
        switch ($month) {
            case '01':
                return "Janeiro";
            case '02':
                return "Fevereiro";
            case '03':
                return "Março";
            case '04':
                return "Abril";
            case '05':
                return "Maio";
            case '06':
                return "Junho";
            case '07':
                return "Julho";
            case '08':
                return "Agosto";
            case '09':
                return "Setembro";
            case '10':
                return "Outubro";
            case '11':
                return "Novembro";
            case '12':
                return "Dezembro";
            default:
                return "";
        }
    }

    public function getDateForExtense($dateBegin, $dateEnd) {
        $day_begin = date("d", strtotime($dateBegin));
        $month_begin = Helper::getMonthPT(date("m", strtotime($dateBegin)));
        $year_begin = date("Y", strtotime($dateBegin));
        if (!empty($dateEnd)) {
            $day_end = date("d", strtotime($dateEnd));
            $month_end = Helper::getMonthPT(date("m", strtotime($dateEnd)));
            $year_end = date("Y", strtotime($dateEnd));
            if ($year_begin != $year_end) {
                $date = "$day_begin de $month_begin de $year_begin a $day_end de $month_end de $year_end";
            } elseif ($month_begin != $month_end) {
                $date = "$day_begin de $month_begin a $day_end de $month_end de $year_begin";
            } else {
                $date = "$day_begin a $day_end de $month_end de $year_begin";
            }
        } else {
            $date = "$day_begin de $month_begin de $year_begin";
        }
        return $date;
    }

    public static function getOptionsTime($param, $selected) {
        $limit = ($param == "h" ? 24 : 60);
        $pass = ($param == "h" ? 1 : 5);
        for ($i = 0; $i < $limit; $i = $i + $pass) {
            if (!empty($selected)) {
                if ($selected == $i) {
                    if ($i < 10) {
                        echo "<option selected>0$i</option>";
                    } else {
                        echo "<option selected>$i</option>";
                    }
                } else {
                    if ($i < 10) {
                        echo "<option>0$i</option>";
                    } else {
                        echo "<option>$i</option>";
                    }
                }
            } else {
                if ($i < 10) {
                    echo "<option selected>0$i</option>";
                } else {
                    echo "<option selected>$i</option>";
                }
            }
        }
    }

    public static function getCharts() {
        $data_charts = [];
        $data_activities = (new Activity())->read();
        foreach ($data_activities as $row => $a) {
            $count_users = count((new Ticket())->read(['id_activity' => $a['id_activity']]));
            $data_charts['activities'][$a['name']] = $count_users;
        }
        $data_ticket_status = (new TicketStatus())->read();
        $old_name = '';
        $count_users = 0;
        foreach ($data_ticket_status as $row => $ts) {
            $count_tickets = count((new Ticket())->read(['id_ticket_status' => $ts['id_ticket_status']]));
            if ($old_name == $ts['name']) {
                $count_tickets += $old_count;
                $old_count = $count_tickets;
            } else {
                $old_count = $count_tickets;
            }
            $old_name = $ts['name'];
            $data_charts['tickets'][$ts['name']] = $count_tickets;
        }
        return $data_charts;
    }

    public static function romanForNumber($roman) {
        $conv = array(
            array("letter" => 'I', "number" => 1),
            array("letter" => 'V', "number" => 5),
            array("letter" => 'X', "number" => 10),
            array("letter" => 'L', "number" => 50),
            array("letter" => 'C', "number" => 100),
            array("letter" => 'D', "number" => 500),
            array("letter" => 'M', "number" => 1000),
            array("letter" => 0, "number" => 0)
        );
        $arabic = 0;
        $state = 0;
        $sidx = 0;
        $len = strlen($roman);
        while ($len >= 0) {
            $i = 0;
            $sidx = $len;
            while ($conv[$i]['number'] > 0) {
                if (@$roman[$sidx] == $conv[$i]['letter']) {
                    if ($state > $conv[$i]['number']) {
                        $arabic -= $conv[$i]['number'];
                    } else {
                        $arabic += $conv[$i]['number'];
                        $state = $conv[$i]['number'];
                    }
                }
                $i++;
            }
            $len--;
        }
        return($arabic);
    }

    public static function numberForRoman($num, $isUpper = true) {
        $n = intval($num);
        $res = '';
        $roman_numerals = array(
            'M' => 1000,
            'CM' => 900,
            'D' => 500,
            'CD' => 400,
            'C' => 100,
            'XC' => 90,
            'L' => 50,
            'XL' => 40,
            'X' => 10,
            'IX' => 9,
            'V' => 5,
            'IV' => 4,
            'I' => 1
        );
        foreach ($roman_numerals as $roman => $number) {
            $matches = intval($n / $number);
            $res .= str_repeat($roman, $matches);
            $n = $n % $number;
        }
        if ($isUpper)
            return $res;
        else
            return strtolower($res);
    }

    public static function CreateCardOptions($register = NULL, $list = NULL, $report = NULL) {
        echo "<div class='row'>" .
        "<div class='card'>";
        if (!empty($register)) {
            echo "<div class='col s10 l2'><a class='btn waves-effect green lighten-1 white-text button-card-option' href='" . routeFor($register) . "'>Registrar</a></div>";
        }
        if (!empty($list)) {
            echo "<div class='col s10 l2'><a class='btn waves-effect blue lighten-1 white-text button-card-option' href='" . routeFor($list) . "'>Lista</a></div>";
        }
        if (!empty($report)) {
            echo "<div class='col s10 l2'><a class='btn waves-effect grey lighten-1 white-text button-card-option' href='" . routeFor($report) . "'>Relatório</a></div>";
        }
        echo "</div>" .
        "</div>";
    }

    public static function cardOptions(Array $buttons) {
        foreach ($buttons as $text => $properties) {
            if (array_key_exists('id', $properties)) {
                $id = "id=\"{$properties['id']}\"";
            } else {
                $id = '';
            }
            echo "<a {$id} class='btn {$properties['color']} button-panel-option' href='{$properties['route']}'>" . __($text) . "</a>";
        }
    }

    public static function isKeyValueOnArray(Array $matrix, $key, $value) {
        foreach ($matrix as $row) {
            if (!empty($row[$key]) && $row[$key] == $value) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public static function handleError($error_code) {
        switch ($error_code) {
            case '1010':
                $error_message = 'Você não tem permissão para acessar esta página!';
                Session::setError($error_code, $error_message);
                return routeFor('/login/');
            case '2010':
                $error_message = 'Você não tem permissão para acessar esta página.';
                Session::setError($error_code, $error_message);
                return routeFor('/login/');
            case '1030':
                $error_message = 'Você precisa informar uma categoria para participação.';
                Session::setError($error_code, $error_message);
                return routeFor('/categories/');
            case '9903':
                $error_message = 'Não foi possível obter o login via facebook.';
                Session::setError($error_code, $error_message);
                return routeFor('/login/');
            case '1037':
                $error_message = 'Você não tem permissão para acessar esta página, ';
                Session::setError($error_code, $error_message);
                return routeFor('/login/');
            case '5010':
                $error_code = NULL;
                $error_message = NULL;
                Session::setError($error_code, $error_message);
                return routeFor('/administrator/');
            case '5030':
                $error_code = NULL;
                $error_message = NULL;
                Session::setError($error_code, $error_message);
                return routeFor('/admin/activities/');
            case '5040':
                $error_code = NULL;
                $error_message = NULL;
                Session::setError($error_code, $error_message);
                return routeFor('/admin/institutions/');
            case '5050':
                $error_code = NULL;
                $error_message = NULL;
                Session::setError($error_code, $error_message);
                return routeFor('/participants/');
            case '7011':
                $error_message = 'O código do QRCode não foi identificado';
                Session::setError($error_code, $error_message);
                return routeFor('/admin/institutions/');
            case '7012':
                $error_message = 'O Ticket não pode ser verificado com este código';
                Session::setError($error_code, $error_message);
                return routeFor('/admin/institutions/');
            default:
                return routeFor('/ops/');
        }
    }

    public static function messageBoxRow($clean_message = TRUE) {
        if (Session::hasError()) {
            echo "<div class='row none-margin'>";
            echo "<div class='red darken-1 message-box'>";
            echo Session::errorMessage();
            echo "</div>";
            echo "</div>";
        } elseif (Session::hasMessage()) {
            echo "<div class='row none-margin'>";
            echo "<div class='light-blue darken-2 message-box'>";
            echo Session::message();
            echo "</div>";
            echo "</div>";
        }
        if ($clean_message) {
            Session::unsetMessages();
        }
    }

    public static function getCardActivity($activity, $min_height = FALSE) {
        ?>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="card <?= ($min_height ? 'card-activity-min' : '') ?>" data-id="<?= $activity['id_activity'] ?>" data-status="<?= $activity['id_status'] ?>">
                <div class="card-block">
                    <h4 class="card-title margin-none"><i class="fa <?= $activity['icon'] ?>" aria-hidden="true"></i> <?= $activity['type']; ?></h4>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><a class="modal-trigger" href="#modal-activity-<?= $activity['id_activity'] ?>"><?= $activity['name'] ?></a></li>
                    <li class="list-group-item"><span class="label"><?= __("Ministrado por") ?>: </span><br/><span><?= $activity['presenter'] ?></span></li>
                    <li class="list-group-item"><span class="label"><?= __("Data") ?>: </span><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i><b><?= $activity['dtt_begin'] ?> <b><?= $activity['dtt_end'] ?></b></b></span></li>
                    <li class="list-group-item"><span class="label"><?= __("Local") ?>: </span><b><?= $activity['location'] ?></b></li>
                    <li class="list-group-item"><span class="label"><?= __("Vagas totais") ?>: </span><b><?= $activity['vacancies'] ?></b></li>

                    <?php
                    if ($GLOBALS['config']->activity_payment_style == 1) {
                        echo '<li class="list-group-item">' . __("Sua vaga estará garantida somente após o pagamento da inscrição.") . '</li>';
                    } else {
                        ?>
                        <li class="list-group-item"><span class="label"><?= __("Valor") ?>: </span><b class="green-text text-darken-3"><?= $activity['unit_value'] > 0 ? 'R$ ' . number_format($activity['unit_value'], 2, ',', '.') : __('Gratuito') ?></b></li>
                        <?php
                    }
                    ?>
                </ul>
                <div class="card-block">
                    <?php
                    if (!empty($activity['ticket'])) {
                        ?>
                        <button data-operation="unsubscribe" data-activity="<?= $activity['id_activity'] ?>" data-ticket="<?= $activity['ticket'] ?>" class="card-link btn text-white btn-danger" ><?= __("CANCELAR") ?></button>
                        <?php
                    } elseif ($activity['occupied'] >= $activity['vacancies']) {
                        ?>
                        <button class="card-link btn text-white disabled btn-default" disabled="disabled" ><?= __("Sem vagas") ?></button>
                        <?php
                    } else {
                        ?>
                        <button data-operation="subscribe" data-activity="<?= $activity['id_activity'] ?>" class="card-link btn text-white btn-primary" ><?= ($activity['type'] == 'Torneio' ? __("INSCREVER EQUIPE") : __("PARTICIPAR") ) ?></button>
                        <?php
                    }
                    ?>
                    <a class="card-link btn btn-default" href="<?= routeFor("/activities/details/", ['id' => $activity['id_activity']]) ?>"><i class="fa fa-plus" aria-hidden="true"></i> Info</a>
                    <div class="card-link status">
                        <i class="fa <?= TicketStatus::icon($activity['id_status']) ?>" aria-hidden="true"></i>
                        <span class=""><?= TicketStatus::description($activity['id_status']) ?> </span>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    public static function createTable(array $array) {
        $colNum = 0;
        echo "<table class='" . (is_string($array['html_class']) ? $array['html_class'] : '') . "' cellspacing='0' width='100%'>";
        $colNum = self::createThead($array, $colNum);
        echo "<tbody>";
        if (!empty($array['list']) && is_array($array['list'])) {
            $colNum = self::createTbody($array, $colNum);
        } else {
            echo "<tr class=''><td>Sem dados para exibição!</td></tr>";
        }
        echo "</tbody>";
        echo "<tfoot>";
        echo "<tr>";
        if (!empty($array['list'])) {
            $colNum -= 1;
            echo "<th colspan='{$colNum}'>Total</th>";
            echo "<th>" . count($array['list']) .
            "</th>";
        }
        echo "</tr>";
        echo "</tfoot>";
        echo "</table>";
    }

    public static function createThead(array $array, $colNum = NULL) {
        echo "<thead>";
        echo "<tr>";
        if (!empty($array['list']) && is_array($array['list'])) {
            if (is_bool($array['line_number']) && $array['line_number']) {
                echo "<th>n º</th>";
                $colNum += 1;
            }
            foreach ($array ['list'][0] as $key => $value) {
                if ($key[0] != '_') {
                    $colNum += 1;
                    if (!empty($array['model'])) {
                        echo "<th>" . (is_string($array['model']) ? $array['model']::getDescriptionKey($key) : $key) . "</th>";
                    } else {
                        echo "<th>" . $key . "</th>";
                    }
                }
            }
            if (!empty($array['options'])) {
                $colNum += 1;
                echo "<th class='right-align'>Opções</th>";
            }
        }
        echo "</tr>";
        echo "</thead>";
        if (!empty($colNum)) {
            return $colNum;
        }
    }

    public static function createTbody(array $array, $colNum = NULL) {
        foreach ($array['list'] as $nrow => $row) {
            echo "<tr> ";
            if (is_bool($array['line_number']) && $array['line_number']) {
                echo "<td>" . ($nrow + 1) . "</td>";
            }
            foreach ($row as $key => $value) {
                if ($key[0] != '_') {
                    echo "<td>$value</td>";
                }
            }
            if (!empty($array['options'])) {
                echo "<td class='right-align'>";
                foreach ($array['options'] AS $option) {
                    self::createLink($option['url'], $row['_id'], $option['icon'], $option['tooltip']);
                }
                echo "</td>";
            }
            echo '</tr>';
        }
        if (!empty($colNum)) {
            return $colNum;
        }
    }

    public static function createDataTable($lst, $url = NULL, $htmlClass = 'bordered', $object = NULL) {
        /* Método para gerar as tags de Select para formulários.

         * @param String $name nome do campo;         
         * @param String $id identificador do campo;         
         * @param String $list array de dados para gerar os options do campo;         
         * @param String $action_js método javascript do campo (opcional);         
         */
        $colNum = 0;
        echo "<table class='{$htmlClass} table dataTable' id='example' cellspacing='0' width='100%'><thead><tr>";
        if (!empty($lst)) {
            $colNum += 1;
            foreach ($lst[0] as $key => $value) {
                if ($key[0] != '_') {
                    $colNum += 1;
                    echo "<th>{$object:: getDescriptionKey($key)}</th>";
                }
            }
        }
        echo "</tr></thead><tbody>";
        if (!empty($lst)) {
            foreach ($lst as $nrow => $row) {
                echo "<tr> ";
                foreach ($row as $key => $value) {

                    if ($key[0] != '_') {
                        echo "<td>$valu e</td>";
                    }
                }
                echo '</tr>';
            }
        } else {
            echo "<tr class=''><td>Sem dados para exibição!</td></tr>";
        }
        echo "</tbody><tfoot><tr>";
        if (!empty($lst)) {
            $colNum -= 1;
            echo "<th colspan='{$colNum}'>Total</th>";
            echo "<th>" . count($lst) . "</th>";
        }

        echo "</tr></tfoot></table>";
    }

    public static function createFormGroupSelect($title, $properties, $value, $text, $class = NULL, $id = NULL, $idselected = NULL) {
        /* Método para gerar as tags de Select para formulários.

         * @param String $name nome do campo;         
         * @param String $id identificador do campo;         
         * @param String $list array de dados para gerar os options do campo;         
         * @param String $action_js método javascript do campo (opcional);         
         */
        echo '<div  class="' . $class . '" id="' . $id . '">';
        if ($title != NULL) {
            
        }
        Helper::createSelect($properties, $value, $text, $idselected);
        echo "<label>$title</label>";
        echo '</div>';
    }

    public static function createSelect(Array $properties, $value, $text, $idselected = NULL) {
        /* Método para gerar as tags de Select para formulários.

         * @param String $name nome do campo;         
         * @param String $id identificador do campo;         
         * @param String $list array de dados para gerar os options do campo;         
         * @param String $action_js método javascript do campo (opcional);         
         */

        echo "<select ";
        foreach ($properties as $row => $val) {
            if ($row != 'list') {
                echo $row . "='" . $val . "' ";
            }
        } echo '>';
        foreach ($properties['list'] as $row => $val) {
            if (isset($idselected) && $idselected == $val["$value"]) {
                echo "<option value='" . $val["$value"] . "' selected='selected' >" . $val["$text"] . "</option>";
            } else {
                echo "<option value='" . $val["$value"] . "'><span class='black-text'>" . $val["$text"] . "</span></option>";
            }
        }
        echo "</select>";
    }

    public static function createFormGroupMultSelect($title, $properties, $value, $text, $data_price, $class = NULL, $idselected = NULL) {
        /* Método para gerar as tags de Select para formulários.

         * @param String $name nome do campo;         
         * @param String $id identificador do campo;         
         * @param String $list array de dados para gerar os options do campo;         
         * @param String $action_js método javascript do campo (opcional);         
         */
        echo '<div class="' . $class . '">';
        if ($title != NULL) {
            echo "<label>$title</label>";
        }
        echo '<div>';
        Helper:: createMultSelect($properties, $value, $text, $data_price, $idselected);

        echo '</div>';
        echo '</div>';
    }

    public static function createMultSelect(Array $properties, $value, $text, $data_price, $idselected = NULL) {
        /* Método para gerar as tags de Select para formulários.

         * @param String $name nome do campo;         
         * @param String $id identificador do campo;         
         * @param String $list array de dados para gerar os options do campo;         
         * @param String $action_js método javascript do campo (opcional);         
         */ echo '<s elect ';
        foreach ($properties as $row => $val) {
            if ($row != 'list') {
                echo $row . "='" . $val . "' ";
            }
        } echo '>';
        foreach ($properties['list'] as $row => $val) {
            if (isset($idselected) && $idselected == $val["$value"]) {
                echo '<option value="' . $val["$value"] . '" data-price="' . $val ["$data_price"] . '" selected="selected" />' . $val["$text"] . "</option>";
            } else {
                echo "<option value='" . $val["$value"] . "' data-price='" . $val["$data_price"] . "'/>" . $val["$text"] . "</option>";
            }
        }
        echo "</select>";
    }

    public static function createInputRadio(Array $list, $name, $title, $idselected = NULL) {
        /* Método para gerar os inputs para formulários.

         * @param String $type tipo de input;         
         * @param String $name nome do input;         
         * @param String $id identificador do input;         
         * @param String $value Valor do input;         
         * @param String $title titulo do label (opcional);         
         */
        $count = 0;
        echo "<div id='$name'>";
        echo "<div class='card-title grey-text text-darken-2 padding-bottom'>$title</div>";
        foreach ($list as $row => $input) {
            $count++;
            if ($idselected == $input['id']) {
                echo "<p class='col s12 m4 margin-top'>
            <input name='$name' type='radio' id='" . $name . $count . "' value='{$input['id']}' checked='' />
            <label for='" . $name . $count . "'> {$input['title']} </label>
            </p>";
            } else {
                echo "
                    <p class='col s12 m4 margin-top'>
            <input name='$name' type='radio' id='" . $name . $count . "' value='{$input['id']}' />
            <label for='" . $name . $count . "'> {$input['title']} </label>
            </p>";
            }
        }
        echo '</div>';
    }

    public static function createFormGroupInput($title, Array $properties, $class = NULL, $id = NULL) {
        /* Método para gerar os inputs para formulários.

         * @param String $type tipo de input;         
         * @param String $name nome do input;         
         * @param String $id identificador do input;         
         * @param String $value Valor do input;         
         * @param String $title titulo do label (opcional);         
         */

        echo '<div class="' . $class . '" id="' . $id . '">';
        if ($title != NULL) {
            echo "<label>$title</label>";
        }
        echo ''
        . '<div>';
        Helper::createInput($properties);

        echo '</div>'
        . '</div>';
    }

    public static function createInput(Array $properties) {
        /* Método para gerar os inputs para formulários.

         * @param String $type tipo de input;         
         * @param String $name nome do input;         
         * @param String $id identificador do input;         
         * @param String $value Valor do input;         
         * @param String $title titulo do label (opcional);         
         */

        echo '<input';

        foreach ($properties as $key => $value) {
            echo ' ' . $key . ' = "' . $value .
            '"';
        }
        echo ' />';
    }

    public static function createSubmit($name, $title, $class = NULL) {
        echo

        '<div class="btn-group btn-group-vertical">'
        . '<button type="submit" class="' . $class . '" name="' . $name . '" value=""><i> </i>' .
        $title . '</button>'
        . '</div>';
    }

    public static function createLink($text, $class, $id, array $properties = null) {
        return "<button type='button' class='$class' id='$id' " . implode(' ', $properties) . "><i class='fa $icon' aria-hidden='true'></i>  $text</button>";
    }

    public static function createFormGroupCheckbox($title, Array $properties, $class = NULL, $id = NULL
    ) {
        /* Método para gerar os inputs para formulários.

         * @param String $type tipo de input;         
         * @param String $name nome do input;         
         * @param String $id identificador do input;         
         * @param String $value Valor do input;         
         * @param String $title titulo do label (opcional);         
         */
        echo '<div class="' . $class . '" id="' . $id . '">';
        Helper::createCheckBox($properties);
        if ($title != NULL) {
            echo "<label>$title

</label>";
        }
        echo '</div>';
    }

    public static function createCheckBox(Array $properties) {
        /* Método para gerar os inputs para formulários.

         * @param String $type tipo de input;         
         * @param String $name nome do input;         
         * @param String $id identificador do input;         
         * @param String $value Valor do input;         
         * @param String $title titulo do label (opcional);         
         */

        echo '<input ';

        foreach ($properties as $key => $value) {
            if ($key == 'value' && $value == '1') {
                echo 'checked="checked" ' . $key . ' = "' . $value . '"';
            } else {
                echo ' ' . $key . ' = "' . $value . '"';
            }
        }
        echo '/>';
    }

    public static function formatDateTime($datetime, $format = 'd/m/Y H:i') {
        /* Método para formatar a data de acordo com a necessidade para exibi-la.

         */

        if (!empty($datetime)) {
            $date = date("$format", strtotime($datetime));
            return $date;
        } else {
            echo '';
        }
    }

    public static function formatNameToShort($name, $maxLength = 15, $words = 1) {
        /* Método para formatar a data de acordo com a necessidade para exibi-la.
         */

        $short = explode(' ', $name)[0];
        if (count($short) > $maxLength) {
            return substr(count($short), $maxLength);
        } else {
            return $short;
        }
    }

    public static function formatToPrice($number) {
        if (is_numeric($number)) {
            return number_format($number, 2, ',', '.');
        } else {
            return 0;
        }
    }

    public static function gravatarImage($userObj, $class = '', array $properties = array()) {
        if (strpos(get_headers("//0.gravatar.com/avatar/" . md5(strtolower(trim($userObj->email))) . "?d=" . urlencode('//trendcongresso.com.br/assets/images/account_circle.png') . "&s=40", 1)[0], "404") == false) {
            $gravatar_image = "//www.gravatar.com/avatar/" . md5(strtolower(trim($userObj->email))) . "?d=" . urlencode('//trendcongresso.com.br/assets/images/account_circle.png') . "&s=40";
            return '<img src = "' . $gravatar_image . '" class="circle">';
        } else {
            return NULL;
        }
    }

    public static function profileImage($userObj, $class = 'profile_img', array $properties = array(), $tam_icon = 'fa-5x') {
        if (!empty($userObj) && $userObj->profileImageLink() && ($userObj instanceof Participant || $userObj instanceof Admin || $userObj instanceof User)) {
            return '<img src="' . $userObj->profileImageLink() . '" class="rounded-circle ' . $class . '"' . implode(' ', $properties) . '/>';
        } else {
            return '<i class="fa ' . $tam_icon . ' fa-user" aria-hidden="true"></i> ';
        }
    }

    public static function getColorChart() {
        $data = ["#00657F", "#68E0FF", "#00CAFF", "#52767F", "#00A1CC", "#00777F", "#33F1FF", "#00EEFF", "#3B7B7F", "#00BFCC"];
        return $data[rand(0, count($data)-1)];
    }

    public static function cardList($name, $image, $description, $route) {
        echo "<div class='col-sm-12 col-lg-4 text-lg-center'>
            <div class = 'card card-default card_square'>
            <a href='$route'>
            <div class = 'card-block'>
            <img class='card-img-top' src='$image'/>
            <h4 class = 'card-title m-1'>" . __($name) . "</h4>
            <div class='card-text lead'>" . __($description) . "</div>
            </div>
            </a>
            </div></div>";
    }

}
