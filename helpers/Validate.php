<?php

class Validate {

    public static function email($email) {
        $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
        return (bool) preg_match($pattern, $email);
    }

    public static function validate_phone($phone) {
        $pattern = '/\([0-9]{2}\)([0-9]{4}|[0-9]{5})-[0-9]{4}/';
        return (bool) preg_match($pattern, $phone);
    }

    public static function decimal($value) {
        $value = str_replace(',', '.', $value);
        if ((is_double($value) || is_numeric($value)) && $value >= 0) {
            return (float) $value;
        } else {
            return 0;
        }
    }

    public static function string($value, $min_lenght) {
        $lenght = strlen($value);
        if ($lenght > $min_lenght) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function integer($value, $positive = FALSE) {
        $num = 0;
        if (ctype_digit(strval($value))) {
            $num = strval($value);
        } else {
            $num = (int) $value;
        }
        if ($positive) {
            if ($num > 0) {
                return $num;
            } else {
                return FALSE;
            }
        }
        return $num;
    }

    public static function replaceSpecialChars($string) {
        $replace = [
            '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '',
            '&quot;' => '', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae',
            '&Auml;' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Æ' => 'Ae',
            'Ç' => 'C', 'Ć' => 'C', 'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D',
            'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E',
            'Ę' => 'E', 'Ě' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G',
            'Ġ' => 'G', 'Ģ' => 'G', 'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I',
            'Î' => 'I', 'Ï' => 'I', 'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I',
            'İ' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J', 'Ķ' => 'K', 'Ł' => 'K', 'Ľ' => 'K',
            'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N',
            'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
            'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O', 'Ŏ' => 'O',
            'Œ' => 'OE', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Š' => 'S',
            'Ş' => 'S', 'Ŝ' => 'S', 'Ș' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
            'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ū' => 'U',
            '&Uuml;' => 'Ue', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U',
            'Ŵ' => 'W', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ź' => 'Z', 'Ž' => 'Z',
            'Ż' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
            'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
            'æ' => 'ae', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
            'ď' => 'd', 'đ' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
            'ë' => 'e', 'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e',
            'ƒ' => 'f', 'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h',
            'ħ' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i',
            'ĩ' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĳ' => 'ij', 'ĵ' => 'j',
            'ķ' => 'k', 'ĸ' => 'k', 'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l',
            'ŀ' => 'l', 'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n',
            'ŋ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
            '&ouml;' => 'oe', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o', 'ŏ' => 'o', 'œ' => 'oe',
            'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
            'û' => 'u', 'ü' => 'ue', 'ū' => 'u', '&uuml;' => 'ue', 'ů' => 'u', 'ű' => 'u',
            'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ý' => 'y', 'ÿ' => 'y',
            'ŷ' => 'y', 'ž' => 'z', 'ż' => 'z', 'ź' => 'z', 'þ' => 't', 'ß' => 'ss',
            'ſ' => 'ss', 'ый' => 'iy', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G',
            'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
            'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
            'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F',
            'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SCH', 'Ъ' => '',
            'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'а' => 'a',
            'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo',
            'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's',
            'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e',
            'ю' => 'yu', 'я' => 'ya'
        ];
        return preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(array_keys($replace), $replace, $string)));
    }

    public static function cep($cep) {
        return str_replace('-', '', $cep);
    }

    public static function phone($phone) {
        return str_replace(array('-', '.', ' ', '/', '*', '', '(', ')'), '', $phone);
    }

    public static function cpf($cpf) {
        $cpf = str_replace(array('.', '-', ' '), '', $cpf);
        if (self::validateCpf($cpf)) {
            return $cpf;
        } else {
            return FALSE;
        }
    }

    public static function cnpj($cnpj) {
        $cnpj = str_replace(array('.', '-', ' '), '', $cnpj);
        return $cnpj;
    }

    public static function validateEmail($email) {
//        return TRUE;
        if (!preg_match("^[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\-]+\.[a-z]{2,4}$", $email)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    // Validar CEP (xxxxx-xxx)
    public static function validatePostal_Cod($postal_cod) {
        if (!preg_match("^[0-9]{5}-[0-9]{3}$", $postal_cod)) {
            return $this->message(1, 'cep', null, null);
        } else {
            return TRUE;
        }
    }

    // Validar Datas (DD/MM/AAAA)
    public static function validateDate($date) {
        if (!preg_match("^[0-9]{2}/[0-9]{2}/[0-9]{4}$", $date)) {
            return $this->message(2, 'data', null, null);
        } else {
            return TRUE;
        }
    }

    public static function validateFone($fone) {
//        $string = "(32)5555-5555";
        if (!preg_match("(\([1-9][1-9]\))([0-9]){8,9}", $fone)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    // Validar CPF (111111111111)
    public static function validateCpf($cpf) {

        if (!is_numeric($cpf)) {
            $status = false;
        } else {
            # Pega o digito verificador
            $dv_informado = substr($cpf, 9, 2);

            for ($i = 0; $i <= 8; $i++) {
                $digito[$i] = substr($cpf, $i, 1);
            }
            # Calcula o valor do 10� digito de verifica��o
            $posicao = 10;
            $soma = 0;

            for ($i = 0; $i <= 8; $i++) {
                $soma = $soma + $digito[$i] * $posicao;
                $posicao = $posicao - 1;
            }

            $digito[9] = $soma % 11;

            if ($digito[9] < 2) {
                $digito[9] = 0;
            } else {
                $digito[9] = 11 - $digito[9];
            }

            # Calcula o valor do 11 digito de verificação
            $posicao = 11;
            $soma = 0;

            for ($i = 0; $i <= 9; $i++) {
                $soma = $soma + $digito[$i] * $posicao;
                $posicao = $posicao - 1;
            }

            $digito[10] = $soma % 11;

            if ($digito[10] < 2) {
                $digito[10] = 0;
            } else {
                $digito[10] = 11 - $digito[10];
            }

            # Verifica de o dv � igual ao informado
            $dv = $digito[9] * 10 + $digito[10];

            if ($dv != $dv_informado) {
                $status = false;
            } else {
                $status = true;
            }
        }
        return $status;
    }

    // Validar Numero
    public static function validateNumber($number) {
        if (!is_numeric($number)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    // Validar URL
    public static function validateUrl($url) {
        return filter_var($url, FILTER_VALIDATE_URL);
    }

    public static function getWordsNumber($text) {
        $count = count(explode(' ', $text));
        return $count;
    }

    public static function truncateString($text, $count_caracters) {
        return substr($text, 0, $count_caracters);
    }

// Verificação simples (campos vazios, ultrapassar máximo e mínimo de caracteres)

    public static function validateField($value, $max, $min) {
        if ($value == "") {
            return FALSE;
        } elseif (strlen($value) > $max) {
            return FALSE;
        } elseif (strlen($value) < $min) {
            return FALSE;
        }
        return TRUE;
    }

    public static function clear_string($string) {
//        $chars = array("'", ";", "!", "@", "#", "&", "$", ",", ".", "%", "\"", '\\');
        $chars = array("'", "\\", "\"");
        return str_replace($chars, "", $string);
    }

    public static function dateToday() {
        return date('d/m/Y H:i:s');
    }

    public static function formatPhone($phone) {
        $ddd = substr($phone, 0, 2);
        if (strlen($phone) > 10) {
            $part1 = substr($phone, 2, 5);
            $part2 = substr($phone, 7);
        } else {
            $part1 = substr($phone, 2, 4);
            $part2 = substr($phone, 6);
        }
        return "($ddd) $part1-$part2";
    }

    public static function formatDateTimeFromPtBr($brDate) {
        $date = str_replace('/', '-', $brDate);
        return date('Y-m-d H:i:s', strtotime($date));
    }

    public static function formatDateFromPtBr($brDate) {
        $date = str_replace('/', '-', $brDate);
        return date('Y-m-d', strtotime($date));
    }

    public static function formatDateTimeFromEn($enDate) {
        return date('d/m/Y H:i', strtotime($enDate));
    }

    public static function formatDateFromEn($enDate) {
        return date('d/m/Y', strtotime($enDate));
    }

    public static function formatTimeFromEn($enDate) {
        return date('H:i', strtotime($enDate));
    }

    public static function formatDateFromWide($date_begin, $date_end = NULL) {
        $date_event = "";
        $date_begin = Validate::formatDateFromPtBr($date_begin);
        if (!empty($date_end)) {
            $date_end = new DateTime(Validate::formatDateFromPtBr($date_end));
        }
        $date_begin = new DateTime($date_begin);
        $d_begin = date_format($date_begin, "d");
        $m_begin = Helper::getMonthExpandPT(date_format($date_begin, "m"));
        $y_begin = date_format($date_begin, "Y");
        if (empty($date_end)) {
            return $d_begin . " de " . $m_begin . " de " . $y_begin;
        } else {
            $d_end = date_format($date_end, "d");
            $m_end = Helper::getMonthExpandPT(date_format($date_end, "m"));
            $y_end = date_format($date_end, "Y");
            if ($y_begin != $y_end) {
                return $date_begin . " a " . $date_end;
            }
            if ($d_begin != $d_end && $m_begin == $m_end) {
                $date_event = "De " . $d_begin . " a " . $d_end . " de " . $m_begin;
            } elseif ($m_begin != $m_end) {
                $date_event = "De " . $d_begin . " de " . $m_begin . " a " . $d_end . " de " . $m_end;
            } else {
                $date_event = "Dia " . $d_begin . " de " . $m_begin;
            }
            $date_event = $date_event . " de " . $y_begin;
            return $date_event;
        }
    }

}

?>