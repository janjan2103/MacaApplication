<?php

/**
 * Helper para utilização do pagamento do pagseguro
 * @author dalponte
 */
class Pagseguro {

    const SANDBOX_CHECKOUT_URI = '//sandbox.ws.pagseguro.uol.com.br/v2/checkout/';
    const PAGSEGURO_CHECKOUT_URI = 'https://ws.pagseguro.uol.com.br/v2/checkout/';

    /**
     * @return PagSeguroAccountCredentials Credencial do pagseguro
     */
    public static function credentials() {
        $env = Event::enviromentValue('pg_enviroment');
        if (empty($env)) {
            return PagSeguroConfig::getAccountCredentials();
        } else {
            PagSeguroConfig::setEnvironment($env);
            $email = Event::enviromentValue($env)['email'];
            $token = Event::enviromentValue($env)['token'];
            return new PagSeguroAccountCredentials($email, $token);
        }
    }

    public static function notificationUrl() {
        return '//' . $_SERVER['SERVER_NAME'] . '/' . Event::id() . '/pagseguro/notification/';
    }

    public static function redirectUrl() {
        return '//' . $_SERVER['SERVER_NAME'] . '/' . Event::id() . '/pagseguro/callback/';
    }

}
