<?php

/**
 * Description of GoogleReCaptcha
 * @author dalponte
 */
class GoogleReCaptcha {

    const SITE_KEY = "6LcRgAgTAAAAADlPaZ7ltyHQ3_G0gxH3CJOAawZu";

    static public function proprietyDataSitekey() {
        return 'data-sitekey="' . self::SITE_KEY . '"';
    }
    static public function spanReCaptcha() {
        return '<span class="g-recaptcha" data-sitekey="' . self::SITE_KEY . '"></span>';
    }

    static public function verifyCaptchaCode($captcha) {
        if (empty($captcha)) {
            return FALSE;
        }
        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcRgAgTAAAAAHr5G4YsC5cnNy0FM8pp1vPbNPzd&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']), true);
        if (!empty($response['success'])) {
            return $response['success'];
        } else {
            return FALSE;
        }
    }

}
