<?php

/*
 * ***********************************************************************
  PagSeguro Config File
 * ***********************************************************************
 */

$PagSeguroConfig = array();

$PagSeguroConfig['environment'] = "production"; // production, sandbox

$PagSeguroConfig['credentials'] = array();
$PagSeguroConfig['credentials']['email'] = "financeiro@trendsoft.co";
$PagSeguroConfig['credentials']['token']['production'] = "B2F94ADC8F304F14A41BE339CDB11DA3"; 
$PagSeguroConfig['credentials']['token']['sandbox'] = "221B6F947A8A4963987D65297AE21973"; //dalpontemaico@gmail.com

$PagSeguroConfig['application'] = array();
$PagSeguroConfig['application']['charset'] = "UTF-8"; // UTF-8, ISO-8859-1

$PagSeguroConfig['log'] = array();
$PagSeguroConfig['log']['active'] = true;
$PagSeguroConfig['log']['fileLocation'] = $_SERVER['DOCUMENT_ROOT'] . "/log/";
