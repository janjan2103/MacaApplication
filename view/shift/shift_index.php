<html>
    <head>
        <?php include './partial/head.php'; ?>
        <title>Shift</title>
    </head>
    <body>
        <?= Helper::createBarPage("Shifts", TRUE); ?>
        <?php include './partial/menu.php'; ?>
        <main id="main" class="row pt-2">
            <div class="col-sm-12">
                <div class="section_message">
                    <?= message_box() ?>
                </div>
                <div class="mb-1 text-lg-right">
                    <?= View::button('New Shift', ['class' => 'register btn btn-sm btn-primary',
                        'onclick' => 'get_modal_register(this)',
                        'id' => 'id_shift',
                        'data-shift' => $shift->id_shift], ['icon' => 'fa-plus']);
                    ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <?php
                        $table = new TableHelper(['id' => 'table', 'class' => 'table table-bordered']);
                        $table->setFields([
                            'id_shift' => 'ID',
                            'code' => 'Code',
                            'qtd_hours' => 'Qtd of Hours',
                            'options' => 'Edit'
                        ]);
                        $table->onRowAdd(function ($object) {
                            return " id='item-{$object['id_shift']}' data-id='{$object['id_shift']}' class='render-trigger'";
                        });
                        $table->onField([
                            'options' => function ($value) {
                                $html = "<div style='width=100%'><button type='button' class='btn btn-link edit-shift' id='edit-$value' data-shift='$value'><i class='fa fa-pencil'></i> Edit</button></div>";
                                return $html;
                            }
                        ]);
                        $table->build($list);
                        ?>
                    </div>
                </div>
            </div>
            <div id="partial-modal">
                <div class="modal_content">
                </div>
            </div>
        </main>
        <?php include './partial/scripts.php'; ?>
        <script src="/assets/js/jquery.renderOn.js"></script>
        <script>
            $(document).ready(function () {
                $('tr.render-trigger').click(function () {
                    $('#table').renderOn_handle('tr.render-trigger', {
                        'route': "/shift/info.partial"
                    });
                });
                $(".edit-shift").click(function (el) {
                    $(this).renderOn_load({
                        'route': "/shift/modal_edit.partial",
                        container: "#partial-modal"
                    });
                    el.stopPropagation();
                });
            });
            var get_modal_register = function (el) {
                $(el).renderOn_load({
                    'route': "/shift/modal_register.partial",
                    container: "#partial-modal"
                });
            };
        </script>
    </body>
</html>
