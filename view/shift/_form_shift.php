<div class="row">
    <div class="form-group col-sm-12 col-lg-6">
        <label for="code"><?= __('Code') ?></label>
        <input class='form-control' type="text" id="code" name="code" placeholder="*<?= __('Insert the code of the shift') ?>" value="<?= (empty($shift->code) ? '' : $shift->code) ?>" required="">
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group col-sm-12 col-lg-6">
        <label for="qtd_hours"><?= __('Qtd. of hours') ?></label>
        <input class='form-control' type="text" id="qtd_hours" name="qtd_hours" placeholder="*<?= __('Insert the quantity of hours for the shift.') ?>" value="<?= (empty($shift->qtd_hours) ? '' : $shift->qtd_hours) ?>" required="">
        <div class="help-block with-errors"></div>
    </div>
</div>