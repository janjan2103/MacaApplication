<html>
    <head>
        <?php include './partial/head.php'; ?>
        <link href="/assets/css/jquery-ui.css" rel="stylesheet" type="text/css" /> 
        <link href="/assets/css/jquery.datetimepicker.min.css" rel="stylesheet" type="text/css" /> 
        <title>Drilling Main</title>
    </head>
    <body>
        <?= Helper::createBarPage("Edit Drilling Main"); ?>
        <?php include './partial/menu.php'; ?>
        <main id="main" class="row pt-2">
            <div class="col-sm-12">
                <div class="section_message">
                    <?= message_box() ?>
                </div>
                <div class="mb-1 text-lg-right">
                    <a class="btn btn-sm btn-primary text-white" href="/drilling/main/"><i class="fa fa-backward"></i> Return</a>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <?php include './view/drilling_main/_form_drilling_main.php' ?>
                </div>
            </div>
        </main>
        <?php include './partial/scripts.php'; ?>
        <script type="text/javascript" src="/assets/js/sweetalert2.js"></script>
        <script type="text/javascript" src="/assets/js/jquery.mask.js"></script>
        <script type="text/javascript" src="/assets/js/jquery.tmpl.js"></script>
        <script type="text/javascript" src="/assets/js/jquery-ui.custom.min.js"></script>
        <script src="/assets/js/jquery.datetimepicker.full.min.js"></script>
        <script id="productionsRowTemplate" type="text/x-jquery-tmpl">
            <?php
            $productions = $drilling_main->getProductions();
            ?>
            <div class='col-sm-12 new_production mt-1' data-id="${id_drilling_production}">
            <div class='col-sm-12 col-lg-2 type_holes'>
                <select class="form-control mb-1" name="productions[${id_drilling_production}][id_type_holes]">
                    <option id="hole-select" value="0">Select an item...</option>
                    <?php
                    $type_holes = (new TypeOfHoles())->getList();
                    foreach ($type_holes as $row => $type_hole) {
                        if ($type_hole['id_type_holes'] == $drilling_main->id_type_holes) {
                            echo "<option value='{$type_hole['id_type_holes']}' selected>{$type_hole['type']}</option>";
                        } else {
                            echo "<option value='{$type_hole['id_type_holes']}'>{$type_hole['type']}</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class='col-sm-12 col-lg-1 meters'><input name="productions[${id_drilling_production}][meters]" value="${meters}" class="meters input-group form-control" type="text" ></div>
            <div class='col-sm-12 col-lg-2 number_holes'><input name="productions[${id_drilling_production}][number_holes]" value="${number_holes}" class="number_holes input-group form-control" placeholder="" type="text" ></div>
            <div class='col-sm-12 col-lg-2 bit_prefix'><input name="productions[${id_drilling_production}][bit_prefix]" value="${bit_prefix}" class="bit_prefix input-group form-control" placeholder="" type="text" ></div>
            <div class='col-sm-12 col-lg-2 bit_number'><input name="productions[${id_drilling_production}][bit_number]" value="${bit_number}" class="bit_number input-group form-control" placeholder="" type="text" ></div>
            <div class='col-sm-12 col-lg-2 bit_sharp'><input name="productions[${id_drilling_production}][bit_sharp]" value="${bit_sharp}" class="bit_sharp input-group form-control" placeholder="" type="text"  onchange="addDetail();"></div>
            <div class='col-sm-12 col-lg-1 button_remove'>
            <button type="button" class="remove-detail btn btn-flat btn-danger">
            <i class="fa fa-remove"></i>
            </button>
            </div>
            </div>
        </script>
        <script>
            var detailData = function () {
                return <?= json_encode($productions)?>;
            };
        </script>
        <script src="/assets/js/drilling-main-master-detail.js"></script>
    </body>
</html>