<html>
    <head>
        <?php include './partial/head.php'; ?>
        <title>Drilling Main</title>
    </head>
    <body>
        <?= Helper::createBarPage("Drilling Main", TRUE); ?>
        <?php include './partial/menu.php'; ?>
        <main id="main" class="row pt-2">
            <div class="col-sm-12">
                <div class="section_message">
                    <?= message_box() ?>
                </div>
                <div class="mb-1">
                    <span class="lead-md text-muted"><?= __("Click in a row for expand the informations about the drilling.") ?></span>
                    <a class="register btn btn-sm btn-primary float-lg-right" href="/drilling/main/register/"><i class="fa fa-plus"></i> New Drilling</a>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <?php
                        $table = new TableHelper(['id' => 'table', 'class' => 'table table-bordered']);
                        $table->setFields([
                            'shift' => 'Shift',
                            'operator' => 'Operator',
                            'equipment' => 'Equipment',
                            'date_drilling' => 'Date',
                            'hour_start_meter' => 'Start Meter',
                            'start_hamer' => 'Start Hamer'
                        ]);
                        $table->onRowAdd(function ($object) {
                            return " id='item-{$object['id_drilling_main']}' data-id='{$object['id_drilling_main']}' class='render-trigger'";
                        });
                        $table->onField([
                            'shift' => function ($value) {
                                $html = Shift::getBadge($value);
                                return $html;
                            },
                            'equipment' => function ($value) {
                                $html = Equipment::getBadge($value);
                                return $html;
                            },
                            'date_drilling' => function ($value) {
                                $html = Validate::formatDateTimeFromEn($value);
                                return $html;
                            }
                        ]);
                        $table->build($list);
                        ?>
                    </div>
                </div>
            </div>
            <div id="partial-modal">
                <div class="modal_content">
                </div>
            </div>
        </main>
        <?php include './partial/scripts.php'; ?>
        <script src="/assets/js/jquery.renderOn.js"></script>
        <script>
            $(document).ready(function () {
                $('#table').renderOn_handle('tr.render-trigger', {
                    'route': "/drilling/main/info.partial"
                });
            });
        </script>
    </body>
</html>
