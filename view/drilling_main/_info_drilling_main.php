<div class="col-sm-11">
    <span class="text-primary lead"><?= __("Follow in down the drilling productions of this drilling. For update click in button in the side.") ?></span>
    <a href="/drilling/main/edit/?drilling=<?= $drilling_main->id_drilling_main ?>" class="edit-drilling btn btn-info btn-sm mr-2 float-lg-right" id="edit-<?= $drilling_main->id_drilling_main ?>"><i class="fa fa-pencil"></i> <?= __("Edit") ?></a>
</div>
<?php
foreach ($productions as $row => $production) {
    $type_of_hole = new TypeOfHoles($production->id_type_holes);
    ?>
    <div class="col-sm-12 col-lg-4">
        <div class="text-muted"><b><?= __("Type of Hole") ?>: </b> <span class="lead"><?= $type_of_hole->type ?></span></div>
        <div class="text-muted"><b><?= __("Description") ?>: </b> <span class="lead"><?= $type_of_hole->description ?></span></div>
    </div>
    <div class="col-sm-12 col-lg-3">
        <div class="text-muted"><b><?= __("Meters") ?>: </b> <span class="lead"><?= $production->meters ?></span></div>
        <div class="text-muted"><b><?= __("Number Holes") ?>: </b> <span class="lead"><?= $production->number_holes ?></span></div>
    </div>
    <div class="col-sm-12 col-lg-3">
        <div class="text-muted"><b><?= __("Bit Prefix") ?>: </b> <span class="lead"><?= $production->bit_prefix ?></span></div>
        <div class="text-muted"><b><?= __("Bit Number") ?>: </b> <span class="lead"><?= $production->bit_number ?></span></div>
        <div class="text-muted"><b><?= __("Bit Sharp") ?>: </b> <span class="lead"><?= $production->bit_sharp ?></span></div>
    </div>
    <div class="col-sm-11">
        <hr/>
    </div>
    <?php
}
?>