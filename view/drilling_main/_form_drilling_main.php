<form class="card-block" action="#" method="POST" id="form-drilling" data-method="<?= $method ?>">
    <input type="hidden" name="id_drilling_main" value="<?= $drilling_main->id_drilling_main ?>" />
    <div class="row">
        <div class="col-sm-12 form-drilling-main">
            <legend class="card-title text-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>
                <span class="hide-on-small-only"> <?= __('Drilling Main') ?></span>
            </legend>
            <input type="hidden" id="id_drilling_main" name="id_drilling_main" value="<?= $drilling_main->id_drilling_main ?>">
            <div class="col-sm-12 col-lg-4">
                <label class="lead">Shift</label>
                <select class="form-control mb-1" name="id_shift" id="id_shift" onfocus="">
                    <option id="shift-select" value="0">Select an item...</option>
                    <?php
                    $shifts = (new Shift())->getList();
                    foreach ($shifts as $row => $shift) {
                        if ($shift['id_shift'] == $drilling_main->id_shift) {
                            echo "<option value='{$shift['id_shift']}' selected>{$shift['code']}</option>";
                        } else {
                            echo "<option value='{$shift['id_shift']}'>{$shift['code']}</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-sm-12 col-lg-4 mb-1">
                <label class="lead">Drill Operator</label>
                <select class="form-control" name="id_drill_operator">
                    <option id="operator-select" value="0">Select an item...</option>
                    <?php
                    $drill_operators = (new DrillOperator())->getList();
                    foreach ($drill_operators as $row => $drill_operator) {
                        if ($drill_operator['id_drill_operator'] == $drilling_main->id_drill_operator) {
                            echo "<option value='{$drill_operator['id_drill_operator']}' selected>{$drill_operator['name']}</option>";
                        } else {
                            echo "<option value='{$drill_operator['id_drill_operator']}'>{$drill_operator['name']}</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-sm-12 col-lg-4 mb-1">
                <label class="lead">Equipment</label>
                <select class="form-control" name="id_equipment">
                    <option id="equipment-select" value="0">Select an item...</option>
                    <?php
                    $equipments = (new Equipment())->getList();
                    foreach ($equipments as $row => $equipment) {
                        if ($equipment['id_equipment'] == $drilling_main->id_equipment) {
                            echo "<option value='{$equipment['id_equipment']}' selected>{$equipment['equipment']}</option>";
                        } else {
                            echo "<option value='{$equipment['id_equipment']}'>{$equipment['equipment']}</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="rl" class="lead" > <?= __('RL') ?></label>
                    <input class='form-control input-lg' type="text" id="rl" name="rl" placeholder="Insert the RL" value="<?= $drilling_main->rl ?>" >
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-4">
                <div class="form-group">
                    <label for="short_number" class="lead" > <?= __('Shot Number') ?></label>
                    <input class='form-control input-lg' type="text" id="shot_number" name="shot_number" placeholder="Insert the shot number" value="<?= $drilling_main->shot_number ?>" >
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="date_drilling" class="lead" > <?= __('Date Drilling') ?></label>
                    <input class='form-control input-lg' type="text" id="date_drilling" name="date_drilling" placeholder="00/00/0000 00:00" value="<?= $drilling_main->date_drilling ?>" >
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <div class="form-group">
                    <label for="hour_start_meter" class="lead" > <?= __('Start Hour Meter') ?></label>
                    <input class='form-control input-lg' type="text" id="hour_start_meter" name="hour_start_meter" placeholder="Start Hour Meter" value="<?= $drilling_main->hour_start_meter ?>" >
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <div class="form-group">
                    <label for="hour_finish_meter" class="lead"> <?= __('Finish Hour Meter') ?></label>
                    <input class='form-control input-lg' type="text" id="hour_finish_meter" name="hour_finish_meter" placeholder="Finish Hour Meter" value="<?= $drilling_main->hour_finish_meter ?>" >
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <div class="form-group">
                    <label for="start_hamer" class="lead" > <?= __('Start Hamer') ?></label>
                    <input class='form-control input-lg' type="text" id="start_hamer" name="start_hamer" placeholder="Start Hamer" value="<?= $drilling_main->start_hamer ?>" >
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <div class="form-group">
                    <label for="finish_hamer" class="lead" > <?= __('Finish Hamer') ?></label>
                    <input class='form-control input-lg' type="text" id="finish_hamer" name="finish_hamer" placeholder="Finish Hamer" value="<?= $drilling_main->finish_hamer ?>" >
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <hr>
            <legend class="card-title text-success"><i class="fa fa-clock-o" aria-hidden="true"></i> <?= __("Drilling Production") ?></legend>
            <div id="detail-fields" class="col-sm-12 text-lg-center p-0">
                <div class="col-sm-2" data-field="type_holes"><b><?= __("Type Hole") ?></b></div>
                <div class="col-sm-1" data-field="meters"><b><?= __("Meters") ?></b></div>
                <div class="col-sm-2" data-field="number_holes"><b><?= __("Number Holes") ?></b></div>
                <div class="col-sm-2" data-field="bit_prefix"><b><?= __("Bit Prefix") ?></b></div>
                <div class="col-sm-2" data-field="bit_number"><b><?= __("Bit Number") ?></b></div>
                <div class="col-sm-2" data-field="bit_sharp"><b><?= __("Bit Sharp") ?></b></div>
                <div class="col-sm-1" data-field=""></div>
            </div>
            <div class="col-sm-12 mt-2 text-lg-right">
                <button type="button" id="add-detail" class="btn btn-success" onclick="addDetail();">
                    <i class="fa fa-plus-circle"></i> <?= __("Add") ?>
                </button>
            </div>
            <div class="col-sm-12 mt-1">
                <button type="submit" onclick="register('#form-drilling')" class="btn btn-primary float-lg-right"><?= __("SEND") ?></button>
            </div>
        </div>
    </div>
</form>