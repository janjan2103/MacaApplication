<div class="row">
    <div class="form-group col-sm-12 col-lg-6">
        <label for="name"><?= __('Name') ?></label>
        <input class='form-control' type="text" id="name" name="name" placeholder="*<?= __('Name of operator') ?>" value="<?= (empty($drill_operator->name) ? '' : $drill_operator->name) ?>" required="">
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group col-sm-12 col-lg-6">
        <label for="status"><?= __('Status') ?></label>
        <textarea class='form-control' id="status" name="status" placeholder="*<?= __('Description of operator.') ?>" required=""><?= (empty($drill_operator->status) ? '' : $drill_operator->status) ?></textarea>
        <div class="help-block with-errors"></div>
    </div>
</div>