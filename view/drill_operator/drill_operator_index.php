<html>
    <head>
        <?php include './partial/head.php'; ?>
        <title>Drill Operator</title>
    </head>
    <body>
        <?= Helper::createBarPage("Drill Operator", TRUE); ?>
        <?php include './partial/menu.php'; ?>
        <main id="main" class="row pt-2">
            <div class="col-sm-12">
                <div class="section_message">
                    <?= message_box() ?>
                </div>
                <div class="mb-1 text-lg-right">
                    <?=
                    View::button('New Drill Operator', ['class' => 'register btn btn-sm btn-primary',
                        'onclick' => 'get_modal_register(this)',
                        'id' => 'id_drill_operator',
                        'data-holes' => $drill_operator->id_drill_operator], ['icon' => 'fa-plus']);
                    ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <?php
                        $table = new TableHelper(['id' => 'table', 'class' => 'table table-bordered']);
                        $table->setFields([
                            'id_drill_operator' => 'ID',
                            'name' => 'Name',
                            'status' => 'Status',
                            'options' => 'Edit'
                        ]);
                        $table->onRowAdd(function ($object) {
                            return " id='item-{$object['id_drill_operator']}' data-id='{$object['id_drill_operator']}' class='render-trigger'";
                        });
                        $table->onField([
                            'options' => function ($value) {
                                $html = "<button type='button' class='btn btn-link drill-register' id='edit-$value' data-drill='$value'><i class='fa fa-pencil'></i> Edit</button>";
                                return $html;
                            }
                        ]);
                        $table->build($list);
                        ?>
                    </div>
                </div>
            </div>
            <div id="partial-modal">
                <div class="modal_content">
                </div>
            </div>
        </main>
        <?php include './partial/scripts.php'; ?>
        <script src="/assets/js/jquery.renderOn.js"></script>
        <script>
            $(document).ready(function () {
                $('#table').renderOn_handle('tr.render-trigger', {
                    'route': "/drill/operator/info.partial"
                });
                $(".drill-register").click(function (el) {
                    $(this).renderOn_load({
                        'route': "/drill/operator/modal_edit.partial",
                        container: "#partial-modal"
                    });
                    el.stopPropagation();
                });
            });
            var get_modal_register = function (el) {
                $(el).renderOn_load({
                    'route': "/drill/operator/modal_register.partial",
                    container: "#partial-modal"
                });
            };
        </script>
    </body>
</html>
