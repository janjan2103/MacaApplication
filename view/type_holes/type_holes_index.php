<html>
    <head>
        <?php include './partial/head.php'; ?>
        <title>Type of Holes</title>
    </head>
    <body>
        <?= Helper::createBarPage("Type of Holes", TRUE); ?>
        <?php include './partial/menu.php'; ?>
        <main id="main" class="row pt-2">
            <div class="col-sm-12">
                <div class="section_message">
                    <?= message_box() ?>
                </div>
                <div class="mb-1 text-lg-right">
                    <?=
                    View::button('New Type', ['class' => 'register btn btn-sm btn-primary',
                        'onclick' => 'get_modal_register(this)',
                        'id' => 'id_type_holes',
                        'data-holes' => $type_holes->id_type_holes], ['icon' => 'fa-plus']);
                    ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <?php
                        $table = new TableHelper(['id' => 'table', 'class' => 'table table-bordered']);
                        $table->setFields([
                            'id_type_holes' => 'ID',
                            'type' => 'Type',
                            'description' => 'Description',
                            'options' => 'Edit'
                        ]);
                        $table->onRowAdd(function ($object) {
                            return " id='item-{$object['id_type_holes']}' data-id='{$object['id_type_holes']}' class='render-trigger'";
                        });
                        $table->onField([
                            'options' => function ($value) {
                                $html = "<div style='width=100%'><button type='button' class='btn btn-link edit-hole' id='edit-$value' data-holes='$value'><i class='fa fa-pencil'></i> Edit</button></div>";
                                return $html;
                            }
                        ]);
                        $table->build($list);
                        ?>
                    </div>
                </div>
            </div>
            <div id="partial-modal">
                <div class="modal_content">
                </div>
            </div>
        </main>
        <?php include './partial/scripts.php'; ?>
        <script src="/assets/js/jquery.renderOn.js"></script>
        <script>
            $(document).ready(function () {
                $('tr.render-trigger').click(function () {
                    $('#table').renderOn_handle('tr.render-trigger', {
                        'route': "/holes/info.partial"
                    });
                });
                $(".edit-hole").click(function (el) {
                    $(this).renderOn_load({
                        'route': "/holes/modal_edit.partial",
                        container: "#partial-modal"
                    });
                    el.stopPropagation();
                });
            });
            var get_modal_register = function (el) {
                $(el).renderOn_load({
                    'route': "/holes/modal_register.partial",
                    container: "#partial-modal"
                });
            };
        </script>
    </body>
</html>
