<div class="row">
    <div class="form-group col-sm-12 col-lg-6">
        <label for="code"><?= __('Type') ?></label>
        <input class='form-control' type="text" id="type" name="type" placeholder="*<?= __('Insert the type of hole') ?>" value="<?= (empty($type_holes->type) ? '' : $type_holes->type) ?>" required="">
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group col-sm-12 col-lg-6">
        <label for="qtd_hours"><?= __('Description') ?></label>
        <textarea class='form-control' id="description" name="description" placeholder="*<?= __('Insert a description.') ?>" required=""><?= (empty($type_holes->description) ? '' : $type_holes->description) ?></textarea>
        <div class="help-block with-errors"></div>
    </div>
</div>