<script>
    function sendForm() {
        var formData = new FormData($("#type_holes_edit")[0]);
        $.ajax({
            method: "POST",
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            url: "/holes/update.json",
            success: function (data) {
                res = JSON.parse(data);
                if (res.code === 0) {
                    $("#box-success").removeClass("hideMe");
                    $("#message").html(res.message);
                    setTimeout(function () {
                        $("#modal_type_holes_edit").modal("hide");
                        window.location.assign("/holes/");
                    }, 3000);
                } else {
                    $("#box-error").removeClass("hideMe");
                    $("#message-error").html(res.message);
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                console.log("xhr: " + xhr);
                console.log("ajaxOptions: " + ajaxOptions);
                console.log("thrownError: " + thrownError);
            }
        });
    }

    $(document).ready(function () {
        $("#modal_type_holes_edit").modal("show");
    });
</script>
<div class="modal fade" id="modal_type_holes_edit" tabindex="-1" role="dialog" aria-labelledby="modal_type_holes_edit-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title text-lg-center" id="modal_type_holes_edit-label"><?= __('Register Type of Holes') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 hideMe" id="box-error">
                        <div class="alert alert-danger" id="message-error">

                        </div>
                    </div>
                    <div class="col-sm-12 hideMe" id="box-success">
                        <div class="alert alert-success" id="message">

                        </div>
                    </div>
                    <form class="col-sm-12" action="/holes/update" method="POST" id="type_holes_edit">
                        <input type="hidden" name="id_type_holes" value="<?= $type_holes->id_type_holes ?>" />
                        <?php include './view/type_holes/_form_type_holes.php'; ?>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button id="cancel_modal" type="button" class="btn btn-link" data-dismiss="modal"><?= __('Cancelar') ?></button>
                <button type="button" class="btn btn-primary" onclick="sendForm(this);"><?= __('Salvar') ?></button>
            </div>
        </div>  
    </div>
</div>