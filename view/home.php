<html>
    <head>
        <?php include './partial/head.php'; ?>
        <title>Home</title>
    </head>
    <body style="background-color: #F6F6F6;">
        <?= new TopMenu(NULL, false, ['display_logo' => TRUE]); ?>
        <div class="container top-container">
            <div class="row">
                <?php include './partial/menu.php';  ?>
                <div class="col-sm-12 <?= $col_lg ?>">
                    <div class="card mt-1">
                        <div class="card-block p-1" style="text-align: center;">
                            <h3 class="lead-lg text-primary">Lorem Ipsum</h3>
                            <p class="text-justify lead text-muted">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
                                "There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."

                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at efficitur leo. Nullam non dui id lacus dignissim sagittis. Integer gravida turpis sed velit placerat semper at sed turpis. Pellentesque feugiat vel erat maximus dignissim. Donec in pulvinar tellus. Aliquam non elementum mauris, in condimentum velit. Fusce id odio elit. Vivamus id posuere orci. Sed luctus tempor aliquet. Ut quis eleifend enim. Aliquam consequat finibus venenatis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque ut purus ultrices, tristique dolor et, ornare metus.

                                Donec tempor posuere dolor, in euismod dui cursus a. Sed sagittis ipsum vitae tellus egestas tristique. Vestibulum ac metus a metus sollicitudin luctus ac dictum arcu. Mauris scelerisque in justo at accumsan. In eu tincidunt metus. Quisque quis bibendum diam. Fusce pharetra eros lacus, ac facilisis arcu faucibus eu. Vestibulum feugiat tincidunt dapibus. In vitae efficitur augue. Praesent vel faucibus nunc. Duis scelerisque nunc vel urna facilisis, ut tempor est maximus. Nunc efficitur risus in arcu tincidunt bibendum. Nulla dictum sapien nec ante sodales efficitur.

                                Aliquam ullamcorper suscipit faucibus. Maecenas dapibus ipsum sed urna hendrerit, id blandit nunc congue. Curabitur scelerisque quis elit a lobortis. In vitae vulputate mi. In mattis facilisis varius. Duis lobortis, est nec lobortis malesuada, diam elit porttitor tortor, sit amet tempor ex sem a sapien. Mauris feugiat cursus mauris eu consectetur.

                                Aenean luctus eros vitae felis vulputate hendrerit. Phasellus lacinia vulputate urna ac convallis. Ut volutpat eleifend eros at fringilla. Phasellus risus mauris, vulputate vel pharetra at, vestibulum et felis. Nam accumsan augue lorem, porta pharetra leo rutrum vitae. Donec id leo rutrum, tempor metus ut, tincidunt velit. Nunc sed suscipit velit. Donec ante est, bibendum a metus semper, consequat malesuada diam. Nullam finibus ex sit amet dolor volutpat, non tincidunt mi rhoncus. Praesent odio nunc, molestie vitae aliquet et, ullamcorper vel nibh. Curabitur efficitur pellentesque malesuada.

                                Phasellus porta orci quis aliquet luctus. Aenean ullamcorper sapien at convallis pretium. Aliquam commodo sodales tellus sit amet faucibus. Maecenas at enim a sapien ultricies congue. Morbi in tellus nulla. Curabitur luctus, orci iaculis placerat cursus, magna nisi egestas dui, vitae facilisis metus orci in augue. Sed euismod nec turpis nec laoreet. Suspendisse potenti. Praesent tempor rutrum ligula at facilisis. Suspendisse fringilla blandit ante a iaculis.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="partial-modal">
                <div class="modal_content">
                </div>
            </div>
        </div>
        <?php include './partial/scripts.php'; ?>
        <script src="/assets/js/jquery.renderOn.js"></script>
        <script>
            function change_profile(el) {
                $(el).renderOn_load({
                    'route': "/login/modal_update.partial",
                    container: "#partial-modal"
                });
            }
            function get_modal_login(el) {
                $(el).renderOn_load({
                    'route': "/login/modal_login.partial",
                    container: "#partial-modal"
                });
            }
        </script>
    </body>
</html>