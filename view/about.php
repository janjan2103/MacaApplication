<html>
    <head>
        <?php include './partial/head.php'; ?>
        <title>Informations</title>
    </head>
    <body>
        <?= Helper::createBarPage("Informations"); ?>
        <?php include './partial/menu.php'; ?>
        <main id="main" class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block text-justify">
                        <p>One of the craziest times to be inside my mind—and there are many—is when I've just sent a woman an important text message and am waiting for her response. Especially when the relationship (or potential relationship—let’s not get ahead of ourselves here) is in its early stages. Examples of a crucial text include, but are certainly not limited to, iterations of the following: "Would you like to go out with me again sometime?" "What did I do?" and "Who is that guy in all your recent Instagram pictures and why is he so beautiful haha?" I'm not the only guy who thinks all kinds of things while waiting for a response, or who even sometimes flat-out panics. In that vein, I asked a few other men what runs through their minds during those trying times. Here’s what they had to say.</p>
                        <h2>1. Keep hope alive.</h2>
                        <p>"I try to think about how great it’s going to be when she texts me back and it’s a positive response. Sometimes I even start to plan for positivity, like looking into places we can go to on another date, for example. I’m not gonna lie to you—my therapist suggested I try this. It’s easier said than done, because I usually still end up thinking she’s either not going to answer me at all, or is going to answer with something awful." —Micah A., 25</p>
                        <h2>2. Did I screw up somehow?</h2>
                        <p>"I freak out and think about all the things she might be upset with me about. Then I look through all my previous text messages and analyze them with a fine-toothed comb, wondering what I might have said or done wrong. After that, I think about whether any of my behaviors might have annoyed her. Like maybe I posted something on social media she didn’t like, or I was contacting her too much or not enough. My thought process is very in line with Murphy’s Law: I think about anything that can go wrong and assume it already has. Most of the time, though, she texts back after a few minutes and everything is fine." —Scott P., 28</p>
                        <h2>3. Those typing bubbles are the best/worst thing ever.</h2>
                        <p>"All the excitement and anxiety I drum up while I’m waiting for a response multiplies by about a thousand whenever I see those bubble indicators that pop up when she’s typing back at me. I almost have a heart attack when the bubbles appear and then disappear again." —Jared S., 30</p>
                        <h2>4. Do I dare to double text?</h2>
                        <p>"I keep thinking about how long I should wait to follow up. I do this until she either texts me back or I, you know, follow up." —Patrick W., 24</p>
                        <h2>5. We're not together, so whatever she's doing instead of answering me is OK...but I hate this.</h2>
                        <p>"It's mainly stuff like, 'Oh god oh god oh god, she’s having sex with someone else right now, isn’t she? That has to be why she isn’t answering me. And I can’t even be mad because we aren’t technically even exclusive!’ I’m a pessimist and also a little bit insecure, if you couldn’t tell." —Alex H., 24</p>
                        <h2>6. It's 2016—why is there no technological invention that lets me retract that message?</h2>
                        <p>"My typical post-text thought is that we need to come up with a way to unsend texts until the person we sent them to reads them. That way you can take it back if you're waiting too long and start to regret sending it." —Brent F., 31</p>
                        <h2>7. She's clearly gravely injured, otherwise she'd be responding.</h2>
                        <p>"My thoughts are always worried ones, ranging from 'Is she pissed at me about something?' to 'Oh, sh*t, is she severely injured or even dead?' I’m generally a pretty anxious person. I just don’t think I’m wired to think of what's actually the most likely reason she’s not texting me back: she’s got something else going on, and will get to answering me when she has a chance." —Sam W., 28</p>
                        <h2>8. Is she also staring at our text conversation right now?</h2>
                        <p>"I compulsively check my phone like every three seconds after sending, and I spend the entire time until she responds thinking about what she might be doing instead of texting me back. I wonder if she’s thinking about how to reply, is simply busy, or if she’s with another guy." —Matthew L., 29</p>
                        <h2>9. It'll all work out in the end.</h2>
                        <p>"Everything is going to be fine. Keep calm. Everything is going to be fine. Keep calm." —Robert F., </p>
                    </div>
                </div>
            </div>
        </main>
        <?php include './partial/scripts.php'; ?>
    </body>
</html>