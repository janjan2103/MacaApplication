<div class="row">
    <div class="form-group col-sm-12">
        <label for="code"><?= __('Equipment') ?></label>
        <input class='form-control' type="text" id="equipment" name="equipment" placeholder="*<?= __('Insert the name of equipment') ?>" value="<?= (empty($equipment->equipment) ? '' : $equipment->equipment) ?>" required="">
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group col-sm-12">
        <label for="company"><?= __('Company') ?></label>
        <input class='form-control' type="text" id="company" name="company" placeholder="*<?= __('Insert the company of equipment.') ?>" value="<?= (empty($equipment->company) ? '' : $equipment->company) ?>" required="">
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group col-sm-12 col-lg-6">
        <label for="type"><?= __('Type') ?></label>
        <input class='form-control' type="text" id="type" name="type" placeholder="*<?= __('Insert the type of equipment.') ?>" value="<?= (empty($equipment->company) ? '' : $equipment->company) ?>" required="">
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group col-sm-12 col-lg-6">
        <label for="fleet"><?= __('Fleet') ?></label>
        <input class='form-control' type="text" id="fleet" name="fleet" placeholder="*<?= __('Insert the fleet of equipment.') ?>" value="<?= (empty($equipment->company) ? '' : $equipment->company) ?>" required="">
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group col-sm-12">
        <label for="status"><?= __('Status') ?></label>
        <input class='form-control' type="text" id="status" name="status" placeholder="*<?= __('Insert the status of equipment.') ?>" value="<?= (empty($equipment->company) ? '' : $equipment->company) ?>" required="">
        <div class="help-block with-errors"></div>
    </div>
</div>