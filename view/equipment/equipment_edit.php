<script>
    function sendForm() {
        var formData = new FormData($("#equipment_edit")[0]);
        $.ajax({
            method: "POST",
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            url: "/equipment/update.json",
            success: function (data) {
                res = JSON.parse(data);
                if (res.code === 0) {
                    $("#box-success").removeClass("hideMe");
                    $("#message").html(res.message);
                    setTimeout(function () {
                        $("#modal_equipment_edit").modal("hide");
                        window.location.assign("/equipment/");
                    }, 3000);
                } else {
                    $("#box-error").removeClass("hideMe");
                    $("#message-error").html(res.message);
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                console.log("xhr: " + xhr);
                console.log("ajaxOptions: " + ajaxOptions);
                console.log("thrownError: " + thrownError);
            }
        });
    }

    $(document).ready(function () {
        $("#modal_equipment_edit").modal("show");
    });
</script>
<div class="modal fade" id="modal_equipment_edit" tabindex="-1" role="dialog" aria-labelledby="modal_equipment_edit-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title text-lg-center" id="modal_equipment_edit-label"><?= __('Register Equipment') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 hideMe" id="box-error">
                        <div class="alert alert-danger" id="message-error">

                        </div>
                    </div>
                    <div class="col-sm-12 hideMe" id="box-success">
                        <div class="alert alert-success" id="message">

                        </div>
                    </div>
                    <form class="col-sm-12" action="/equipment/update" method="POST" id="equipment_edit">
                        <input type="hidden" name="id_equipment" value="<?= $equipment->id_equipment ?>" />
                        <?php include './view/equipment/_form_equipment.php'; ?>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button id="cancel_modal" type="button" class="btn btn-link" data-dismiss="modal"><?= __('Cancelar') ?></button>
                <button type="button" class="btn btn-primary" onclick="sendForm(this);"><?= __('Salvar') ?></button>
            </div>
        </div>  
    </div>
</div>