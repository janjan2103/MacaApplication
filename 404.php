<html>
    <head>
        <?php
        header('HTTP/1.0 404 Not Found');
        include './partial/head.php';
        ?>        
        <title>ERRO 404</title>
        <link type="text/css" rel="stylesheet" href="/assets/css/cover.css">    
    </head>
    <body>
        <div class="site-wrapper">
            <div class="site-wrapper-inner">
                <div class="inner cover">
                    <h1 class="cover-heading"><i class="text-white fa fa-exclamation-triangle"></i> 404</h1>
                    <p class="lead">
                        OOOPS! <i class="fa fa-frown-o text-white"></i>
                    </p>
                    <p class="lead">
                        Página não foi encontrada
                    </p>
                    <div class="lead">
                        Verifique a URL no seu navegador.
                    </div>
                    <div class="lead">
                        Por favor, caso seja algum botão ou link tenha redirecionado você para esta página, contate-nos.
                    </div>
                    <br/>
                    <p class="lead">
                        <a class="btn btn-lg btn-secondary" href="/">INICIO</a>
                    </p>
                </div>
            </div>
        </div>
        <?php include './partial/scripts.php'; ?>
    </body>
</html>